CREATE TABLE  "AUTHOR" 
   (    "AUTHOR_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "EXP" TIMESTAMP (6), 
    "NAME" VARCHAR2(255 CHAR), 
     PRIMARY KEY ("AUTHOR_ID") ENABLE
   ) ;CREATE TABLE  "NEWS" 
   (    "NEWS_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "CREATION_DATE" TIMESTAMP (6), 
    "FULL_TEXT" VARCHAR2(255 CHAR), 
    "MODIFICATION_DATE" TIMESTAMP (6), 
    "SHORT_TEXT" VARCHAR2(255 CHAR), 
    "TITLE" VARCHAR2(255 CHAR), 
    "VERSION" NUMBER, 
     PRIMARY KEY ("NEWS_ID") ENABLE
   ) ;CREATE TABLE  "COMMENTS" 
   (    "COMMENTS_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "COMMENT_TEXT" VARCHAR2(255 CHAR), 
    "CREATION_DATE" TIMESTAMP (6), 
    "NEWS_ID" NUMBER(19,0), 
     PRIMARY KEY ("COMMENTS_ID") ENABLE
   ) ;CREATE TABLE  "HTMLDB_PLAN_TABLE" 
   (    "STATEMENT_ID" VARCHAR2(30), 
    "PLAN_ID" NUMBER, 
    "TIMESTAMP" DATE, 
    "REMARKS" VARCHAR2(4000), 
    "OPERATION" VARCHAR2(30), 
    "OPTIONS" VARCHAR2(255), 
    "OBJECT_NODE" VARCHAR2(128), 
    "OBJECT_OWNER" VARCHAR2(30), 
    "OBJECT_NAME" VARCHAR2(30), 
    "OBJECT_ALIAS" VARCHAR2(65), 
    "OBJECT_INSTANCE" NUMBER(*,0), 
    "OBJECT_TYPE" VARCHAR2(30), 
    "OPTIMIZER" VARCHAR2(255), 
    "SEARCH_COLUMNS" NUMBER, 
    "ID" NUMBER(*,0), 
    "PARENT_ID" NUMBER(*,0), 
    "DEPTH" NUMBER(*,0), 
    "POSITION" NUMBER(*,0), 
    "COST" NUMBER(*,0), 
    "CARDINALITY" NUMBER(*,0), 
    "BYTES" NUMBER(*,0), 
    "OTHER_TAG" VARCHAR2(255), 
    "PARTITION_START" VARCHAR2(255), 
    "PARTITION_STOP" VARCHAR2(255), 
    "PARTITION_ID" NUMBER(*,0), 
    "OTHER" LONG, 
    "DISTRIBUTION" VARCHAR2(30), 
    "CPU_COST" NUMBER(*,0), 
    "IO_COST" NUMBER(*,0), 
    "TEMP_SPACE" NUMBER(*,0), 
    "ACCESS_PREDICATES" VARCHAR2(4000), 
    "FILTER_PREDICATES" VARCHAR2(4000), 
    "PROJECTION" VARCHAR2(4000), 
    "TIME" NUMBER(*,0), 
    "QBLOCK_NAME" VARCHAR2(30)
   ) ;CREATE TABLE  "NEWS_AUTHOR" 
   (    "NEWS_AUTHOR_ID" NUMBER(20,0) NOT NULL ENABLE, 
    "NEWS_ID" NUMBER(20,0) NOT NULL ENABLE, 
    "AUTHOR_ID" NUMBER(20,0) NOT NULL ENABLE, 
     CONSTRAINT "NEWS_AUTHOR_PK" PRIMARY KEY ("NEWS_AUTHOR_ID") ENABLE
   ) ;CREATE TABLE  "TAG" 
   (    "TAG_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "TAG_NAME" VARCHAR2(255 CHAR), 
     PRIMARY KEY ("TAG_ID") ENABLE
   ) ;CREATE TABLE  "NEWS_TAG" 
   (    "NEWS_TAG_ID" NUMBER(20,0) NOT NULL ENABLE, 
    "NEWS_ID" NUMBER(20,0) NOT NULL ENABLE, 
    "TAG_ID" NUMBER(20,0) NOT NULL ENABLE, 
     CONSTRAINT "NEWS_TAG_PK" PRIMARY KEY ("NEWS_TAG_ID") ENABLE
   ) ;CREATE TABLE  "USER_ROLE" 
   (    "USER_ROLE_ID" NUMBER NOT NULL ENABLE, 
    "ROLE" VARCHAR2(100) NOT NULL ENABLE, 
     CONSTRAINT "USER_ROLE_PK" PRIMARY KEY ("USER_ROLE_ID") ENABLE
   ) ;CREATE TABLE  "USERS" 
   (    "USERS_ID" NUMBER NOT NULL ENABLE, 
    "USERNAME" VARCHAR2(200) NOT NULL ENABLE, 
    "PASSWORD" VARCHAR2(400) NOT NULL ENABLE, 
    "USER_ROLE_ID" NUMBER NOT NULL ENABLE, 
     CONSTRAINT "USERS_PK" PRIMARY KEY ("USERS_ID") ENABLE
   ) ;ALTER TABLE  "COMMENTS" ADD CONSTRAINT "FK_119BKP1H3JKBBT8IVT80YLB7" FOREIGN KEY ("NEWS_ID")
      REFERENCES  "NEWS" ("NEWS_ID") ENABLE;ALTER TABLE  "NEWS_TAG" ADD CONSTRAINT "FK_13RA5RGBN1E9A7ACEDLUWBY5D" FOREIGN KEY ("NEWS_ID")
      REFERENCES  "NEWS" ("NEWS_ID") ENABLE;ALTER TABLE  "NEWS_TAG" ADD CONSTRAINT "FK_8OGR04DD3S762CD01BJCEXQLJ" FOREIGN KEY ("TAG_ID")
      REFERENCES  "TAG" ("TAG_ID") ENABLE;ALTER TABLE  "NEWS_AUTHOR" ADD CONSTRAINT "FK_8U150PDQRAI6L9VVNN3SMH9PR" FOREIGN KEY ("NEWS_ID")
      REFERENCES  "NEWS" ("NEWS_ID") ENABLE;ALTER TABLE  "NEWS_AUTHOR" ADD CONSTRAINT "FK_GFLYVF5KR8WX1UMD586DFTD83" FOREIGN KEY ("AUTHOR_ID")
      REFERENCES  "AUTHOR" ("AUTHOR_ID") ENABLE;ALTER TABLE  "USERS" ADD CONSTRAINT "USERS_FK" FOREIGN KEY ("USER_ROLE_ID")
      REFERENCES  "USER_ROLE" ("USER_ROLE_ID") ENABLE;CREATE OR REPLACE FUNCTION  "GETPREVIOUSNEWS" 
(
  NEWSID IN NUMBER 
) RETURN NUMBER AS 
  idPreviousNews INTEGER;
BEGIN
DECLARE
    CURSOR news_cursor IS SELECT news.news_id       
                          FROM news 
                              LEFT JOIN comments 
                                  ON news.news_id = comments.news_id 
                          GROUP BY news.news_id, news.modification_date
                          ORDER BY count(comments.comments_id) DESC, news.modification_date DESC;
    news_id INTEGER;
    
    BEGIN
  
        OPEN news_cursor;
        
        LOOP
           
          FETCH news_cursor INTO news_id;
          EXIT WHEN news_cursor%NOTFOUND;

          IF (news_id = newsid ) THEN 
             EXIT;
          END IF;
          
          idPreviousNews:=news_id;
        
        END LOOP;
        
        CLOSE news_cursor;
        
    END;
  RETURN  idPreviousNews;
END GETPREVIOUSNEWS;
/
/
CREATE OR REPLACE FUNCTION  "GETNEXTNEWS" 
(newsid in NUMBER)
return NUMBER
as
    idNextNews INTEGER;
begin
    
    DECLARE
    CURSOR news_cursor IS SELECT news.news_id       
                          FROM news 
                              LEFT JOIN comments 
                                  ON news.news_id = comments.news_id 
                          GROUP BY news.news_id, news.MODIFICATION_DATE
                          ORDER BY count(comments.comments_id) DESC, news.MODIFICATION_DATE DESC;
    flag INTEGER;
    news_id INTEGER;
    
    BEGIN
        
        flag := 0;

        OPEN news_cursor;
        
        LOOP
           
          FETCH news_cursor INTO news_id;
          EXIT WHEN news_cursor%NOTFOUND;

          IF (flag = 1) THEN idNextNews := news_id;
          EXIT;
          END IF;
          IF (news_id = newsid ) THEN 
              flag := 1;
          END IF;
        
        END LOOP;
        
        CLOSE news_cursor;

        
        
    END;
    RETURN(idNextNews);
    
end;
/
/
CREATE OR REPLACE FUNCTION  "CUSTOM_HASH" (p_username in varchar2, p_password in varchar2)
return varchar2
is
  l_password varchar2(4000);
  l_salt varchar2(4000) := 'XY817MSQYL80TWL9EGYBTYNR9ENTDK';
begin

-- This function should be wrapped, as the hash algorhythm is exposed here.
-- You can change the value of l_salt or the method of which to call the
-- DBMS_OBFUSCATOIN toolkit, but you much reset all of your passwords
-- if you choose to do this.

l_password := utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5
  (input_string => p_password || substr(l_salt,10,13) || p_username ||
    substr(l_salt, 4,10)));
return l_password;
end;
/
/
CREATE OR REPLACE FUNCTION  "CUSTOM_AUTH" (p_username in VARCHAR2, p_password in VARCHAR2)
return BOOLEAN
is
  l_password varchar2(4000);
  l_stored_password varchar2(4000);
  l_expires_on date;
  l_count number;
begin
-- First, check to see if the user is in the user table
select count(*) into l_count from demo_users where user_name = p_username;
if l_count > 0 then
  -- First, we fetch the stored hashed password & expire date
  select password, expires_on into l_stored_password, l_expires_on
   from demo_users where user_name = p_username;

  -- Next, we check to see if the user's account is expired
  -- If it is, return FALSE
  if l_expires_on > sysdate or l_expires_on is null then

    -- If the account is not expired, we have to apply the custom hash
    -- function to the password
    l_password := custom_hash(p_username, p_password);

    -- Finally, we compare them to see if they are the same and return
    -- either TRUE or FALSE
    if l_password = l_stored_password then
      return true;
    else
      return false;
    end if;
  else
    return false;
  end if;
else
  -- The username provided is not in the DEMO_USERS table
  return false;
end if;
end;
/
/
CREATE UNIQUE INDEX  "NEWS_AUTHOR_PK" ON  "NEWS_AUTHOR" ("NEWS_AUTHOR_ID") 
  ;CREATE UNIQUE INDEX  "NEWS_TAG_PK" ON  "NEWS_TAG" ("NEWS_TAG_ID") 
  ;CREATE UNIQUE INDEX  "SYS_C008063" ON  "AUTHOR" ("AUTHOR_ID") 
  ;CREATE UNIQUE INDEX  "SYS_C008065" ON  "COMMENTS" ("COMMENTS_ID") 
  ;CREATE UNIQUE INDEX  "SYS_C008067" ON  "NEWS" ("NEWS_ID") 
  ;CREATE UNIQUE INDEX  "SYS_C008069" ON  "TAG" ("TAG_ID") 
  ;CREATE UNIQUE INDEX  "USERS_PK" ON  "USERS" ("USERS_ID") 
  ;CREATE UNIQUE INDEX  "USER_ROLE_PK" ON  "USER_ROLE" ("USER_ROLE_ID") 
  ;CREATE OR REPLACE PROCEDURE  "GETNEXT" 
(idnews IN NUMBER)
is
begin
dsfdfs
end;
/
/
 CREATE SEQUENCE   "USER_ROLE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "USER_ROLES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "TAG_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 161 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "NEWS_TAG_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 803 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "NEWS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 505 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "NEWS_AUTHOR_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 603 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "DEMO_USERS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "DEMO_PROD_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "DEMO_ORD_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 11 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "DEMO_ORDER_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "DEMO_CUST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "COMMENTS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 261 CACHE 20 NOORDER  NOCYCLE ; CREATE SEQUENCE   "AUTHOR_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 221 CACHE 20 NOORDER  NOCYCLE ;CREATE OR REPLACE TRIGGER  "TAG_T1" 
BEFORE
delete on "TAG"
for each row
begin
    EXECUTE IMMEDIATE 'DELETE FROM news_tag n WHERE n.tag_id= :id'  USING :old.tag_id;
    
end;

/
ALTER TRIGGER  "TAG_T1" ENABLE;CREATE OR REPLACE TRIGGER  "NEWS_T1" 
BEFORE
delete on "NEWS"
for each row
begin

    EXECUTE IMMEDIATE 'DELETE FROM comments c WHERE c.news_id = :id'  USING :old.news_id;
    EXECUTE IMMEDIATE 'DELETE FROM news_author n WHERE n.news_id = :id'  USING :old.news_id;
    EXECUTE IMMEDIATE 'DELETE FROM news_tag n WHERE n.news_id = :id'  USING :old.news_id;
    
end;
/
ALTER TRIGGER  "NEWS_T1" ENABLE;CREATE OR REPLACE TRIGGER  "BI_USER_ROLE" 
  before insert on "USER_ROLE"               
  for each row  
begin   
  if :NEW."USER_ROLE_ID" is null then 
    select "USER_ROLE_SEQ".nextval into :NEW."USER_ROLE_ID" from dual; 
  end if; 
end; 

/
ALTER TRIGGER  "BI_USER_ROLE" ENABLE;CREATE OR REPLACE TRIGGER  "BI_USERS" 
  before insert on "USERS"               
  for each row  
begin   
  if :NEW."USERS_ID" is null then 
    select "USER_ROLE_SEQ".nextval into :NEW."USERS_ID" from dual; 
  end if; 
end; 

/
ALTER TRIGGER  "BI_USERS" ENABLE;CREATE OR REPLACE TRIGGER  "BI_NEWS_TAG" 
  before insert on "NEWS_TAG"               
  for each row  
begin   
  if :NEW."NEWS_TAG_ID" is null then 
    select "NEWS_TAG_SEQ".nextval into :NEW."NEWS_TAG_ID" from dual; 
  end if; 
end; 

/
ALTER TRIGGER  "BI_NEWS_TAG" ENABLE;CREATE OR REPLACE TRIGGER  "BI_NEWS_AUTHOR" 
  before insert on "NEWS_AUTHOR"               
  for each row  
begin   
  if :NEW."NEWS_AUTHOR_ID" is null then 
    select "NEWS_AUTHOR_SEQ".nextval into :NEW."NEWS_AUTHOR_ID" from dual; 
  end if; 
end; 

/
ALTER TRIGGER  "BI_NEWS_AUTHOR" ENABLE;