package by.epam.newsapp.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;

@ContextConfiguration(locations = { "classpath:/spring.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

	@Mock
	private NewsDAO newsDAO;

	@InjectMocks
	@Autowired
	private Service<NewsTO> service;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	/*@Test
	public void testAdd() throws Exception {

		Long id = new Long(1);

		when(newsDAO.add(any(NewsTO.class))).thenReturn(id);

		when(newsDAO.attachAuthorToNews(any(Long.class), any(Long.class)))
				.thenReturn(id);
		when(newsDAO.attachTagToNews(any(Long.class), any(Long.class)))
				.thenReturn(id);

		NewsTO news = initNews();
		AuthorTO author = new AuthorTO(id, "Dima");
		List<AuthorTO> authors = Arrays.asList(author);
		TagTO tag = new TagTO(id, "tag1");
		List<TagTO> tags = Arrays.asList(tag);
		news.setAuthors(authors);
		news.setTags(tags);

		Long newsId = service.save(news);
		Assert.assertEquals(id, newsId);

		verify(newsDAO).attachAuthorToNews(any(Long.class), any(Long.class));
		verify(newsDAO).attachTagToNews(any(Long.class), any(Long.class));

	}
	*/
/*
	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testAddThrowsException() throws Exception {
		Long id = new Long(1);

		when(newsDAO.attachAuthorToNews(any(Long.class), any(Long.class)))
				.thenThrow(DAOException.class);

		NewsTO news =	initNews();
		AuthorTO author = new AuthorTO(id, "Dima");
		List<AuthorTO> authors = Arrays.asList(author);
		TagTO tag = new TagTO(id, "tag1");
		List<TagTO> tags = Arrays.asList(tag);
		news.setAuthors(authors);
		news.setTags(tags);

		service.save(news);

	}
	*/
/*
	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testAttachAuthorThrowsException() throws Exception {
		Long id = new Long(1);

		when(newsDAO.add(any(NewsTO.class))).thenThrow(DAOException.class);

		NewsTO news = initNews();
		AuthorTO author = new AuthorTO(id, "Dima");
		List<AuthorTO> authors = Arrays.asList(author);
		TagTO tag = new TagTO(id, "tag1");
		List<TagTO> tags = Arrays.asList(tag);
		news.setAuthors(authors);
		news.setTags(tags);

		service.save(news);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testAttachTagThrowsException() throws Exception {
		Long id = new Long(1);

		when(newsDAO.attachTagToNews(any(Long.class), any(Long.class)))
				.thenThrow(DAOException.class);

		NewsTO news = initNews();
		AuthorTO author = new AuthorTO(id, "Dima");
		List<AuthorTO> authors = Arrays.asList(author);
		TagTO tag = new TagTO(id, "tag1");
		List<TagTO> tags = Arrays.asList(tag);
		news.setAuthors(authors);
		news.setTags(tags);

		service.save(news);

	}
	*/

	@Test
	public void testUpdate() throws Exception {

		Long id = new Long(1);

		NewsTO news = initNews();

		NewsTO newsNews = new NewsTO();
		newsNews.setId(id);
		newsNews.setTitle("test1_title");
		newsNews.setFullText("test1_full");
		newsNews.setShortText("test1_short");
		newsNews.setCreationDate(new Date());
		newsNews.setModificationDate(new Date());

		when(newsDAO.fetchById(id)).thenReturn(news);
		boolean update = service.update(newsNews);
		Assert.assertTrue(update);
		verify(newsDAO).fetchById(id);
		ArgumentCaptor<NewsTO> newsCaptor = ArgumentCaptor.forClass(NewsTO.class);
		verify(newsDAO).update(newsCaptor.capture());
		NewsTO updatedNews = newsCaptor.getValue();
		Assert.assertEquals("test1_title", updatedNews.getTitle());
		Assert.assertEquals("test1_full", updatedNews.getFullText());
		Assert.assertEquals("test1_short", updatedNews.getShortText());

	}

	@Test
	public void testUpdateIfNewsNotFound() throws Exception {

		Long id = new Long(1);
		when(newsDAO.fetchById(id)).thenReturn(null);

		NewsTO newsNews = new NewsTO();
		newsNews.setId(id);
		newsNews.setTitle("test1_title");
		newsNews.setFullText("test1_full");
		newsNews.setShortText("test1_short");
		newsNews.setCreationDate(new Date());
		newsNews.setModificationDate(new Date());

		boolean update = service.update(newsNews);
		Assert.assertFalse(update);
		verify(newsDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testUpdateThrowException() throws Exception {

		Long id = new Long(1);

		NewsTO news = initNews();

		when(newsDAO.fetchById(id)).thenReturn(news);
		doThrow(DAOException.class).when(newsDAO).update(news);
		service.update(news);
		verify(newsDAO).fetchById(id);
		verify(newsDAO).update(news);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(1);
		NewsTO news = initNews();
		news.setModificationDate(new Date());
		when(newsDAO.fetchById(id)).thenReturn(news);
		boolean delete = service.delete(id);
		Assert.assertTrue(delete);
		verify(newsDAO).fetchById(id);
		verify(newsDAO).delete(id);
	}

	@Test
	public void testDeleteIfNewsNotFound() throws Exception {

		Long id = new Long(1);
		when(newsDAO.fetchById(id)).thenReturn(null);
		boolean delete = service.delete(id);
		Assert.assertFalse(delete);
		verify(newsDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testDeleteThrowsException() throws Exception {

		Long id = new Long(1);
		NewsTO news = initNews();
		when(newsDAO.fetchById(id)).thenReturn(news);
		doThrow(DAOException.class).when(newsDAO).delete(id);
		service.delete(id);
		verify(newsDAO).fetchById(id);
		verify(newsDAO).delete(id);

	}

	@Test
	public void testList() throws Exception {

		List<NewsTO> newsList = new LinkedList<NewsTO>();

		NewsTO news = initNews();

		newsList.add(news);
		
		when(newsDAO.list()).thenReturn(newsList);

		List<NewsTO> resultNews = service.list();

		Assert.assertNotNull(resultNews);
		Assert.assertEquals(1, resultNews.size());

		verify(newsDAO).list();

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testListThrowsException() throws Exception {

		when(newsDAO.list()).thenThrow(DAOException.class);
		service.list();

	}

	private NewsTO initNews() {

		NewsTO news = new NewsTO();
		news.setId(new Long(1));
		news.setTitle("test_title");
		news.setFullText("test_full");
		news.setShortText("test_short");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		
		return news;
	}

}
