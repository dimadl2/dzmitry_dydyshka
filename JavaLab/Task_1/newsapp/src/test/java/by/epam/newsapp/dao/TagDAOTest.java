package by.epam.newsapp.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.newsapp.dto.TagTO;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TagDAOTest {

    public static final String PATH_TO_TAGS = "/tag/tag.xml";

    @Autowired
    private TagDAO tagDAO;

    private TagTO expected;

    @Before
    public void setUp() {

        expected = new TagTO(1L, "Tag1");

    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testList() throws Exception {

        List<TagTO> tags = tagDAO.list();
        Assert.assertNotNull(tags);

        for (TagTO tag : tags) {

            Assert.assertNotNull(tag);

            if (tag.getId() == 1L) {
                assertTags(expected, tag);
            }

        }

    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testAdd() throws Exception {

        TagTO tag = new TagTO();
        tag.setTagName("Tag4");

        Long id = tagDAO.add(tag);
        TagTO actual = tagDAO.fetchById(id);
        Assert.assertEquals(tag.getTagName(), actual.getTagName());


    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testDelete() throws Exception {

        tagDAO.delete(3L);
        TagTO actual = tagDAO.fetchById(3L);
        Assert.assertNull(actual);


    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testUpdate() throws Exception {

        TagTO tag = new TagTO();
        tag.setId(1L);
        tag.setTagName("TagUpdate");

        tagDAO.update(tag);

        TagTO actual = tagDAO.fetchById(1L);
        assertTags(tag, actual);
    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testFetchById() throws Exception {

        TagTO actual = tagDAO.fetchById(1L);

        Assert.assertNotNull(actual);
        assertTags(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testFetchTagByNews() throws Exception {

        List<TagTO> tags = tagDAO.fetchTagByNews(1L);

        Assert.assertNotNull(tags);
        Assert.assertEquals(2, tags.size());

        for (TagTO tag : tags) {

            Assert.assertNotNull(tag);
            if (tag.getId() == 1L){
                assertTags(expected, tag);
            }

        }

    }

    private void assertTags(TagTO expected, TagTO actual) {

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getTagName(), actual.getTagName());

    }

}
