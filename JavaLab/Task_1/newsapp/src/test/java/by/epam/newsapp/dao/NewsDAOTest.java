package by.epam.newsapp.dao;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.newsapp.dto.NewsTO;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "/news/news.xml")
public class NewsDAOTest {

	private static final String PATH_TO_NEWS = "/news/news.xml";

	@Autowired
	private NewsDAO newsDAO;

    private NewsTO expected;

    @Before
    public void setUp() throws Exception{

        expected = new NewsTO();
        expected.setShortText("test short");
        expected.setFullText("test full");
        expected.setTitle("test title");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        Date date = dateFormat.parse("01/01/2015 11:10:00");
        expected.setCreationDate(date);
        expected.setModificationDate(date);
        expected.setId(1L);

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testList() throws Exception {

        List<NewsTO> list = newsDAO.list();

        Assert.assertNotNull(list);

        for (NewsTO news : list) {

            Assert.assertNotNull(news);

            if (news.getId() == 1L){

               assertNews(expected, news);

            }

        }

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testSave() throws Exception {


        Long id = newsDAO.add(expected);

        NewsTO actual = newsDAO.fetchById(id);

        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expected.getFullText(), actual.getFullText());
        Assert.assertEquals(expected.getShortText(), actual.getShortText());
        Assert.assertEquals(expected.getModificationDate(), actual.getModificationDate());
        Assert.assertEquals(expected.getTitle(), actual.getTitle());

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testDelete() throws Exception {

		newsDAO.delete(3L);
        NewsTO actual = newsDAO.fetchById(3L);
        Assert.assertNull(actual);

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testUpdate() throws Exception {

		NewsTO news = new NewsTO();
		news.setId(3l);
		news.setShortText("new");
		news.setFullText("new");
		news.setTitle("new");
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd/MM/yyyy HH:mm:ss");
		Date date = dateFormat.parse("01/01/2015 11:10:00");
		news.setCreationDate(date);
		news.setModificationDate(date);

		newsDAO.update(news);

        NewsTO actual = newsDAO.fetchById(3L);
        assertNews(news, actual);

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testFetchById() throws Exception {

		NewsTO actual = newsDAO.fetchById(1L);
        Assert.assertNotNull(actual);
        assertNews(expected, actual);


	}

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testSearchByAuthor() throws Exception {

		List<NewsTO> news = newsDAO.searchByAuthor(2L);

		Assert.assertNotNull(news);
		Assert.assertEquals(2, news.size());

		for (NewsTO itemNews : news) {

			Assert.assertNotNull(itemNews);
            if(itemNews.getId() == 1L){
                assertNews(expected, itemNews);
            }

		}

	}

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testSearchByTag() throws Exception {

		List<Long> tagsId = Arrays.asList(1L, 2L);
		List<NewsTO> news = newsDAO.searchByTags(tagsId);

		Assert.assertNotNull(news);
		Assert.assertEquals(1, news.size());

		for (NewsTO itemNews : news) {

			Assert.assertNotNull(itemNews);
            if (itemNews.getId() == 1L){
                assertNews(expected, itemNews);
            }

		}
	}

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testAttachAuthorToNews() throws Exception {

        newsDAO.attachAuthorToNews(1L, 2L);
        List<NewsTO> newsList = newsDAO.searchByAuthor(1L);
        boolean flag = false;
        for (NewsTO news : newsList) {
            flag = true;
        }

        Assert.assertTrue(flag);


    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testAttachTagToNews() throws Exception {

		newsDAO.attachTagToNews(3L, 1L);
        List<Long> tagsId = Arrays.asList(2L, 3L);
        List<NewsTO> actualListNews = newsDAO.searchByTags(tagsId);
        boolean flag = false;
        for (NewsTO news : actualListNews) {

            if (news.getId() == 1L){
                flag = true;
            }

        }

        Assert.assertTrue(flag);

	}

    private void assertNews(NewsTO expected, NewsTO actual){

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expected.getFullText(), actual.getFullText());
        Assert.assertEquals(expected.getShortText(), actual.getShortText());
        Assert.assertEquals(expected.getModificationDate(), actual.getModificationDate());
        Assert.assertEquals(expected.getTitle(), actual.getTitle());

    }



}
