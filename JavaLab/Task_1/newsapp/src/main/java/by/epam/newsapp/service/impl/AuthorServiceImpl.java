package by.epam.newsapp.service.impl;

import java.util.List;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.service.AuthorService;
import org.apache.log4j.Logger;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;

/**
 * The Class AuthorServiceImpl.
 */
public class AuthorServiceImpl implements AuthorService {

	/** The author dao. */
	private AuthorDAO authorDAO;

	private static Logger log = Logger.getLogger(AuthorServiceImpl.class);

	/**
	 * Sets the author dao.
	 *
	 * @param authorDAO
	 *            the authorDAO to set
	 */
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#add(Object)
	 * 
	 */
	@Override
	public Long save(AuthorTO author) throws TechnicalException {

		Long id = null;

		try {

			id = authorDAO.add(author);

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return id;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#delete(Long)
	 * 
	 */
	@Override
	public boolean delete(Long authorId) throws TechnicalException {

		boolean flagResult = false;
		try {

			AuthorTO author = authorDAO.fetchById(authorId);
			if (author != null) {

				authorDAO.delete(authorId);
				flagResult = true;
			} 

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return flagResult;
	}

	/**
	 * @see by.epam.newsapp.service.Service#update(Object)
	 */
	@Override
	public boolean update(AuthorTO author) throws TechnicalException {

		boolean flagResult = false;

		try {

			AuthorTO updatedAuthor = authorDAO.fetchById(author.getId());

			if (updatedAuthor != null) {

				authorDAO.update(author);
				flagResult = true;

			}

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#fetchById(Long)
	 */
	@Override
	public AuthorTO fetchById(Long id) throws TechnicalException {

		AuthorTO author = null;

		try {

			author = authorDAO.fetchById(id);

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return author;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#list()
	 */
	@Override
	public List<AuthorTO> list() throws TechnicalException {

		List<AuthorTO> list = null;

		try {

			list = authorDAO.list();

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return list;

	}

    @Override
    public List<AuthorTO> fetchAuthorsByNews(Long newsId) throws TechnicalException {

        List<AuthorTO> authors;
        try {
            authors = authorDAO.fetchAuthorsByNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return authors;

    }
}
