package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.TechnicalException;

public interface AuthorService extends Service<AuthorTO> {
	
	List<AuthorTO> fetchAuthorsByNews(Long newsId) throws TechnicalException;

}
