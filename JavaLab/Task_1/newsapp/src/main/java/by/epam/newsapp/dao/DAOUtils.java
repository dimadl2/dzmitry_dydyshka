package by.epam.newsapp.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import by.epam.newsapp.exception.DAOException;

/**
 * Class methods are useful for working for with database connections,
 * statements and result sets .
 */
public class DAOUtils {

	public static void closeResources(Connection conn, Statement st,
			ResultSet rs) throws DAOException {
		try {
			try {
				try {
					if (rs != null) {
						rs.close();
					}
				} finally {
					if (st != null) {
						st.close();
					}
				}
			} finally {
				if (conn != null) {
					conn.close();
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

}
