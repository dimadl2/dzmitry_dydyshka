package by.epam.newsapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.DAOException;

/**
 * The Class provides access to Tag table in database.
 */
public class TagDAO extends AbstractDAO<TagTO> {

    //SQL-query
	private static final String SQL_FETCH_TAGS_BY_NEWS = "SELECT tag.tag_id, tag.tag_name " +
                                                            "FROM news_tag " +
                                                                "JOIN tag " +
                                                                    "ON news_tag.tag_id = tag.tag_id " +
                                                            "WHERE news_id = ?";
    private static final String COLUMN_TAG_ID = "tag_id";
    private static final String COLUMN_TAG_NAME = "tag_name";

    /**
	 * Fetch tag by news.
	 *
	 * @param newsId the news id
	 * @return the list tags
	 * @throws DAOException the DAO exception
	 */
	public List<TagTO> fetchTagByNews(Long newsId) throws DAOException {

		List<TagTO> tags = new LinkedList<TagTO>();

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_TAGS_BY_NEWS);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				TagTO tag = new TagTO();

				tag.setId(resultSet.getLong(COLUMN_TAG_ID));
				tag.setTagName(resultSet.getString(COLUMN_TAG_NAME));

				tags.add(tag);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return tags;

	}

}
