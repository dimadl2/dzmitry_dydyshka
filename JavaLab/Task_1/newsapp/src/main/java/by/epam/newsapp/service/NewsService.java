package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.TechnicalException;

public interface NewsService extends Service<NewsTO>{
	
	List<NewsTO> searchByAuthor(Long authorId) throws TechnicalException;
	List<NewsTO> searchByTags(List<Long> tagsId) throws TechnicalException;
	Long attachAuthorToNews(Long authorId, Long newsId) throws TechnicalException;
	Long attachTagToNews(Long tagId, Long newsId) throws TechnicalException;
	void disconnectAllTagsOnNews(Long newsId) throws TechnicalException;
	List<NewsTO> getAllWithSortedByComments() throws TechnicalException;

}
