package by.epam.newsapp.service.impl;

import java.util.List;

import by.epam.newsapp.service.NewsService;
import org.apache.log4j.Logger;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;

/**
 * The Class NewsServiceImpl.
 */
public class NewsServiceImpl implements NewsService {


    /** The news dao. */
	private NewsDAO newsDAO;

	/** The comment dao. */
	private CommentDAO commentDAO;

	/** The tag dao. */
	private TagDAO tagDAO;

	/** The author dao. */
	private AuthorDAO authorDAO;

	/** The log. */
	private static Logger log = Logger.getLogger(NewsServiceImpl.class);

	/**
	 * Sets the author dao.
	 *
	 * @param authorDAO
	 *            the authorDAO to set
	 */
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	/**
	 * Sets the tag dao.
	 *
	 * @param tagDAO
	 *            the tagDAO to set
	 */
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/**
	 * Sets the comment dao.
	 *
	 * @param commentDAO
	 *            the commentDAO to set
	 */
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/**
	 * Sets the news dao.
	 *
	 * @param newsDAO
	 *            the newsDAO to set
	 */
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.newsapp.service.Service#save(by.epam.newsapp.dto.Entity)
	 */
	@Override
	public Long save(NewsTO news) throws TechnicalException {

		Long newsId = null;
		try {

			newsId = newsDAO.add(news);

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return newsId;
	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#delete(Long)
	 */
	@Override
	public boolean delete(Long newsId) throws TechnicalException {

		boolean flagResult = false;
		try {

			NewsTO news = newsDAO.fetchById(newsId);
			if (news != null) {

				newsDAO.delete(newsId);
				flagResult = true;

			}

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#update(by.epam.newsapp.dto.Entity)
	 */
	@Override
	public boolean update(NewsTO news) throws TechnicalException {

		boolean flagResult = false;

		try {

			NewsTO updatedNews = newsDAO.fetchById(news.getId());

			if (updatedNews != null) {

				newsDAO.update(news);
				flagResult = true;

			}

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#fetchById(Long)
	 */
	@Override
	public NewsTO fetchById(Long newsId) throws TechnicalException {

		NewsTO news = null;
		try {

			news = newsDAO.fetchById(newsId);

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return news;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#list()
	 */
	@Override
	public List<NewsTO> list() throws TechnicalException {
		List<NewsTO> list = null;
		try {
			list = newsDAO.list();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return list;
	}

    @Override
    public List<NewsTO> searchByAuthor(Long authorId) throws TechnicalException {

        List<NewsTO> newses;
        try {
            newses = newsDAO.searchByAuthor(authorId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
        return newses;
    }

    @Override
    public List<NewsTO> searchByTags(List<Long> tagsId) throws TechnicalException {

        List<NewsTO> newses;
        try {
            newses = newsDAO.searchByTags(tagsId);
        } catch (DAOException e) {
            throw new TechnicalException();
        }

        return newses;
    }

    @Override
    public Long attachAuthorToNews(Long authorId, Long newsId) throws TechnicalException {

        Long id;
        try {
            id = newsDAO.attachAuthorToNews(authorId, newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return id;
    }

    @Override
    public Long attachTagToNews(Long tagId, Long newsId) throws TechnicalException {
        Long id;
        try {
            id = newsDAO.attachTagToNews(tagId, newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return id;
    }

    @Override
    public void disconnectAllTagsOnNews(Long newsId) throws TechnicalException {

        try {
            newsDAO.disconnectAllTagsOnNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    @Override
    public List<NewsTO> getAllWithSortedByComments() throws TechnicalException {

        List<NewsTO> newses;
        try {
            newses = newsDAO.getAllWithSortedByComments();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
        return newses;
    }

}
