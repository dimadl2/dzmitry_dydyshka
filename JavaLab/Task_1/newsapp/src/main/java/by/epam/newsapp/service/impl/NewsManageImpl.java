package by.epam.newsapp.service.impl;

import by.epam.newsapp.dto.*;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.CommentService;
import by.epam.newsapp.service.NewsManage;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 3/25/2015.
 */
public class NewsManageImpl implements NewsManage {

    private NewsServiceImpl newsService;
    private AuthorServiceImpl authorService;
    private CommentService commentService;
    private TagServiceImpl tagService;

    public void setNewsService(NewsServiceImpl newsService) {
        this.newsService = newsService;
    }

    public void setAuthorService(AuthorServiceImpl authorService) {
        this.authorService = authorService;
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    public void setTagService(TagServiceImpl tagService) {
        this.tagService = tagService;
    }

    @Override
    public Long save(NewsVO newsVO) throws TechnicalException {

        Long newsId = null;

        NewsTO newsTO = newsVO.getNewsTO();
        newsId = newsService.save(newsTO);

        List<AuthorTO> authors = newsVO.getAuthors();
        List<TagTO> tags = newsVO.getTags();

        for (AuthorTO author : authors) {

            newsService.attachAuthorToNews(author.getId(), newsId);

        }

        for (TagTO tag : tags) {

            newsService.attachTagToNews(tag.getId(), newsId);
        }


        return newsId;
    }

    @Override
    public NewsVO fetchById(Long newsId) throws TechnicalException {

        NewsTO newsTO = newsService.fetchById(newsId);

        List<CommentTO> comments = commentService.fetchCommentsByNews(newsId);
        List<TagTO> tags = tagService.fetchTagByNews(newsId);
        List<AuthorTO> authors = authorService.fetchAuthorsByNews(newsId);

        NewsVO newsVO = new NewsVO();
        newsVO.setNewsTO(newsTO);
        newsVO.setAuthors(authors);
        newsVO.setComments(comments);
        newsVO.setTags(tags);

        return newsVO;
    }

    @Override
    public List<NewsVO> getAll() throws TechnicalException {

        List<NewsVO> listNewsVO = new LinkedList<>();
        List<NewsTO> listNewsTO = newsService.list();
        for (NewsTO newsTO : listNewsTO) {

            Long newsId = newsTO.getId();
            List<TagTO> tags = tagService.fetchTagByNews(newsId);
            List<AuthorTO> authors = authorService.fetchAuthorsByNews(newsId);
            int count = commentService.getCountCommentsOnNews(newsId);

            NewsVO newsVO = new NewsVO();
            newsVO.setNewsTO(newsTO);
            newsVO.setTags(tags);
            newsVO.setAuthors(authors);
            newsVO.setCountComments(count);

            listNewsVO.add(newsVO);
        }

        return listNewsVO;
    }
}
