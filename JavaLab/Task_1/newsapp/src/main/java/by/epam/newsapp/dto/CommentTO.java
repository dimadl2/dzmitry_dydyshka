package by.epam.newsapp.dto;

import java.util.Date;

import by.epam.newsapp.annotation.Column;
import by.epam.newsapp.annotation.Table;

/**
 * The Class Comment.
 */
@Table(name = "comments")
public class CommentTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The comment text. */
	@Column(name = "comment_text")
	private String commentText;

	/** The creation date. */
	@Column(name = "creation_date")
	private Date creationDate;

	/** The news id. */
	@Column(name = "news_id")
	private Long newsId;

	/**
	 * Instantiates a new comment.
	 */
	public CommentTO() {

	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param id
	 *            the id
	 * @param commentText
	 *            the comment text
	 * @param creationDate
	 *            the creation date
	 * @param newsId
	 *            the news id
	 */
	public CommentTO(Long id, String commentText, Date creationDate, Long newsId) {
		super(id);
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the commentText
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text.
	 *
	 * @param commentText
	 *            the commentText to set
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the newsId
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the newsId to set
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentTO other = (CommentTO) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

}
