package by.epam.newsapp.service;

import by.epam.newsapp.dto.NewsVO;
import by.epam.newsapp.exception.TechnicalException;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 3/25/2015.
 */
public interface NewsManage {

    Long save (NewsVO newsVO) throws TechnicalException;
    NewsVO fetchById(Long id) throws TechnicalException;
    List<NewsVO> getAll() throws TechnicalException;


}
