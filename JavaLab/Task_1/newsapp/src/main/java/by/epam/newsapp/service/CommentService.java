package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.exception.TechnicalException;

public interface CommentService extends Service<CommentTO> {

	List<CommentTO> fetchCommentsByNews(Long newsId);
	void delete(List<Long> listId) throws TechnicalException;
	int getCountCommentsOnNews(Long newsId) throws TechnicalException;
}
