package by.epam.newsapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.DAOException;

/**
 * The Class provides access to News table in database.
 */
public class NewsDAO extends AbstractDAO<NewsTO> {

	// Column names
	private static final String COLUMN_MODIFICATION_DATE = "modification_date";
	private static final String COLUMN_CREATION_DATE = "creation_date";
	private static final String COLUMN_TITLE = "title";
	private static final String COLUMN_FULL_TEXT = "full_text";
	private static final String COLUMN_SHORT_TEXT = "short_text";
	private static final String COLUMN_NEWS_ID = "news_id";

	// Column id name
	private static final String NEWS_AUTHOR_ID = "news_author_id";
	private static final String NEWS_TAG_ID = "news_tag_id";

	// SQL-queries
	private static final String SQL_LIST_SORTED_NEWS = "SELECT news.news_id, news.title, news.short_text, " +
                                                                "news.full_text, news.creation_date, " +
                                                                "news.modification_date, count(comments.comments_id) as count " +
                                                        "FROM news " +
                                                            "LEFT JOIN comments " +
                                                                "ON news.news_id = comments.news_id " +
                                                        "GROUP BY news.news_id, news.title, news.short_text, " +
                                                                    "news.full_text, news.creation_date, " +
                                                                    "news.modification_date " +
                                                        "ORDER BY count DESC";

	private static final String SQL_DELETE_ALL_TAGS_ON_NEWS = "DELETE FROM news_tag WHERE news_id = ?";
	private static final String SQL_ADD_TAG_TO_NEWS = "INSERT INTO news_tag VALUES(Null, ?, ?)";
	private static final String SQL_ADD_AUTHOR_TO_NEWS = "INSERT INTO news_author VALUES (NULL, ?, ?)";
	private static final String SQL_SEARCH_NEWS_BY_AUTHOR = "SELECT * FROM news_author JOIN news USING(news_id) WHERE author_id = ?";

	/**
	 * Search news by author.
	 *
	 * @param authorId
	 *            the author id
	 * @return the list of news
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<NewsTO> searchByAuthor(Long authorId) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<NewsTO> listNews = new LinkedList<NewsTO>();

		try {

			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_SEARCH_NEWS_BY_AUTHOR);
			statement.setLong(1, authorId);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				NewsTO news = createFromResultSet(resultSet);
				listNews.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);

		}

		return listNews;

	}

	/**
	 * Search news by tags.
	 *
	 * @param tagsId
	 *            the tags id
	 * @return the list of news
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<NewsTO> searchByTags(List<Long> tagsId) throws DAOException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<NewsTO> listNews = new LinkedList<NewsTO>();

		try {
			connection = dataSource.getConnection();

			String query = generateSearchByTagQuery(tagsId);

			statement = connection.createStatement();
			resultSet = statement.executeQuery(query.toString());

			while (resultSet.next()) {

				NewsTO news = createFromResultSet(resultSet);
				listNews.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);

		}

		return listNews;

	}

	/**
	 * Attach author to news.
	 *
	 * @param authorId
	 *            the author id
	 * @param newsId
	 *            the news id
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
	public Long attachAuthorToNews(Long authorId, Long newsId)
			throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		Long id = null;

		try {

			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_ADD_AUTHOR_TO_NEWS,
					new String[] { NEWS_AUTHOR_ID });

			statement.setLong(1, newsId);
			statement.setLong(2, authorId);

			statement.executeUpdate();

			resultSet = statement.getGeneratedKeys();
			resultSet.next();
			id = resultSet.getLong(1);

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return id;
	}

	/**
	 * Attach tag to news.
	 *
	 * @param tagId
	 *            the tag id
	 * @param newsId
	 *            the news id
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
	public Long attachTagToNews(Long tagId, Long newsId) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		Long id = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_ADD_TAG_TO_NEWS,
					new String[] { NEWS_TAG_ID });

			statement.setLong(1, newsId);
			statement.setLong(2, tagId);

			statement.executeUpdate();

			resultSet = statement.getGeneratedKeys();
			resultSet.next();
			id = resultSet.getLong(1);

		} catch (SQLException e) {
			throw new DAOException();
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return id;

	}

	/**
	 * Disconnect all tags on news.
	 *
	 * @param newsId
	 *            the news id
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void disconnectAllTagsOnNews(Long newsId) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_DELETE_ALL_TAGS_ON_NEWS);
			statement.setLong(1, newsId);
			statement.executeQuery();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, null);

		}

	}

	/**
	 * Gets the all sorted news by comments.
	 *
	 * @return the list sorted news by comments.
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<NewsTO> getAllWithSortedByComments() throws DAOException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<NewsTO> newsList = new LinkedList<NewsTO>();

		try {
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_LIST_SORTED_NEWS);

			while (resultSet.next()) {

				NewsTO news = createFromResultSet(resultSet);
				newsList.add(news);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);

		}

		return newsList;

	}

	private String generateSearchByTagQuery(List<Long> tagsId) {
		StringBuilder query = new StringBuilder(
				"SELECT DISTINCT news.*" +
                        " FROM news_tag" +
                            " JOIN news" +
                                " ON news_tag.news_id = news.news_id" +
                        " WHERE tag_id = ANY(");

		Iterator<Long> itr = tagsId.listIterator();

		while (itr.hasNext()) {

			query.append(itr.next());
			if (itr.hasNext()) {

				query.append(", ");
			}

		}

		query.append(")");
		return query.toString();
	}

	/**
	 * Creates news from result set.
	 *
	 * @param resultSet
	 *            the result set
	 * @return the news
	 * @throws java.sql.SQLException
	 *             the SQL exception
	 */
	private NewsTO createFromResultSet(ResultSet resultSet) throws SQLException {

		NewsTO news = new NewsTO();

		news.setId(resultSet.getLong(COLUMN_NEWS_ID));
		news.setShortText(resultSet.getString(COLUMN_SHORT_TEXT));
		news.setFullText(resultSet.getString(COLUMN_FULL_TEXT));
		news.setTitle(resultSet.getString(COLUMN_TITLE));
		news.setCreationDate(resultSet.getTimestamp(COLUMN_CREATION_DATE));
		news.setModificationDate(resultSet
				.getTimestamp(COLUMN_MODIFICATION_DATE));

		return news;

	}

}
