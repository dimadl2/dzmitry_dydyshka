package by.epam.newsapp.dto;

import by.epam.newsapp.annotation.Column;
import by.epam.newsapp.annotation.Table;

/**
 * The Class Tag.
 */
@Table(name = "tag")
public class TagTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The tag name. */
	@Column(name = "tag_name")
	private String tagName;

	/**
	 * Instantiates a new tag.
	 */
	public TagTO() {

	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param id
	 *            the id
	 * @param tagName
	 *            the tag name
	 */
	public TagTO(Long id, String tagName) {
		super(id);
		this.tagName = tagName;
	}

	/**
	 * Gets the tag name.
	 *
	 * @return the tagName
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the tag name.
	 *
	 * @param tagName
	 *            the tagName to set
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagTO other = (TagTO) obj;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

}
