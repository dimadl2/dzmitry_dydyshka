package by.epam.newsapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.DAOException;

/**
 * The Class AuthorDAO.
 * 
 * 
 * 
 */
public class AuthorDAO extends AbstractDAO<AuthorTO> {

    /** SQL-query for fetch authors by news id. */
	private static final String SQL_FETCH_BY_NEWS = "SELECT author.* FROM news_author JOIN author ON news_author.author_id = author.author_id WHERE news_id = ?";
    private static final String COLUMN_AUTHOR_ID = "author_id";
    private static final String COLUMN_NAME = "name";


    /**
	 * Fetch authors by news id.
	 *
	 * @param newsId
	 *            the news id search
	 * @return the list of authors
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<AuthorTO> fetchAuthorsByNews(Long newsId) throws DAOException {

		List<AuthorTO> authors = new LinkedList<AuthorTO>();

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_BY_NEWS);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				AuthorTO author = new AuthorTO();

				author.setId(resultSet.getLong(COLUMN_AUTHOR_ID));
				author.setName(resultSet.getString(COLUMN_NAME));

				authors.add(author);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return authors;

	}

}
