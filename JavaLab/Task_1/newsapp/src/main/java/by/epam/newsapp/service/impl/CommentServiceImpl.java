package by.epam.newsapp.service.impl;

import java.util.List;

import by.epam.newsapp.service.CommentService;
import org.apache.log4j.Logger;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;

/**
 * The Class CommentServiceImpl.
 */
public class CommentServiceImpl implements CommentService {

	/** The comment data access object. */
	private CommentDAO commentDAO;

	/** The log. */
	private static Logger log = Logger.getLogger(CommentServiceImpl.class);

	/**
	 * Sets the comment data access object.
	 *
	 * @param commentDAO
	 *            the commentDAO to set
	 */
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/**
	 * @see by.epam.newsapp.service.Service#save(by.epam.newsapp.dto.Entity)
	 */
	@Override
	public Long save(CommentTO comment) throws TechnicalException {

		Long id = null;

		try {
			id = commentDAO.add(comment);
		} catch (DAOException e) {

			throw new TechnicalException(e);

		}

		return id;

	}

	/**
	 * @see by.epam.newsapp.service.Service#delete(Long)
	 */
	@Override
	public boolean delete(Long id) throws TechnicalException {

		boolean flagResult = false;
		try {

			CommentTO comment = commentDAO.fetchById(id);
			if (comment != null) {

				commentDAO.delete(id);
				flagResult = true;

			}

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * @see by.epam.newsapp.service.Service#update(by.epam.newsapp.dto.Entity)
	 */
	@Override
	public boolean update(CommentTO comment) throws TechnicalException {

		boolean flagResult = false;
		try {

			CommentTO updatedComment = commentDAO.fetchById(comment.getId());

			if (updatedComment != null) {

				commentDAO.update(comment);
				flagResult = true;
			}

		} catch (DAOException e) {

			throw new TechnicalException(e);

		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#fetchById(Long)
	 */
	@Override
	public CommentTO fetchById(Long id) throws TechnicalException {

		CommentTO comment = null;

		try {
			comment = commentDAO.fetchById(id);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return comment;
	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#list()
	 */
	@Override
	public List<CommentTO> list() throws TechnicalException {
		List<CommentTO> list = null;
		try {
			list = commentDAO.list();
		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return list;
	}

    @Override
    public List<CommentTO> fetchCommentsByNews(Long newsId) {

        List<CommentTO> comments = null;
        try {
            comments = commentDAO.fetchCommentsByNews(newsId);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return comments;

    }

    @Override
    public void delete(List<Long> listId) throws TechnicalException {

        try {
            commentDAO.delete(listId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    @Override
    public int getCountCommentsOnNews(Long newsId) throws TechnicalException {

        int count;
        try {
            count = commentDAO.getCountCommentsOnNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return count;
    }
}
