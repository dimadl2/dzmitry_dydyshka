package by.epam.newsapp.service.impl;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.TagService;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * The Class TagServiceImpl.
 */
public class TagServiceImpl implements TagService {


    /** The tag dao. */
	private TagDAO tagDAO;

	/** The log. */
	private static Logger log = Logger.getLogger(TagServiceImpl.class);

	/**
     *
	 * Sets the tag dao.
	 *
	 * @param tagDAO
	 *            the tagDAO to set
	 */
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/**
	 * @see by.epam.newsapp.service.Service#save(by.epam.newsapp.dto.Entity)
	 */
	@Override
	public Long save(TagTO tag) throws TechnicalException {

		Long id = null;

		try {
			id = tagDAO.add(tag);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return id;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#delete(Long)
	 */
	@Override
	public boolean delete(Long tagId) throws TechnicalException {
		boolean flagResult = false;
		try {

			TagTO tag = tagDAO.fetchById(tagId);
			if (tag != null) {

				tagDAO.delete(tagId);
				flagResult = true;

			}

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#update(by.epam.newsapp.dto.Entity)
	 */
	@Override
	public boolean update(TagTO tag) throws TechnicalException {

		boolean flagResult = false;

		try {

			TagTO updatedTag = tagDAO.fetchById(tag.getId());

			if (updatedTag != null) {

				tagDAO.update(tag);
				flagResult = true;

			}
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#fetchById(Long)
	 */
	@Override
	public TagTO fetchById(Long tagId) throws TechnicalException {

		TagTO tag = null;

		try {
			tag = tagDAO.fetchById(tagId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return tag;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#list()
	 */
	@Override
	public List<TagTO> list() throws TechnicalException {

		List<TagTO> tags = null;

		try {
			tags = tagDAO.list();
		} catch (DAOException e) {

			throw new TechnicalException(e);

		}

		return tags;

	}

    @Override
    public List<TagTO> fetchTagByNews(Long newsId) throws TechnicalException {
        List<TagTO> tags;
        try {
            tags = tagDAO.fetchTagByNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return tags;
    }
}
