package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.TechnicalException;

public interface TagService extends Service<TagTO> {
	
	List<TagTO> fetchTagByNews(Long newsId) throws TechnicalException;

}
