package by.epam.newsadmin.util;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Dzmitry_Dydyshka on 4/27/2015.
 */
public class DateUtils {


    private static final String PATTERN_DATE = "pattern.date";
    private static final String LABEL = "label";

    public static SimpleDateFormat getFormat(String locale){


        String pattern = getValueFromBundle(locale);

        return new SimpleDateFormat(pattern);

    }

    public static String getStringFormat(String locale){

        return getValueFromBundle(locale);
    }

    private static String getValueFromBundle(String locale){

        ResourceBundle labels = ResourceBundle.getBundle(
                LABEL, new Locale(locale));

        return labels.getString(DateUtils.PATTERN_DATE);

    };
}
