package by.epam.newsadmin.controller;

import by.epam.newsadmin.util.StringDUtils;
import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/7/2015.
 */
@Controller
public class EditTagController {

    private static final String URL_TAGS = "/tags";
    private static final String URL_TAGS_ADD = "/tags/add";
    private static final String URL_TAGS_UPDATE = "/tags/update";
    private static final String URL_TAGS_DELETE = "/tags/delete";
    private static final String REDIRECT = "redirect:";
    private static final String TAGS = "tags";

    @Autowired
    private TagService tagService;

    @RequestMapping(URL_TAGS)
    public ModelAndView loadPage() throws Exception{

        ModelAndView modelAndView = new ModelAndView();

        List<Tag> tags = tagService.list();

        modelAndView.addObject(TAGS, tags);
        modelAndView.setViewName(TAGS);
        return  modelAndView;

    }

    @RequestMapping(URL_TAGS_ADD)
    public ModelAndView addTag(@RequestParam("new-tag") String nameTag) throws Exception{


        nameTag = StringDUtils.escape(nameTag);

        Tag tag = new Tag(null, nameTag);

        tagService.save(tag);

        return new ModelAndView(REDIRECT + URL_TAGS);

    }

    @RequestMapping(URL_TAGS_UPDATE)
    public ModelAndView updateTag(@RequestParam String name,
                                  @RequestParam("tag-id") Long tagId) throws Exception{

        name = StringDUtils.escape(name);

        Tag tag = new Tag(tagId, name);

        tagService.update(tag);

        return new ModelAndView(REDIRECT + URL_TAGS);

    }

    @RequestMapping(URL_TAGS_DELETE)
    public ModelAndView deleteTag(@RequestParam("tag-id") Long tagId) throws Exception{

        tagService.delete(tagId);

        return new ModelAndView(REDIRECT + URL_TAGS);

    }

}
