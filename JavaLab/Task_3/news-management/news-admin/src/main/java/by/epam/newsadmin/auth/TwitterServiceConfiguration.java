package by.epam.newsadmin.auth;

/**
 * Created by Dzmitry_Dydyshka on 9/1/2015.
 */
public class TwitterServiceConfiguration {

    private String consumerKey;
    private String consumerSecret;
    private String screenName;

    private String urlObtainNews;

    public TwitterServiceConfiguration(){

    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getUrlObtainNews() {
        return urlObtainNews;
    }

    public void setUrlObtainNews(String urlObtainNews) {
        this.urlObtainNews = urlObtainNews;
    }
}
