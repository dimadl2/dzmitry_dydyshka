package by.epam.newsadmin.auth;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by Dzmitry_Dydyshka on 9/1/2015.
 */
@Component
public class OAuth {

    @Autowired
    private TwitterServiceConfiguration configuration;

    public String getBearerAccessToken(){

        Client client = Client.create();

        byte[] encodedBearerToken = Base64.encode(configuration.getConsumerKey()+":"+configuration.getConsumerSecret());

        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("grant_type", "client_credentials");

        WebResource webResource = client.resource("https://api.twitter.com/oauth2/token");
        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED)
                .header("Authorization", "Basic " + new String(encodedBearerToken))
                .post(ClientResponse.class, formData);


        JsonObject parse = Json.parse(response.getEntity(String.class)).asObject();

        return parse.get("access_token").asString();

    }

    public TwitterServiceConfiguration getConfiguration() {
        return configuration;
    }
}
