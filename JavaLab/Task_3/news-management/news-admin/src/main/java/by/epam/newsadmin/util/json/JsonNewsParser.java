package by.epam.newsadmin.util.json;

import by.epam.newsadmin.util.DateUtils;
import by.epam.newsapp.dto.Author;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.dto.Tag;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static by.epam.newsadmin.util.json.ConstatnsJsonKey.*;

/**
 * @author Dzmitry_Dydyshka.
 */
public final class JsonNewsParser {


    public static News parseJsonToNews(JsonObject newsJsnObj) throws ParseException {

        News news = new News();
        news.setTitle(newsJsnObj.get(JSON_KEY_TITLE).asString());
        news.setShortText(newsJsnObj.get(JSON_KEY_SHORT_TEXT).asString());
        news.setFullText(newsJsnObj.get(JSON_KEY_FULL_TEXT).asString());
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
        Date creationDate = format.parse(newsJsnObj.get(JSON_KEY_CREATION_DATE).asString());
        news.setCreationDate(creationDate);
        news.setModificationDate(new Timestamp(creationDate.getTime()));

        return news;

    }


    public static JsonObject parseListNewsToJson(List<News> news, String locale) {


        JsonObject resultJson = new JsonObject();
        JsonArray newsArray = new JsonArray();

        for (News aNew : news) {
            JsonObject newsObj = new JsonObject();
            newsObj.add(JSON_KEY_ID, aNew.getId());
            newsObj.add(JSON_KEY_TITLE, aNew.getTitle());
            newsObj.add(JSON_KEY_SHORT_TEXT, aNew.getShortText());
            newsObj.add(JSON_KEY_FULL_TEXT, aNew.getFullText());

            SimpleDateFormat format = DateUtils.getFormat(locale);

            newsObj.add(JSON_KEY_MOD_DATE, format.format(aNew.getModificationDate()));
            newsObj.add(JSON_KEY_COMMENTS, aNew.getNumberOfComments());
            for (Author author : aNew.getAuthors()) {
                newsObj.add(JSON_KEY_AUTHOR, author.getName());
            }
            JsonArray tags = new JsonArray();
            for (Tag tag : aNew.getTags()) {
                tags.add(tag.getTagName());
            }


            newsObj.add(JSON_KEY_TAGS, tags);
            newsArray.add(newsObj);

        }

        resultJson.add(JSON_KEY_NEWS, newsArray);
        return resultJson.asObject();

    }


}
