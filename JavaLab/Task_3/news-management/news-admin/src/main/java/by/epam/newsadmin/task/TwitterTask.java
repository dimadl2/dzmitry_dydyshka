package by.epam.newsadmin.task;

import by.epam.newsadmin.auth.OAuth;
import by.epam.newsadmin.util.json.JsonNewsParser;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.service.NewsManage;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import static by.epam.newsadmin.util.json.ConstatnsJsonKey.*;

import javax.ws.rs.core.MediaType;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dzmitry_Dydyshka.
 */
@Component
class TwitterTask {

    //Query param names
    private static final String QUERY_PARAM_TOKEN = "access_token";
    private static final String QUERY_PARAM_NAME = "screen_name";
    private static final String QUERY_PARAM_SINCE_ID = "since_id";

    private static long lastTweetID = 0;

    @Autowired
    private OAuth auth;

    @Autowired
    private NewsManage newsManage;

    @Scheduled(fixedRate = 300_000)
    public void obtainNews() throws Exception {

        Client client = Client.create();
        WebResource webResource = client.resource(auth.getConfiguration().getUrlObtainNews())
                .queryParam(QUERY_PARAM_TOKEN, URLEncoder.encode(auth.getBearerAccessToken(), "UTF-8"))
                .queryParam(QUERY_PARAM_NAME, URLEncoder.encode(auth.getConfiguration().getScreenName(), "UTF-8"));

        if (lastTweetID != 0) {
            webResource = webResource.queryParam(QUERY_PARAM_SINCE_ID, String.valueOf(lastTweetID));
        }

        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED)
                .get(ClientResponse.class);


        String data = response.getEntity(String.class);

        JsonObject newsObject = Json.parse(data).asObject();

        lastTweetID = newsObject.get(JSON_KEY_LAST_ID).asLong();

        for (JsonValue newsJsn : newsObject.get(JSON_KEY_NEWS).asArray()) {

            JsonObject newsJsnObj = newsJsn.asObject();
            News news = JsonNewsParser.parseJsonToNews(newsJsnObj);
            String authorName = newsJsnObj.get(JSON_KEY_AUTHOR).asString();
            JsonArray tagsArray = newsJsnObj.get(JSON_KEY_TAGS).asArray();
            List<String> tags = new LinkedList<>();

            if (!tagsArray.isEmpty()) {

                for (JsonValue tagJsn : tagsArray) {
                    tags.add(tagJsn.asString());

                }

            }

            newsManage.save(news, tags, authorName);
        }
    }

}
