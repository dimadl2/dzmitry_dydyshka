package by.epam.newsadmin.controller;

import by.epam.newsadmin.util.DateUtils;
import by.epam.newsadmin.util.StringDUtils;
import by.epam.newsapp.dto.Author;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsManage;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.service.TagService;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/7/2015.
 */
@Controller
public class EditNewsController {

    private static final String URL_NEWS_EDIT = "/news/edit/{newsId}";
    private static final String URL_NEWS_EDIT_VERSION = "/news/edit/{newsId}/version";
    private static final String URL_NEWS = "/news";
    private static final String ATTR_TAGS = "tags";
    private static final String ATTR_AUTHORS = "authors";
    private static final String ATTR_NEWS = "news";
    private static final String ATTR_DATE = "date";
    private static final String VIEW_NEWS_ADD = "news-add";
    private static final String REDIRECT = "redirect:";
    private static final String ATTR_PLACEHOLDER = "placeholder";

    public static final Logger LOG = Logger.getLogger(EditNewsController.class);

    @Autowired
    private NewsManage newsManage;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = URL_NEWS_EDIT, method = RequestMethod.GET)
    public ModelAndView loadPage(@PathVariable Long newsId, HttpServletRequest request) throws Exception {

        String locale = String.valueOf(request.getAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME));

        SimpleDateFormat format = DateUtils.getFormat(locale);

        String creationDate = format.format(new Date());

        ModelAndView model = new ModelAndView();

        News newsVO = newsManage.fetchById(newsId);
        List<Tag> tags = tagService.list();
        List<Author> authors = authorService.fetchExpireAndNewsAuthor(newsId);

        model.addObject(ATTR_TAGS, tags);
        model.addObject(ATTR_AUTHORS, authors);
        model.addObject(ATTR_NEWS, newsVO);
        model.addObject(ATTR_PLACEHOLDER, DateUtils.getStringFormat(locale));
        model.addObject(ATTR_DATE, creationDate);
        model.setViewName(VIEW_NEWS_ADD);

        return model;
    }


    @RequestMapping(value = URL_NEWS_EDIT, method = RequestMethod.POST)
    public ModelAndView updateNews(@PathVariable Long newsId,
                                @RequestParam String title,
                                @RequestParam String date,
                                @RequestParam("short-text") String shortText,
                                @RequestParam("full-text") String fullText,
                                @RequestParam Long author,
                                @RequestParam Timestamp modDate,
                                @RequestParam(required = false) List<Long> tags,
                                HttpServletRequest request) throws Exception {

        if(!newsManage.verifyVersion(newsId, modDate)){
            ModelAndView model = new ModelAndView(REDIRECT + "/news/edit" + "/" + newsId);
            model.addObject("errorCuncurency", true);
            return model;
        }

        News news = new News();
        news.setId(newsId);
        news.setTitle(StringDUtils.escape(title));
        news.setShortText(StringDUtils.escape(shortText));
        news.setFullText(fullText);

        String locale = String.valueOf(request.getAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME));

        SimpleDateFormat format = DateUtils.getFormat(locale);

        Date dateCreate = format.parse(date);
        news.setCreationDate(dateCreate);
        news.setModificationDate(new Timestamp(System.currentTimeMillis()));

        newsManage.update(news, tags, author);

        return new ModelAndView(REDIRECT + URL_NEWS + "/" + newsId);

    }


    @RequestMapping(value = URL_NEWS_EDIT_VERSION, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String fetchVersion(@PathVariable Long newsId, @RequestParam Timestamp modDate) throws TechnicalException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("checkVersion", newsManage.verifyVersion(newsId, modDate) );

        return jsonObject.toJSONString();

    }
}
