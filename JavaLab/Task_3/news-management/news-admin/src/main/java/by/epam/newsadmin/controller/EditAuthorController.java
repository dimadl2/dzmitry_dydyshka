package by.epam.newsadmin.controller;

import by.epam.newsadmin.util.StringDUtils;
import by.epam.newsapp.dto.Author;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/8/2015.
 */
@Controller
public class EditAuthorController {

    private static final String URL_AUTHORS = "/authors";
    private static final String URL_AUTHORS_ADD = "/authors/add";
    private static final String REDIRECT = "redirect:";
    private static final String AUTHORS = "authors";
    private static final String URL_AUTHORS_DELETE = "/authors/delete";
    private static final String URL_AUTHORS_UPDATE = "/authors/update";

    @Autowired
    private AuthorService authorService;

    @RequestMapping(URL_AUTHORS)
    public ModelAndView loadPage() throws TechnicalException {

        ModelAndView modelAndView = new ModelAndView();

        List<Author> authors = authorService.getAllNotExpire();

        modelAndView.addObject(AUTHORS, authors);
        modelAndView.setViewName(AUTHORS);
        return modelAndView;

    }


    @RequestMapping(URL_AUTHORS_ADD)
    public ModelAndView addAuthor(@RequestParam("new-author") String nameAuthor) throws TechnicalException {

        nameAuthor = StringDUtils.escape(nameAuthor);

        Author authorTO = new Author(nameAuthor);

        authorService.save(authorTO);


        return new ModelAndView(REDIRECT + URL_AUTHORS);

    }

    @RequestMapping(URL_AUTHORS_DELETE)
    public ModelAndView deleteAuthor(@RequestParam("author-id") Long authorId) throws TechnicalException {


        authorService.expire(authorId);

        return new ModelAndView(REDIRECT + URL_AUTHORS);

    }
    @RequestMapping(value = URL_AUTHORS_UPDATE, method = RequestMethod.POST)
    public ModelAndView updateAuthor(@RequestParam("author-id") Long authorId, @RequestParam String name) throws TechnicalException {

        name = StringDUtils.escape(name);
        Author authorTO = new Author(authorId, name);

        authorService.update(authorTO);

        return new ModelAndView(REDIRECT + URL_AUTHORS);
    }
}
