package by.epam.newsadmin.util;


import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;
import org.springframework.web.util.JavaScriptUtils;

/**
 * Created by Dzmitry_Dydyshka on 8/9/2015.
 */
public final class StringDUtils {
    public static String escape(String string){

        string = HtmlUtils.htmlEscape(string);
        string = JavaScriptUtils.javaScriptEscape(string);
        string = StringUtils.trimWhitespace(string);

        return string;

    }

}
