package by.epam.newsadmin.util.json;

/**
 * @author Dzmitry_Dydyshka.
 */
public abstract class ConstatnsJsonKey {

    public static final String JSON_KEY_LAST_ID = "last_id";
    public static final String JSON_KEY_NEWS = "news";
    public static final String JSON_KEY_TITLE = "title";
    public static final String JSON_KEY_SHORT_TEXT = "short_text";
    public static final String JSON_KEY_FULL_TEXT = "full_text";
    public static final String JSON_KEY_CREATION_DATE = "creation_date";
    public static final String JSON_KEY_MOD_DATE = "modification_date";
    public static final String JSON_KEY_AUTHOR = "author";
    public static final String JSON_KEY_TAGS = "tags";
    public static final String JSON_KEY_ID = "id";
    public static final String JSON_KEY_COMMENTS = "comments";

}
