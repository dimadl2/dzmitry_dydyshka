 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div class="form-add-author">
				<table>
					<c:forEach var="author" items="${authors}">
						<tr>
							<td><spring:message code="label.author"/>: </td>
							<td>
								<form action="/news-admin/authors/update"  method="POST" onsubmit="return verifyUpdateForm();">

									<input type="hidden" name="author-id" value="${author.id}"/>
									<input type="text" name="name" value="${author.name}" class="author-name" disabled>
									<input type="submit" value="<spring:message code='label.update'/>" class="btn-spec update">
								</form>
								<form class="expire" action="/news-admin/authors/delete" method="POST">
									<input type="hidden" name="author-id" value="${author.id}"/>
									<input type="submit" value="<spring:message code='label.expire'/>" class="btn-spec exp">
								</form>
								<button class="btn-spec btn-spec-active edit"><spring:message code='label.edit'/></button>
								<button class="btn-spec cncl"><spring:message code='label.cancel'/></button>

							</td>
						</tr>
					</c:forEach>
					<tr id="authorError" style="display:none">
						<td></td>
						<td class="error"><spring:message code="message.author.empty"/></td>
					</tr>
					<tr>
						<td><spring:message code="label.author.add"/>:</td>
						<td><form action="/news-admin/authors/add" method="POST"  id="addauthor">
							<input type="text" class="input-l" name="new-author" id="new-author"/>
							<input type="submit" class="btn-spec btn-spec-active" value="<spring:message code='label.save'/>"/>
						</form></td>
					</tr>
				</table>
			</div>