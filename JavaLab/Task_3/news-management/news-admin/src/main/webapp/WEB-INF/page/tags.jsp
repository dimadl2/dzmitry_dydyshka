 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


 <div class="form-add-author">
 				<table>
 					<c:forEach var="tag" items="${tags}">
 						<tr>
 							<td><spring:message code="label.tag"/>: </td>
 							<td>
 								<form action="/news-admin/tags/update"  method="POST" onsubmit="return verifyUpdateForm();">

 									<input type="hidden" name="tag-id" value="${tag.id}"/>
 									<input type="text" name="name" value="${tag.tagName}" class="input-name" disabled>
 									<input type="submit" value="<spring:message code='label.update'/>" class="btn-spec update">
 								</form>
 								<form class="expire" action="/news-admin/tags/delete" method="POST">
 									<input type="hidden" name="tag-id" value="${tag.id}"/>
 									<input type="submit" value="<spring:message code='label.delete'/>" class="btn-spec exp">
 								</form>
 								<button class="btn-spec btn-spec-active edit"><spring:message code='label.edit'/></button>
 								<button class="btn-spec cncl"><spring:message code='label.cancel'/></button>

 							</td>
 						</tr>
 					</c:forEach>
 					<tr id="tagError" style="display:none">
                    	<td></td>
                    	<td class="error"><spring:message code="message.tag.empty"/></td>
                    </tr>
 					<tr>
 						<td><spring:message code="label.tag.add"/>:</td>
 						<td><form action="/news-admin/tags/add" method="POST" id="addtag">
 							<input type="text" name="new-tag"/>
 							<input type="submit" class="btn-spec btn-spec-active" value="<spring:message code='label.save'/>"/>
 						</form></td>
 					</tr>
 				</table>
 			</div>