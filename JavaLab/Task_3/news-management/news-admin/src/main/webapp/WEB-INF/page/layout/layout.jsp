<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
	<head>
		<title>News List</title>
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/normolize.css'/>">
		 <link rel="stylesheet" type="text/css" href="<c:url value='/css/style.css'/>">
		<script src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
		  <script src="<c:url value='/js/script.js'/>"></script>
		 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
	<header>
		 <tiles:insertAttribute name="header" />
	</header>
	<div class="main">
		<sec:authorize access="isAuthenticated()">
			<nav class="menu">
				<tiles:insertAttribute name="menu" />
			</nav>
			<div class="container">
                 <tiles:insertAttribute name="body" />
            </div>
        </sec:authorize>
        <sec:authorize access="!isAuthenticated()">

        			<div class="container-login">
                         <tiles:insertAttribute name="body" />
                    </div>
                </sec:authorize>

	</div>
	<footer>
		  <tiles:insertAttribute name="footer" />
	</footer>
	</body>
</html>