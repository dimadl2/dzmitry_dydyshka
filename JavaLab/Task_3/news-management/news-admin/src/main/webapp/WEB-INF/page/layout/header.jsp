<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="label">
	<h1><spring:message code="label.title"/></h1>
</div>
<div class="logout">
	<sec:authorize access="isAuthenticated()">
		<form action="<c:url value='/logout'/>" method="post" id="logoutForm">
			<span><spring:message code="label.hello"/>, <sec:authentication property="principal.username" /></span> <input type="submit" value="<spring:message code="label.logout"/>">
		</form>
	 </sec:authorize>
</div>
<div class="lang">
	<a href="?page=${pageNum}&lang=en&action=${action}"><spring:message code="label.lang.en"/></a>
	<a href="?page=${pageNum}&lang=ru&action=${action}"><spring:message code="label.lang.ru"/></a>
</div>


