   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

	<div class="login-form">
		<c:if test="${error == 'true'}">
		  <span class="error"><spring:message code="error.login"/></span>
		</c:if>
		<form name="loginForm" action="<c:url value='/j_spring_security_check' />" method="POST">
			<table class="table-body">
				<tr>
					<td align="right">
						<spring:message code="label.username"/>:
					</td>
					<td>
						<input type="text" name="j_username">
					</td>

				</tr>
				<tr>
					<td align="right">
						<spring:message code="label.password"/>:
					</td>
					<td>
						<input type="password" name="j_password">
					</td>

				</tr>
				<tr>
					<td colspan="2" align="right">
							<input type="submit" value="<spring:message code="label.login"/>">
					</td>
				</tr>

			</table>
		</form>
	</div>