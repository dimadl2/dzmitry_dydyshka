<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:message var="language" code="language" scope="page"/>
<script type="text/javascript">
	setLang("${language}");
</script>


<div class="form-add-news">
		<c:if test="${errorCuncurency}">
			Errror!
		</c:if>
		<form id="add-news" action="

		        <c:choose>
                    <c:when test="${empty news}">
                         /news-admin/addnews
                    </c:when>
                    <c:otherwise>
                         /news-admin/news/edit/${news.id}
                    </c:otherwise>
                </c:choose>"


					" method="POST">
			<input type="hidden" name="modDate" id="modDate" value="${news.modificationDate}"/>
			<input type="hidden"id="idNews" value="${news.id}"/>
			<table>
				<tr>
                	<td></td>
                	<td id="cuncurError" class="error" style="display:none">
                		<spring:message code="message.cuncur.error"/>
                	</td>
                </tr>
				<tr>
					<td></td>
					<td id="titleError" class="error" style="display:none">
						<spring:message code="message.title.empty"/>
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="label.news.title"/>:
					</td>
					<td>
						<input type="text" name="title" id="title" value="${news.title}">
					</td>
				</tr>
				<tr>
                	<td></td>
                	<td id="dateError" class="error" style="display:none">
                		<spring:message code="message.date.empty"/>
                	</td>
                </tr>
				<tr>
					<td>
						<spring:message code="label.news.date"/>:
					</td>
					<td>
						<input type="text" name="date" id="date" value="${date}" placeholder="${placeholder}">
					</td>
				</tr>
				<tr>
					<td></td>
                    <td class="error" id="briefError" style="display:none">
                       <spring:message code="message.brief.empty"/>:
                    </td>
                </tr>
				<tr>
					<td>
						<spring:message code="label.news.brief"/>:
					</td>
					<td>
						<textarea name="short-text" id="brief" rows="4" cols="61">${news.shortText}</textarea>
					</td>
				</tr>
				<tr>
				    <td></td>
                    <td id="contentError" class="error" style="display:none">
                        <spring:message code="message.content.empty"/>
                    </td>
                </tr>
				<tr>
					<td>
						<spring:message code="label.news.content"/>:
					</td>
					<td>
						<textarea name="full-text" id="content" rows="10" cols="61">${news.fullText}</textarea>
					</td>
				</tr>
				<tr>
                    <td></td>
                    <td id="authorError" class="error" style="display:none">
                         <spring:message code="message.author.choose"/>
                    </td>
                </tr>

				<tr>
					<td>
					</td>
					<td>
						<div>
							<div class="multiselect">
                                <div class="selectBox">
                                    <select>
                                        <option id="sel-title">-- Select tags --</option>
                                    </select>
                                    <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes" align="left" style="display: none;">

                                     <c:forEach var="tag" items="${tags}">
                                                                       <label for="${tag.tagName}">
                                                                       	<input class="check" type="checkbox" name="tags" value="${tag.id}"
                                                                           			<c:forEach var="tagVariant" items="${news.tags}">
                                   	                                        				<c:if test="${tagVariant.id eq tag.id}">
                                   	                                        					checked
                                   	                                        				</c:if>
                                                                           			</c:forEach>

                                                                           />
                                                                           ${tag.tagName}
                                                                       </label>
                                                                   </c:forEach>

                            </div>
                        </div>
                            <select id="author" name="author">
                                <option disabled selected><b><spring:message code="label.select.author"/></b></option>
                               									<c:forEach var="author" items="${authors}">
                               										<option value="${author.id}"
                               											<c:forEach var="authorVariant" items="${news.authors}">
                               												<c:if test="${authorVariant.id eq author.id}">
                               														selected
                               												</c:if>
                               											</c:forEach>
                               										>${author.name}</option>
                               									</c:forEach>>
							</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right"><input type="button" value="Save" id="add-news-btn"></td>
				</tr>
			</tbody>
			</table>
		</form>
			</div>


