var commentsLabel;
var editLabel;
var deleteLabel;

function initLabels(lComment, lEdit, lDelete){

    commentsLabel = lComment;
    editLabel = lEdit;
    deleteLabel = lDelete;

}

function setLocation(curLoc){
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch(e) {}
    location.hash = '#' + curLoc;
}

$(document).ready(function(){

    $(".edit").click(function(){
 		cleanActive();
 		$(this).removeClass("btn-spec-active");
 		var parent = this.parentNode;
 		$(document).removeClass(".active");
 		$(parent).addClass("active");
 		loadClicks();
 		$(".active>form>.exp , .active>.cncl , .active>form>.update").addClass("btn-spec-active");
 		$(".active>form>input[type='text']").removeAttr("disabled");
 	});

 	function loadClicks(){

 		$(".active>.cncl").click(function(){
 			cleanActive();

 		});
 	}

 	function cleanActive(){
 		$(".active>form>input[type='text']").attr("disabled", "disabled");
 			$(".active>form>.exp , .active>.cncl , .active>form>.update").removeClass("btn-spec-active");
 			$(".active>.edit").addClass("btn-spec-active");
 			var parent = this.parentNode;
 			$(".active").removeClass("active");
 	}


    var expanded = false;

    $('html').click(function() {

        $("#checkboxes").css({"display" : "none"});
        expanded = false;
    });


    $(".selectBox").click(function(event){

        event.stopPropagation();
        if (!expanded) {
            $("#checkboxes").css({"display" : "block"});
            expanded = true;
        } else {
            $("#checkboxes").css({"display" : "none"});
            expanded = false;
        }
    });

    $("#checkboxes").click(function(event){

        event.stopPropagation();

    });

     $("#filter").submit(function(){

        var tags = $("input:checkbox:checked").size();
        var author = $("#author").val();

        if(tags === 0 && author === null){

            $("#filterError").css({"display":"block"});
            return false;
        }

        return true;

     });

     $("#reset").click(function(){

        $("input:checkbox").removeAttr('checked');
        $("option").removeAttr('selected');
        $("option#selector").attr('selected', "selected");
        window.location.href = "http://localhost:8087/news-admin/news";

     });

     $("#add-news-btn").click(function(){

        verifyVersion();

     });

     var count = $("input:checkbox:checked").size();
     var message = $("#sel-title").val();

     if (count !== 0){

        $("#sel-title").html("-- Selected (" + count + ") --");
     }

     $(".check").change(function() {

        if (count === 0 ){

            message =  $("#sel-title").val();

        }

        if($(this).is(":checked")) {
            count+=1;
        }else{
            count-=1;
        }

        if (count === 0 ){

            $("#sel-title").html(message);

        }else {
            $("#sel-title").html("-- Selected (" + count + ") --");

        }

     });

     $("#delete-news").submit(function(){

          var checks = $("input:checkbox:checked").size();

          if (checks !== 0){

              return confirm("Are you sure?");

          }else {
               alert("Choose news!");
               return false;
          }

     });

     $(".expire").submit(function(){

           return confirm("Are you shure?");

     });

     $(".f-page").submit(function(){return false;})

     $(".page-btn").click(function(){

        clickOnPageBtn($(this));

     });

});

var lang = "en";
var en_pattern = "^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\\d\\d$";
var ru_pattern = "^(0[1-9]|1[012])[.](0[1-9]|[12][0-9]|3[01])[.](19|20)\\d\\d$"

function setLang(language){

    lang = language;

}

function getPatternDate(){

      if (lang === "en"){
          return en_pattern;
      }else {
          return ru_pattern;
      }

}

function changeState(elemId, elem){

    $("input#field" + elemId).removeAttr("disabled");
    $("input#update" + elemId).css({"display": "inline"});
    $("input#delete" + elemId).css({"display": "inline"});
    elem.style.display = "none";
       

}

function verifyUpdateForm(){

    var elem =  $(".active>form>.update").val();
    elem = $.trim(elem);

    if (elem === ""){
         alert("Empty field!");
         return false;
    }

    return true;

}


function verifyVersion(){

    var modDate = $("#modDate").val();
    var idNews = $("#idNews").val();

    if (idNews === ""){
        if (validate()){
            $("#add-news").submit();
            return;
        };

    }else{

        $.ajax({
            type:"post",

            url: "http://localhost:8087/news-admin/news/edit/" + idNews + "/version" ,

            data:{"modDate": modDate},

            response:"json",

            success:function (data) {

                if (data["checkVersion"]){
                    if (validate()){

                        $("#add-news").submit();

                    };
                }else{
                    $("#cuncurError").css({"display":"block"});
                }

            }
        });
    }
}

function validate(){

    var title = $("#title").val();
    var brief = $("#brief").val();
    var date = $("#date").val();
    var content = $("#content").val();
    var author = $("#author").val();

    var flag = 0;

     title = $.trim(title);
     brief = $.trim(brief);
     date = $.trim(date);
     content = $.trim(content);

     if (title === ""){

        $("#titleError").css({"display":"block"});
        flag = 1;

     }else{
        $("#titleError").css({"display":"none"});
     }

     var reg = new RegExp(getPatternDate());


     if (date === "" || !reg.test(date)){

        $("#dateError").css({"display":"block"});
        flag = 1;

     }else{
        $("#dateError").css({"display":"none"});
     }

     if(brief === ""){
        $("#briefError").css({"display":"block"});
        flag = 1;
     }else{
        $("#briefError").css({"display":"none"});
     }


     if(content === ""){
         $("#contentError").css({"display":"block"});
         flag = 1;
     }else{

        $("#contentError").css({"display":"none"});

     }

     if (author === null){
        $("#authorError").css({"display":"block"})
        flag = 1;
     }else{
        $("#authorError").css({"display":"none"})
     }


     if (flag === 1){

        return false;

     }

     return true;

}

function verifyAuthor(message){

   alert(message);

   return false;

}

function clickOnPageBtn(el){

    var pageNum = el.attr("value");
    var action = el.parent().attr("action");

    $.ajax({
        type:"get",
        url: "http://localhost:8087"+action+"/json/page/" + pageNum,
        response:"json",
        success:function (data) {

            var actualNumberOfPage = data["number_of_page"];
            var currNumberOfPage = $(".page-btn").length;

            if (actualNumberOfPage < currNumberOfPage) {

                for(i=0; i<currNumberOfPage-actualNumberOfPage; i++){
                    $(".paginator div:last-child").remove()
                }

            }else if (actualNumberOfPage > currNumberOfPage){

                for(i=1; i<(actualNumberOfPage-currNumberOfPage) + 1; i++){

                    $(".paginator").append("<div>" + $(".paginator div:last-child").html() + "</div>");
                    $(".paginator div:last-child form input[name='page']").attr("value", currNumberOfPage + i);
                    $(".paginator div:last-child form input[type='submit']").attr("value", currNumberOfPage + i);
                    $(".f-page").submit(function(){return false;});
                    $(".page-btn").click(function() {
                        clickOnPageBtn($(this));
                    });
                }

            }


            var result="";
            var news = data["news"];

            for (i =  0; i < news.length; i++){
                newsItem = news[i];
                result+="<div class='news-item'>"+
                    "<div class='title'><a href='/news-admin/news/"+newsItem["id"]+"'>"+newsItem["title"]+"</a>"+
                    "</div>"+
                    "<div class='author'>(by " + newsItem["author"] + ")</div>"+
                    "<div class='date'>" + newsItem["modification_date"] + "</div>"+
                    "<div class='desc'>" + newsItem["short_text"] + "</div>" +
                    "<div class='tools'>"+
                    "<div class='tool-tag'>";

                var tags = newsItem["tags"];

                for (j = 0; j < tags.length; j++){

                    result += tags[j] + ",";

                }

                result += "</div>" +
                    "<div class='tool-comment'>"+commentsLabel+"("+newsItem["comments"]+")</div>"+
                    "<div><a href='/news-admin/news/edit/'"+newsItem["id"]+">"+editLabel+"</a></div>"+
                    "<div><input type='checkbox' name='newsIds' value='"+newsItem["id"]+"'></div></div></div>";

            }

            result += "<div class='del-news'><input type='submit' value='"+ deleteLabel +"'></div>"
            $("#delete-news").html(result);

            setLocation("news?page=" + pageNum);

        },

        error: function(e){
            alert(e.message);
        }
    });
}



   






