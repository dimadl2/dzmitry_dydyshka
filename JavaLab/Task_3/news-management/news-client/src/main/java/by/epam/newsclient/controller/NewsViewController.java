package by.epam.newsclient.controller;

import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.CommentService;
import by.epam.newsapp.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.Map;


/**
 * Created by Dzmitry_Dydyshka on 3/31/2015.
 */
@Controller
public class NewsViewController {

    private static final String URL_NEWS_VIEW = "/news/{idNews}";
    private static final String ACTION_ADD_COMMENT = "addComment";
    private static final String ATTR_NEXT_ID = "nextId";
    private static final String ATTR_PREVIOUS_ID = "previousId";
    private static final String ATTR_NEWS = "news";
    private static final String VIEW_NEWS = "news-view";
    private static final String ATTR_ERROR_COMMENT = "errorComment";
    private static final String PREVIOUS = "previous";
    private static final String NEXT = "next";


    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(URL_NEWS_VIEW)
    public ModelAndView viewNews(@PathVariable Long idNews,
                                 @RequestParam(required = false) String action,
                                 @RequestParam(required = false) String commentText) throws TechnicalException {

        ModelAndView modelAndView = new ModelAndView();


        if (ACTION_ADD_COMMENT.equals(action)) {


            commentText = StringUtils.trimWhitespace(commentText);

            if (!StringUtils.isEmpty(commentText)) {

                News news = new News();
                news.setId(idNews);
                Comment comment = new Comment(commentText, new Date(), news);
                commentService.save(comment);

            }else {

                modelAndView.addObject(ATTR_ERROR_COMMENT, "error");
            }

        }

        News news = newsService.fetchById(idNews);
        Map<String, Long> nxtAndPrvs = newsService.getNextAndPreviousNewsId(idNews);
        Long nextNewsId = nxtAndPrvs.get(NEXT);
        Long previousNewsId = nxtAndPrvs.get(PREVIOUS);


        modelAndView.addObject(ATTR_NEXT_ID, nextNewsId);
        modelAndView.addObject(ATTR_PREVIOUS_ID, previousNewsId);
        modelAndView.addObject(ATTR_NEWS, news);

        modelAndView.setViewName(VIEW_NEWS);

        return modelAndView;
    }
}
