<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="main">
		<div class="container">
			<div class="back">
				<c:if test="${not empty action}">
                	<a href="/news-client/news/filter?page=${pageNum}">
                        <spring:message code="label.back"/>
                    </a>
                </c:if>

                <c:if test="${empty action}">
                    <a href="/news-client/news?page=${pageNum}">
                    	<spring:message code="label.back"/>
                    </a>
                </c:if>
			</div>
			<div class="news-item">
				<div class="title">${news.title}</div>
				<div class="author">(by
                	<c:forEach var="author" items="${news.authors}">
                    	${author.name}
                    </c:forEach>
                    )
                </div>
				<div class="date">
					<spring:message var="datePattern" code="pattern.date" scope="page"/>
                    <fmt:formatDate type="date" pattern="${datePattern}" value="${news.modificationDate}" />
                </div>
				<div class="desc">
					${news.fullText}
				</div>

				<div class="comments">
					<c:forEach var="comment" items="${news.comments}">
						<div class="comments-item">
							<div class="item-comm-date">
								<fmt:formatDate type="date" pattern="${datePattern}" value="${comment.creationDate}" />
							</div>
							<div class="item-comm-content">
								${comment.commentText}
							</div>
						</div>
					</c:forEach>
					<c:if test="${not empty errorComment}">
                    		<div class="error">
                        		<spring:message code="message.comment.empty"/>
                        	</div>
                    </c:if>
					<div class="error" style="display:none">
						<spring:message code="message.comment.empty"/>
					</div>
					<div class="form-add-comm">
                    	<form action="/news-client/news/${news.id}" method="POST" id="comment">
                    		<input type="hidden" name="action" value="addComment"/>
                    		<textarea name="commentText" id="txt-comment" rows="4"></textarea>
                    		<div class="btn-post">
                    			<input type="submit" value="<spring:message code="label.comments.post"/>">
                    		</div>
                    	</form>
                    </div>
				</div>
			</div>
			<div class="nav-page">
				<div>
					<c:if test="${previousId != 0}" >
                    	<a href="/news-client/news/${previousId}"><spring:message code="label.previous"/></a>
                    </c:if>
				</div>
				<div>
					<c:if test="${nextId != 0}" >
                     	<a href="/news-client/news/${nextId}"><spring:message code="label.next"/></a>
                    </c:if></div>
			</div>
		</div>
	</div>