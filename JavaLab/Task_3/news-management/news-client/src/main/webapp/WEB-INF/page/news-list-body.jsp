<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>



<div class="main">
		<div class="container">
			<c:if test="${not empty errorFilter}">
				<div class="error">Please, choose the author or tags!</div>
			</c:if>

			<div class="error" id="filterError" style="display: none">Please, choose the author or tags!</div>

			<div class="filter">
				<form action="/news-client/news/filter" method="POST" id="filter">
					 <div class="multiselect" class="row">
                     	<div class="selectBox">
                        	<select>
                            	<option id="sel-title"><spring:message code="label.select.tags"/></option>
                            </select>
                            <div class="overSelect"></div>
                        </div>
                        <div id="checkboxes" align="left">
                        	<c:forEach var="tag" items="${tags}">
                            	<label for="${tag.tagName}">
                            		<input type="checkbox" class="check" name="tagsId" value="${tag.id}"

                                        <c:if test="${tagsId.contains(tag.id)}">
                                            checked
                                        </c:if>

                                    />
                                    ${tag.tagName}
                                </label>
                            </c:forEach>
                         </div>
                     </div>
                     <select id="author" name="author">
                     	<option id="selector" disabled selected><b><spring:message code="label.select.author"/></b></option>
                     	<c:forEach var="author" items="${authors}">
                     		<option value="${author.id}"

                     			<c:if test="${author.id eq authorId}">
                     				selected
                     			</c:if>

                     		>${author.name}</option>
                     	</c:forEach>
					 </select>
					 <input type="submit" value="<spring:message code="label.filter"/>">
                     <input type="button" id="reset" value="<spring:message code="label.reset"/>" style="margin-left: 10px">
				</form>
			</div>
			<div class="content">
				<c:forEach var="item" items="${news}">
					<div class="news-item">
						<div class="title">
							${item.title}
						</div>
						<div class="author">(by
                            <c:forEach var="author" items="${item.authors}">
                                ${author.name}
                            </c:forEach>
                            )
                        </div>
						<div class="date">
							<spring:message var="datePattern" code="pattern.date" scope="page"/>
                            <fmt:formatDate type="date" pattern="${datePattern}" value="${item.creationDate}" />
						</div>

						<div class="desc">${item.shortText}</div>
						<div class="tools">
							<div class="tool-tag">
								<c:forEach var="tag" items="${item.tags}">
                                	${tag.tagName},
                                </c:forEach>
							</div>
							<div class="tool-comment"><spring:message code="label.comments"/>(${item.numberOfComments})</div>
							<div><a href="/news-client/news/${item.id}"><spring:message code="label.view"/></a></div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="paginator">
				 <c:forEach var="i" begin="1" end="${numberOfPage}">
					<div>
						  <form

                             <c:if test="${not empty action}">
                                action="/news-client/news/filter" method="GET">
                             </c:if>

                             <c:if test="${empty action}">
                                action="/news-client/news" method="GET">
                             </c:if>

                             <input type="hidden" name="page" value="${i}"/>
                             <input type="submit" value="${i}"/>
                          </form>
					</div>
				 </c:forEach>
			</div>
		</div>
	</div>