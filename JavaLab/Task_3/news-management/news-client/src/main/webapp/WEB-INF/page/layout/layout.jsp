<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>News List</title>
		 <link rel="stylesheet" type="text/css" href="<c:url value='/css/normolize.css'/>">
		 <link rel="stylesheet" type="text/css" href="<c:url value='/css/style.css'/>">
		<script src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
		  <script src="<c:url value='/js/common.js'/>"></script>
		 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>

		 <tiles:insertAttribute name="header" />
		 <tiles:insertAttribute name="body" />
		 <tiles:insertAttribute name="footer" />

	</body>
</html>