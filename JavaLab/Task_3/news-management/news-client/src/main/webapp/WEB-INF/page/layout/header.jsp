<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<header>
	<div class="label">
		<h1><spring:message code="label.title"/></h1>
	</div>
	<div class="lang">
		<a href="?page=${pageNum}&lang=en&action=${action}"><spring:message code="label.lang.en"/></a>
		<a href="?page=${pageNum}&lang=ru&action=${action}"><spring:message code="label.lang.ru"/></a>
	</div>
</header>