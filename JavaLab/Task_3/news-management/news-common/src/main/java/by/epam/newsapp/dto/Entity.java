package by.epam.newsapp.dto;

import java.io.Serializable;

/**
 * Created by Dzmitry_Dydyshka on 5/15/2015.
 */
public interface Entity extends Serializable {
    Long getId();
}
