package by.epam.newsapp.dao.jpa;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.Filter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dzmitry_Dydyshka on 5/15/2015.
 */
@Repository("newsDAOJpa")
@Transactional
public class NewsDAOJpaImpl extends AbstractDAOJpa<News> implements NewsDAO {

    private static final String JQL_FETCH_LIMIT_NEWS = "from News n order by n.numberOfComments DESC, n.modificationDate Desc";
    private static final String JQL_FETCH_ALL_NEWS = "select count(n)from News n";
    private static final String ID = "id";
    private static final String FIELD_TAGS = "tags";
    private static final String FIELD_AUTHORS = "authors";
    private static final String FIELD_NUMBER_OF_COMMENTS = "numberOfComments";
    private static final String FIELD_MODIFICATION_DATE = "modificationDate";
    private static final String JQL_DELETE_LIST = "DELETE FROM News n WHERE n.id IN (:list)";
    private static final String PARAM_LIST = "list";
    private static final String SQL_GET_NEXT_PREVIOUS_ID = "WITH slc AS (\n" +
            "            SELECT news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date, count(comments.comments_id) as count \n" +
            "            FROM news \n" +
            "            LEFT JOIN comments \n" +
            "            ON news.news_id = comments.news_id \n" +
            "            GROUP BY news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date)\n" +
            "select previous, next from (select  slc.news_id as id,\n" +
            "lag(slc.news_id) over (order by slc.count desc, slc.modification_date DESC ) as previous  ,      \n" +
            "lead(slc.news_id) over (order by slc.count desc, slc.modification_date DESC ) as next  from \n" +
            "slc) where id = :cur_id";
    private static final String PREVIOUS = "previous";
    private static final String NEXT = "next";
    private static final String PARAM_CUR_ID = "cur_id";

    private static final Logger LOG = Logger.getLogger(NewsDAOJpaImpl.class);

    @Override
    public List<News> fetchLimitNews(int startPosition, int newsAtPage) throws DAOException {

        Query query = em.createQuery(JQL_FETCH_LIMIT_NEWS);
        query.setFirstResult(startPosition);
        query.setMaxResults(newsAtPage);

        return query.getResultList();
    }


    @Override
    @Transactional
    public void delete(List<Long> newsIds) throws DAOException {

        Query query = em.createQuery(JQL_DELETE_LIST);
        query.setParameter(PARAM_LIST, newsIds);
        query.executeUpdate();

    }

    @Override
    public int getCountRows() throws DAOException {
        return (int) (long) em.createQuery(JQL_FETCH_ALL_NEWS).getSingleResult();
    }

    @Override
    public Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws DAOException {

        Query query = em.createNativeQuery(SQL_GET_NEXT_PREVIOUS_ID);
        query.setParameter(PARAM_CUR_ID, currentNewsId);
        List<Object[]> list = query.getResultList();

        Map<String, Long> mapIds = new HashMap<>();

        BigDecimal next = (BigDecimal) list.get(0)[1];

        if (next != null){
            mapIds.put(NEXT, next.longValue());
        }else{
            mapIds.put(NEXT, 0L);
        }

        BigDecimal previous = (BigDecimal) list.get(0)[0];

        if (previous != null){
            mapIds.put(PREVIOUS, previous.longValue());
        }else{
            mapIds.put(PREVIOUS, 0L);
        }

        return mapIds;

    }

    @Override
    public List<News> fetchSearchLimit(Filter filter, int startPosition, int endPosition) throws DAOException {

        TypedQuery<News> tq = getFilterQuery(filter);
        tq.setFirstResult(startPosition - 1);
        tq.setMaxResults(endPosition);
        List<News> result = tq.getResultList();

        return result;
    }

    @Override
    public int getCountSearchRows(Filter filter) throws DAOException {

        TypedQuery<News> tq = getFilterQuery(filter);
        return tq.getResultList().size();

    }

    @Override
    public Long addCustom(News news) {
        return null;
    }

    private TypedQuery<News> getFilterQuery(Filter filter){

        List<Long> tags = filter.getTagsId();
        Long author = filter.getAuthorId();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<News> query = cb.createQuery(News.class);
        Root<News> n = query.from(News.class);

        query = query.select(n).distinct(true);

        if (filter.isFull()) {
            query.where(cb.and(n.join(FIELD_TAGS).get(ID).in(tags), (n.join(FIELD_AUTHORS).get(ID).in(author))));
        }else if (tags == null || tags.isEmpty()){
            query.where(n.join(FIELD_AUTHORS).get(ID).in(author));
        }else if (author == null || author == 0L) {
            query.where(n.join(FIELD_TAGS).get(ID).in(tags));
        }

        query.orderBy(cb.desc(n.get(FIELD_NUMBER_OF_COMMENTS)), cb.desc(n.get(FIELD_MODIFICATION_DATE)));
        TypedQuery<News> tq = em.createQuery(query);

        return tq;
    };
}