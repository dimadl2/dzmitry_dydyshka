package by.epam.newsapp.dto;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;


/**
 * The Class News.
 */
@javax.persistence.Entity
@Table(name = "news")
@BatchSize(size = 50)
public class News implements Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String NEWS_SEQ = "NEWS_SEQ";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = NEWS_SEQ)
	@SequenceGenerator(name=NEWS_SEQ, sequenceName = NEWS_SEQ, allocationSize = 1)
	@Column(name = "news_id")
	private Long id;

	/** The short text. */
	@Column(name = "short_text")
	private String shortText;

	/** The full text. */
	@Column(name = "full_text")
	private String fullText;

	/** The title. */
	@Column(name = "title")
	private String title;

	/** The creation date. */
	@Column(name = "creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	/** The modification date. */
	@Column(name = "modification_date")
	private Timestamp modificationDate;


	/**
	 *
	 * The set of authors
	 *
	 */
	@ManyToMany(cascade = CascadeType.REMOVE, fetch=FetchType.LAZY)
	@JoinTable(name = "NEWS_TAG",
			joinColumns = @JoinColumn(name = "news_id"),
			inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private List<Tag> tags = new LinkedList<>();

	/**
	 *
	 * The set of authors
	 *
	 */
	@ManyToMany(cascade = CascadeType.REMOVE,  fetch=FetchType.LAZY)
	@JoinTable(name = "NEWS_AUTHOR",
			joinColumns = @JoinColumn(name = "news_id"),
			inverseJoinColumns = @JoinColumn(name = "author_id"))
	private Set<Author> authors = new HashSet<>();

	/**
	 * The set of comments
	 */

	@OneToMany(mappedBy = "news", fetch = FetchType.LAZY)
	private List<Comment> comments = new LinkedList<>();


	@Formula("(SELECT count(c.comments_id) FROM comments c WHERE c.news_id = news_id)")
	private int numberOfComments;

	/**
	 * Gets the short text.
	 *
	 * @return the shortText
	 */
	public String getShortText() {
		return shortText;
	}

	/**-
	 * Sets the short text.
	 *
	 * @param shortText
	 *            the shortText to set
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the fullText
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText
	 *            the fullText to set
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modificationDate
	 */
	public Timestamp getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Timestamp modificationDate) {
		this.modificationDate = modificationDate;
	}


	/**
	 * Getter for the news ID
	 *
	 * @return - the news ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Setter for the news ID
	 * @param id - the news ID
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 *
	 * Getter for the set of authors
	 *
	 * @return - the set of authors
	 */
	public Set<Author> getAuthors() {
		return authors;
	}

	/**
	 *
	 * Setter for the set of authors
	 *
	 * @param authors - the set of authors
	 */
	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	/**
	 *
	 * Getter for number of comments
	 *
	 * @return - the number of comments
	 */
	public int getNumberOfComments() {
		return numberOfComments;
	}

	/**
	 *
	 * Setter for the number of comments
	 *
	 * @param numberOfComments - the number of comments
	 */
	public void setNumberOfComments(int numberOfComments) {
		this.numberOfComments = numberOfComments;
	}

	/**
	 *
	 * Getter for the list of comments
	 *
	 * @return - the list of comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 *
	 * Setter for the list of comments
	 *
	 * @param comments - the list of comments
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 *
	 * Getter for the list of tags
	 *
	 * @return - the list of tags
	 */
	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        News news = (News) o;

        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null)
            return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

}
