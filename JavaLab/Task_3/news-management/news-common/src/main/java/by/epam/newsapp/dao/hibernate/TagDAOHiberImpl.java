package by.epam.newsapp.dao.hibernate;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dto.Tag;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/28/2015.
 */
@Repository("tagDAOHiber")
public class TagDAOHiberImpl extends AbstractDAOHiber<Tag> implements TagDAO {


    private static final String HQL_FETCH_TAGS_BY_NAME = "FROM Tag t WHERE t.tagName IN (:list)";

    @Override
    public List<Tag> saveAndReturn(List<String> names) {

        List<Tag> result = new LinkedList<>();

        if (names.isEmpty()) {
            return result;
        }

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(HQL_FETCH_TAGS_BY_NAME);
        query.setParameterList("list", names);

        List<Tag> list = query.list();


        for (Tag tag : list) {
            if (names.contains(tag.getTagName())) {
                names.remove(tag.getTagName());
            }
        }

        for (String name : names) {
            Tag tag = new Tag(name);
            session.persist(tag);
            result.add(tag);
        }

        result.addAll(list);

        return result;

    }
}
