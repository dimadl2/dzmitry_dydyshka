package by.epam.newsapp.dto;


import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class of dto Author.
 */
@javax.persistence.Entity
@Table(name = "author")
public class Author implements Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_SEQ")
	@SequenceGenerator(name="AUTHOR_SEQ", sequenceName = "AUTHOR_SEQ", allocationSize = 1)
	@Column(name="author_id")
	private Long id;

	/** The name author. */
	@Column(name = "name")
	private String name;

	@Column(name = "exp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date expire;

	@ManyToMany(mappedBy = "authors")
	private Set<News> news = new HashSet<>();

	/**
	 * Default constructor.
	 */
	public Author() {

	}

	/**
	 * Instantiates a new author by name.
	 *
	 * @param name
	 *            the name
	 */
	public Author(String name) {

		this.name = name;

	}

	/**
	 * Instantiates a new author by name and ID.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 */
	public Author(Long id, String name) {

		this.id = id;

		this.name = name;

	}

	/**
	 * Gets the name author.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name author.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	public Date getExpire() {
		return expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	/**
	 * 
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * 
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
