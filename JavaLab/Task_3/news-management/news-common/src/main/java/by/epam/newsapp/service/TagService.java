package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.exception.TechnicalException;


/**
 *
 * The service provides methods for working with tag DAO
 *
 */
public interface TagService extends Service<Tag> {
    List<Tag> saveAndReturn(List<String> names);
}
