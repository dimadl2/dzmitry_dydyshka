package by.epam.newsapp.util;



import by.epam.newsapp.dto.News;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/10/2015.
 */
public class Page {

    private int numberAtPage;
    private int numberItems;
    private List<News> itemsForPage;

    public Page(int numberAtPage, int numberItems, List<News> itemsForPage) {
        this.numberAtPage = numberAtPage;
        this.numberItems = numberItems;
        this.itemsForPage = itemsForPage;
    }



    public int getNumberOfPage(){

        int numberOfPage = (int) Math.ceil(numberItems/ (double) numberAtPage);

        return numberOfPage;

    }

    public void setNumberAtPage(int numberAtPage) {
        this.numberAtPage = numberAtPage;
    }

    public void setNumberItems(int numberItems) {
        this.numberItems = numberItems;
    }

    public void setItemsForPage(List<News> itemsForPage) {
        this.itemsForPage = itemsForPage;
    }

    public List<News> getItemsForPage() {
        return itemsForPage;
    }
}
