package by.epam.newsapp.config;

import by.epam.newsapp.service.Service;
import org.apache.log4j.Logger;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.util.ReflectionUtils;

import java.io.File;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

/**
 * Created by Dzmitry_Dydyshka on 6/16/2015.
 */
@Component
public class DynamicDataAccessObjectBeanPostProcessor implements BeanPostProcessor{

    private static final String HIBERNATE = "hibernate";
    private static final String JPA = "jpa";
    private static final String KEY_CONTEXT_DAO = "context.dao";
    private static final String PROPERTY_CONTEXT = "context";

    @Autowired
    private ApplicationContext context;

    public static final Logger LOG =  Logger.getLogger(DynamicDataAccessObjectBeanPostProcessor.class);

    private Map<String, List<Field>> map = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        Class<?> beanClass = bean.getClass();

        if (!(bean instanceof Service)){
           return bean;
        }
        Field[] declaredFields = beanClass.getDeclaredFields();
        List<Field> fields = new LinkedList<>();
        for (Field declaredField : declaredFields) {

            if (declaredField.isAnnotationPresent(InjectDynamicDAO.class)){
                fields.add(declaredField);
            }

        }

        map.put(beanName, fields);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, String beanName) throws BeansException {

        File file = new File("C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\");
        URL[] urls = new URL[0];
        try {
            urls = new URL[]{file.toURI().toURL()};
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ClassLoader loader = new URLClassLoader(urls);
        ResourceBundle rb = ResourceBundle.getBundle(PROPERTY_CONTEXT, Locale.getDefault(), loader);

        String daoImpl = rb.getString(KEY_CONTEXT_DAO);

        Object daoObject = null;

        List<Field> fields = map.get(beanName);

        if (fields != null){

            for (Field field : fields) {

                InjectDynamicDAO annotation = field.getAnnotation(InjectDynamicDAO.class);


                if (HIBERNATE.equals(daoImpl)){

                    Class hiberImplClass = annotation.hibernate();
                    Repository repository = (Repository) hiberImplClass.getAnnotation(Repository.class);
                    String hiberImpl = repository.value();
                    daoObject = context.getBean(hiberImpl);

                    LOG.info("Changed dao to Hibernate!");

                }else if (JPA.equals(daoImpl)){

                    Class jpaImplClass = annotation.jpa();
                    Repository repository = (Repository) jpaImplClass.getAnnotation(Repository.class);
                    String jpaImpl = repository.value();
                    daoObject = context.getBean(jpaImpl);

                    LOG.info("Changed dao to JPA!");

                }else{
                    throw new IllegalArgumentException("Illegal argument: " + daoImpl);
                }

                LOG.info(daoObject);

                Object target;
                field.setAccessible(true);

                if (AopUtils.isAopProxy(bean) && bean instanceof Advised) {

                    try {
                        target = ((Advised) bean).getTargetSource().getTarget();
                        ReflectionUtils.setField(field, target, daoObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    ReflectionUtils.setField(field, bean, daoObject);
                }
            }

        }

        return bean;
    }
}
