package by.epam.newsapp.dao;

import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.Filter;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 *
 */
public interface NewsDAO extends CommonDAO<News> {

    /**
     *
     * The method allows to fetch limit list of news.
     *
     * @param startPosition - the start position for fetching form database
     * @param endPosition - the end position for fetching from database
     * @return - the limit list of news
     * @throws DAOException
     */
    List<News> fetchLimitNews(int startPosition, int endPosition) throws DAOException;

    /**
     *
     * The method allows to delete list of news from database by list id.
     *
     * @param newsIds - list of news if for deleting from database
     * @throws DAOException
     */
    void delete(List<Long> newsIds) throws DAOException;

    /**
     *
     * The method allows to get count of rows in News table
     *
     * @return - the count of rows in table
     * @throws DAOException
     */
    int getCountRows() throws DAOException;

    /**
     *
     * The method allows to get next and previous id news.
     *
     * @param currentNewsId - the current id news
     * @return - the map. Key - "next", Value - next id news. Key - "previous", Value - previous id news
     * @throws DAOException
     */
    Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws DAOException;

    /**
     *
     * The method allows to fetch searching limit list news
     *
     * @param filter - filter for searching
     * @param startPosition - the start position for fetching form database
     * @param endPosition - the end position for fetching from database
     * @return - the searching limit list
     * @throws DAOException
     */
    List<News> fetchSearchLimit(Filter filter, int startPosition, int endPosition ) throws DAOException;

    /**
     *
     * The method allows to get count of rows in founded list.
     *
     * @param filter - the filter for searching
     * @return - the count of rows in founded list
     * @throws DAOException
     */
    int getCountSearchRows(Filter filter) throws DAOException;


    Long addCustom(News news) throws DAOException;

}
