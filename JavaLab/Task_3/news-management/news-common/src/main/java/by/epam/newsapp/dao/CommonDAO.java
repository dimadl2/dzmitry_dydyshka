package by.epam.newsapp.dao;


import by.epam.newsapp.dto.Entity;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 *
 */
public interface CommonDAO <T extends Entity> {


    /**
     * The method allows to get list of entities
     *
     * @return - list of entities
     * @throws DAOException
     */
    List<T> list() throws DAOException;

    /**
     *
     * The method allows to add entity into database
     *
     * @param object - entity for adding
     * @return - id of added entity
     * @throws DAOException
     */
    Long add(T object) throws DAOException;

    /**
     * The method allows to delete entity from database
     * @param id - id of entity that need to delete from database
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;

    /**
     *
     * The method allows to update entity in database
     *
     * @param object - entity for updating
     * @throws DAOException
     */
    void update(T object) throws DAOException;

    /**
     *
     * The method allows to fetch entity from database by id
     *
     * @param id - id of entity
     * @return - entity from database
     * @throws DAOException
     */
    T fetchById(Long id) throws DAOException;

}
