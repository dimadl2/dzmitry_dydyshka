package by.epam.newsapp.dao.hibernate;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dto.Author;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.*;
import by.epam.newsapp.util.Filter;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Dzmitry_Dydyshka on 4/29/2015.
 */
@Repository("newsDAOHiber")
public class NewsDAOHiberImpl extends AbstractDAOHiber<News> implements NewsDAO {

    private static final String HQL_DELETE_LIST_NEWS = "DELETE FROM News n WHERE n.id IN (:list)";
    private static final String PARAM_LIST = "list";
    private static final String PROP_NUMBER_OF_COMMENTS = "numberOfComments";
    private static final String PROP_MODIFICATION_DATE = "modificationDate";
    private static final String SQL_GET_NEXT_PREVIOUS_ID = "WITH slc AS (\n" +
            "            SELECT news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date, count(comments.comments_id) as count \n" +
            "            FROM news \n" +
            "            LEFT JOIN comments \n" +
            "            ON news.news_id = comments.news_id \n" +
            "            GROUP BY news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date)\n" +
            "select previous, next from (select  slc.news_id as id,\n" +
            "lag(slc.news_id) over (order by slc.count desc, slc.modification_date DESC ) as previous  ,      \n" +
            "lead(slc.news_id) over (order by slc.count desc, slc.modification_date DESC ) as next  from \n" +
            "slc) where id = :cur_id";
    private static final String PREVIOUS = "previous";
    private static final String NEXT = "next";
    private static final String PARAM_CUR_ID = "cur_id";
    private static final String ID = "id";




    @Override
    public List<News> fetchLimitNews(int startPosition, int numberAtPage) throws DAOException {

        Session session = null;
        List<News> news = null;
        try {
            session = sessionFactory.openSession();
            Criteria cr = session.createCriteria(News.class, "NEWS");
            cr.addOrder(Order.desc(PROP_NUMBER_OF_COMMENTS));
            cr.addOrder(Order.desc(PROP_MODIFICATION_DATE));
            cr.setFirstResult(startPosition);
            cr.setMaxResults(numberAtPage);

            news = cr.list();


        } catch (HibernateException e){
            throw new DAOException(e);
        }

        return news;
    }

    @Override
    public void delete(List<Long> newsIds) throws DAOException {

        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            Query query = session.createQuery(HQL_DELETE_LIST_NEWS);
            query.setParameterList(PARAM_LIST, newsIds);

            int res = query.executeUpdate();

            tx.commit();

        } catch (HibernateException e) {

            throw new DAOException(e);

        }

    }


    @Override
    public int getCountRows() throws DAOException {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            return session.createCriteria(News.class).list().size();

        }catch(HibernateException e){
            throw new DAOException(e);
        }
    }

    @Override
    public Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws DAOException {

        Session session = null;

        try {
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery(SQL_GET_NEXT_PREVIOUS_ID);
            query.setLong(PARAM_CUR_ID, currentNewsId);

            List<Object[]> list = query.list();

            Map<String, Long> mapIds = new HashMap<>();

            BigDecimal next = (BigDecimal) list.get(0)[1];

            if (next != null) {
                mapIds.put(NEXT, next.longValue());
            } else {
                mapIds.put(NEXT, 0L);
            }

            BigDecimal previous = (BigDecimal) list.get(0)[0];

            if (previous != null) {
                mapIds.put(PREVIOUS, previous.longValue());
            } else {
                mapIds.put(PREVIOUS, 0L);
            }

            return mapIds;

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }


    @Override
    public List<News> fetchSearchLimit(Filter filter, int startPosition, int endPosition) throws DAOException {

        Session session = null;

        try {
            session = sessionFactory.openSession();


            Criteria criteria = session.createCriteria(News.class, "news");
            criteria.setFetchMode("news.authors", FetchMode.JOIN);
            criteria.setFetchMode("news.tags", FetchMode.JOIN);

            criteria.setFirstResult(startPosition - 1);
            criteria.setMaxResults(endPosition);

            criteria = generateCriteria(criteria, filter);

            ProjectionList columns = Projections.projectionList()
                    .add(Projections.distinct(Projections.property("id")), "id")
                    .add(Projections.property("title"), "title")
                    .add(Projections.property("shortText"), "shortText")
                    .add(Projections.property("fullText"), "fullText")
                    .add(Projections.property("creationDate"), "creationDate")
                    .add(Projections.property("modificationDate"), "modificationDate");

            criteria.setProjection(columns);
            return (List<News>) criteria.list();

        } catch (HibernateException e){
            throw new DAOException(e);
        }
    }

    @Override
    public int getCountSearchRows(Filter filter) throws DAOException {
        Session session = null;

        try {
            session = sessionFactory.openSession();


            Criteria criteria = session.createCriteria(News.class, "news");

            criteria = generateCriteria(criteria, filter);

            ProjectionList columns = Projections.projectionList()
                    .add(Projections.distinct(Projections.count(ID)));
            criteria.setProjection(columns);
            return (int) (long) criteria.list().get(0);

        } catch (HibernateException e){
            throw new DAOException(e);
        }
    }

    @Override
    @Transactional
    public Long addCustom(News news) throws DAOException {

        Session session = null;
        Tag tag;

        try {

            session = sessionFactory.getCurrentSession();
            tag = new Tag();
            tag.setTagName("sdfds");


        }catch (HibernateException e){
            throw new DAOException(e);
        }

        return tag.getId();
    }

    private Criteria generateCriteria(Criteria criteria, Filter filter) {

        final Long authorId = filter.getAuthorId();
        List<Long> tagsId = filter.getTagsId();

        if (filter.isFull()) {

            criteria.createAlias("news.authors", "authors");
            criteria.add(Restrictions.and(Restrictions.eq("authors.id", authorId)));
            criteria.createAlias("news.tags", "tags");
            criteria.add(Restrictions.and(Restrictions.in("tags.id", tagsId)));

        } else if (authorId == null || authorId == 0L) {

            criteria.createAlias("news.tags", "tags");
            criteria.add(Restrictions.and(Restrictions.in("tags.id", tagsId)));

        } else if (tagsId == null || tagsId.isEmpty()) {

            criteria.createAlias("news.authors", "authors");
            criteria.add(Restrictions.and(Restrictions.eq("authors.id", authorId)));

        }

        return criteria;
    }
}
