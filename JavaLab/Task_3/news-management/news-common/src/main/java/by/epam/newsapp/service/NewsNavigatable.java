package by.epam.newsapp.service;

import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.util.Page;

import java.util.Map;

/**
 * The service provides methods for working with navigation on the news
 */
public interface NewsNavigatable {

    /**
     *
     * The method allows get next and previous news ID.
     *
     * @param currentNewsId - the current news ID
     * @return the map, that keep previous value under 'previous' key and next value under 'next' key.
     * @throws DAOException
     */
    Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws TechnicalException;

}
