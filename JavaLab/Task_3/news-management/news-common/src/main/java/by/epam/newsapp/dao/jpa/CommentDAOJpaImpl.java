package by.epam.newsapp.dao.jpa;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.exception.DAOException;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 5/15/2015.
 */
@Repository("commentDAOJpa")
public class CommentDAOJpaImpl extends  AbstractDAOJpa<Comment> implements CommentDAO {


}
