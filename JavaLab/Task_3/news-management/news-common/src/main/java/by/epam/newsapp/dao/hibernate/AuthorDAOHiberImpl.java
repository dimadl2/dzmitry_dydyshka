package by.epam.newsapp.dao.hibernate;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.dto.Author;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Dzmitry_Dydyshka on 4/29/2015.
 */
@Transactional
@Repository("authorDAOHiber")
public class AuthorDAOHiberImpl extends AbstractDAOHiber<Author> implements AuthorDAO {

    private static final String HQL_SELECT_NOT_EXPIRE = "FROM Author WHERE exp is null";
    private static final String HQL_SELECT_NOT_EXPIRE_AND_NEWS = "SELECT distinct a FROM News n RIGHT JOIN n.authors a WHERE a.expire is null OR  n.id = :id";
    private static final String HQL_FETCH_AUTHOR_BY_NAME = "FROM Author a WHERE a.name = :name";

    private static final String PARAM_NAME = "name";

    @Override
    public List<Author> getAllNotExpire() throws DAOException {

        try {

            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_SELECT_NOT_EXPIRE);

            return query.list();

        }catch (HibernateException e){
            throw new DAOException(e);
        }


    }

    @Override
    public void expire(Long authorID) throws DAOException {

        try {
            Session session = sessionFactory.getCurrentSession();
            Author author = (Author) session.get(Author.class, authorID);
            author.setExpire(new Timestamp((new Date().getTime())));
            session.update(author);

        } catch (HibernateException e) {

            throw new DAOException(e);

        }
    }

    @Override
    public List<Author> fetchNotExpireAndNewsAuthor(Long newsId) throws DAOException {


        try {

            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(HQL_SELECT_NOT_EXPIRE_AND_NEWS);
            query.setParameter("id", newsId);
            List list = query.list();

            return list;


        } catch (HibernateException e) {

            throw new DAOException(e);

        }

    }

    @Override
    public Author saveOrReturn(String name) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(HQL_FETCH_AUTHOR_BY_NAME);
        query.setParameter(PARAM_NAME, name);
        Author author = (Author) query.uniqueResult();
        if(author == null){
            author = new Author(name);
            session.persist(author);
        }

        return author;
    }

}
