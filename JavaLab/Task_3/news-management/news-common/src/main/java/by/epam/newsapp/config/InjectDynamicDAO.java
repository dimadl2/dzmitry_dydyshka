package by.epam.newsapp.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectDynamicDAO {

    Class hibernate();
    Class jpa();

}
