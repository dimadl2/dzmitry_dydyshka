package by.epam.newsapp.dao.jpa;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.dto.Author;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.Query;
import java.util.*;

/**
 * Created by Dzmitry_Dydyshka on 5/13/2015.
 */
@Transactional
@Repository("authorDAOJpa")
public class AuthorDAOJpaImpl extends AbstractDAOJpa<Author> implements AuthorDAO {

    private static final String JQL_FETCH_ALL_NOT_EXPIRE_AUTHOR_AND_NEWS = "SELECT distinct a FROM News n RIGHT JOIN n.authors a WHERE a.expire is null OR  n.id = :id";
    private static final String JQL_FETCH_ALL_NOT_EXPIRE_AUTHOR = "from Author where expire is null";
    private static final String JQL_FETCG_AUTHOR_BY_NAME = "FROM Author a WHERE a.name = :name";

    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "name";




    @Override
    public List<Author> getAllNotExpire() throws DAOException {
        return em.createQuery(JQL_FETCH_ALL_NOT_EXPIRE_AUTHOR).getResultList();
    }

    @Override
    public void expire(Long authorID) throws DAOException {
        Author author = em.find(Author.class, authorID);
        author.setExpire(new Date());
        em.merge(author);
    }

    @Override
    public List<Author> fetchNotExpireAndNewsAuthor(Long newsId) throws DAOException {

        Query query = em.createQuery(JQL_FETCH_ALL_NOT_EXPIRE_AUTHOR_AND_NEWS);
        query.setParameter(PARAM_ID, newsId);
        return query.getResultList();
    }

    @Override
    public Author saveOrReturn(String name) {

        Query query = em.createQuery(JQL_FETCG_AUTHOR_BY_NAME);
        query.setParameter(PARAM_NAME, name);
        Author author = (Author) query.getSingleResult();
        if (author == null) {
            author = new Author(name);
            em.persist(author);
        }

        return author;

    }


}
