package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.exception.TechnicalException;

/**
 * The Interface Service.
 * 
 * The Service provides four method for operating with data object.
 *
 * @param <T>
 *            Type of data object
 *
 */
public interface Service<T> {

	/**
	 * Adds the object to database.
	 *
	 * @param object
	 *            the added object
	 * @return the id of new object
	 * @throws by.epam.newsapp.exception.TechnicalException
	 *             the technical exception
	 */
	public Long save(T object) throws TechnicalException;

	/**
	 * Delete the object from database.
	 *
	 * @param id
	 *            the id of deleted object
	 * @throws by.epam.newsapp.exception.TechnicalException
	 *             the technical exception
	 */
	public boolean delete(Long id) throws TechnicalException;

	/**
	 * Update existing object.
	 *
	 * @param object
	 *            the updated object
	 * @return true, if successful; false, if failure
	 * @throws by.epam.newsapp.exception.TechnicalException
	 *             the technical exception
	 */
	public boolean update(T object) throws TechnicalException;

	/**
	 * Fetch the object from database by id.
	 *
	 * @param id
	 *            the id of desired object
	 * @return the found object
	 * @throws by.epam.newsapp.exception.TechnicalException
	 *             the technical exception
	 */
	public T fetchById(Long id) throws TechnicalException;

	/**
	 * choose all objects of type T from database .
	 *
	 * @return the list of objects
	 * @throws by.epam.newsapp.exception.TechnicalException
	 *             the technical exception
	 */
	public List<T> list() throws TechnicalException;
}
