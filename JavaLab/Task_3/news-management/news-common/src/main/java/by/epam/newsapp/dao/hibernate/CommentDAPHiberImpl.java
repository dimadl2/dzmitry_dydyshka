package by.epam.newsapp.dao.hibernate;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dto.Comment;
import org.springframework.stereotype.Repository;

/**
 * Created by Dzmitry_Dydyshka on 4/29/2015.
 */
@Repository("commentDAOHiber")
public class CommentDAPHiberImpl extends AbstractDAOHiber<Comment> implements CommentDAO {

}
