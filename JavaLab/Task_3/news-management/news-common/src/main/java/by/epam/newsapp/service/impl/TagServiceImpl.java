package by.epam.newsapp.service.impl;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dao.hibernate.TagDAOHiberImpl;
import by.epam.newsapp.dao.jpa.TagDAOJpaImpl;
import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.config.InjectDynamicDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The Class TagServiceImpl.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TagServiceImpl implements TagService {


    /**
     * The tag dao.
     */

    @InjectDynamicDAO(hibernate = TagDAOHiberImpl.class,
            jpa = TagDAOJpaImpl.class)
    private TagDAO tagDAO;

    /**
     * The log.
     */
    private static Logger log = Logger.getLogger(TagServiceImpl.class);

    /**
     * @see by.epam.newsapp.service.Service#save(Object)
     */
    @Override
    public Long save(Tag tag) throws TechnicalException {

        try {
            return tagDAO.add(tag);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see by.epam.newsapp.service.Service#delete(Long)
     */
    @Override
    public boolean delete(Long tagId) throws TechnicalException {
        boolean flagResult = false;
        try {

            Tag tag = tagDAO.fetchById(tagId);
            if (tag != null) {

                tagDAO.delete(tagId);
                flagResult = true;

            }

        } catch (DAOException e) {

            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#update(Object)
     */
    @Override
    public boolean update(Tag tag) throws TechnicalException {

        boolean flagResult = false;

        try {

            Tag updatedTag = tagDAO.fetchById(tag.getId());

            if (updatedTag != null) {

                tagDAO.update(tag);
                flagResult = true;

            }
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#fetchById(Long)
     */
    @Override
    public Tag fetchById(Long tagId) throws TechnicalException {

        try {
            return tagDAO.fetchById(tagId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see by.epam.newsapp.service.Service#list()
     */
    @Override
    public List<Tag> list() throws TechnicalException {

        try {
           return  tagDAO.list();
        } catch (DAOException e) {

            throw new TechnicalException(e);

        }

    }

    @Override
    public List<Tag> saveAndReturn(List<String> names) {
        return tagDAO.saveAndReturn(names);
    }
}
