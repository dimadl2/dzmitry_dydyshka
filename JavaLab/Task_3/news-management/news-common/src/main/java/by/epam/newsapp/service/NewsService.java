package by.epam.newsapp.service;

import java.sql.Timestamp;
import java.util.List;

import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.util.Page;
import org.springframework.transaction.annotation.Transactional;

/**
 * The service provides methods for working with news DAO
 */
@Transactional
public interface NewsService extends Service<News>, NewsSearchable, NewsNavigatable {

	/**
	 * The method allows remove list of news.
	 *
	 * @param newsIds - The list of news ID to be removed
	 * @throws TechnicalException
	 */
	void delete(List<Long> newsIds) throws TechnicalException;
}
