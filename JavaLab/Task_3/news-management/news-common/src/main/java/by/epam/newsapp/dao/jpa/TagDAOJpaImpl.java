package by.epam.newsapp.dao.jpa;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dto.Tag;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 5/15/2015.
 */
@Repository("tagDAOJpa")
public class TagDAOJpaImpl extends AbstractDAOJpa<Tag> implements TagDAO {


    private static final String JQL_FETCH_TAGS_BY_NAME = "FROM Tag t WHERE t.tagName IN (:list)";

    @Override
    public List<Tag> saveAndReturn(List<String> names) {

        List<Tag> result = new LinkedList<>();

        if (names.isEmpty()) {
            return result;
        }

        Query query = em.createQuery(JQL_FETCH_TAGS_BY_NAME);
        query.setParameter("list", names);

        List<Tag> list = query.getResultList();


        for (Tag tag : list) {
            if (names.contains(tag.getTagName())) {
                names.remove(tag.getTagName());
            }
        }

        for (String name : names) {
            Tag tag = new Tag(name);
            em.persist(tag);
            result.add(tag);
        }

        result.addAll(list);

        return result;


    }
}
