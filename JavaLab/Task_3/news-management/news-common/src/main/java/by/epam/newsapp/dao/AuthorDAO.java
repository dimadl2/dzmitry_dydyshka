package by.epam.newsapp.dao;

import by.epam.newsapp.dto.Author;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 *
 */
public interface AuthorDAO extends CommonDAO<Author> {

    /**
     * Return not expire authors
     *
     * @return Return not expire authors
     * @throws DAOException
     */
    List<Author> getAllNotExpire() throws DAOException;

    /**
     *
     * Set author as expire authors
     *
     * @param authorID - id author for expireing
     * @throws DAOException
     */
    void expire(Long authorID) throws DAOException;

    /**
     *
     * Method for fetching all not expire authors and author of news
     *
     * @param newsId - id news
     * @return - list of not expire authors and author of news
     * @throws DAOException
     */
    List<Author> fetchNotExpireAndNewsAuthor(Long newsId) throws DAOException;

    Author saveOrReturn(String name);

}
