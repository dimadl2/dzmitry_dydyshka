package by.epam.newsapp.util;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/10/2015.
 */
public class Filter {

    private Long authorId;
    private List<Long> tagsId;

    public Filter() {
    }

    public Filter(Long authorId) {
        this.authorId = authorId;
    }

    public Filter(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Filter(Long authorId, List<Long> tagsId) {
        this.authorId = authorId;
        this.tagsId = tagsId;
    }

    public boolean isFull(){

        if (authorId != null && tagsId != null) {
            if (authorId != 0L && !tagsId.isEmpty()) {
                return true;
            }
        }

        return false;

    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }
}
