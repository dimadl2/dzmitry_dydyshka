package by.epam.newsapp.dao.hibernate;

import by.epam.newsapp.dao.CommonDAO;
import by.epam.newsapp.dto.Entity;
import by.epam.newsapp.exception.DAOException;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/28/2015.
 */
@Transactional
public class AbstractDAOHiber<T extends Entity> implements CommonDAO<T> {

    @Autowired
    protected SessionFactory sessionFactory;

    private final static Logger LOG = Logger.getLogger(AbstractDAOHiber.class);

    @Override
    public List<T> list() throws DAOException {

        LOG.info("THIS IS HIBERNATE!");

        List<T> objects = null;

        try {

            Session session = sessionFactory.getCurrentSession();
            objects = session.createCriteria(getTypeArgument()).list();

            return objects;

        } catch (HibernateException e) {

            throw new DAOException(e);

        }
    }

    @Override
    public Long add(T object) throws DAOException {

        try {
            Session session = sessionFactory.getCurrentSession();
            Long id = (Long) session.save(object);

            return id;
        } catch (HibernateException e) {

            throw new DAOException(e);

        }

    }

    @Override
    public void delete(Long id) throws DAOException {

        try {
            Session session = sessionFactory.getCurrentSession();
            T object = (T) session.get(getTypeArgument(), id);
            session.delete(object);
        } catch (HibernateException e) {

            throw new DAOException(e);

        }
    }

    @Override
    public void update(T object) throws DAOException {

        try {
            Session session = sessionFactory.getCurrentSession();
            session.update(object);

        } catch (HibernateException e) {

            throw new DAOException(e);

        }
    }

    @Override
    public T fetchById(Long id) throws DAOException {

        Session session = sessionFactory.getCurrentSession();
        T object = (T) session.get(getTypeArgument(), id);

        return object;
    }

    /**
     *
     * The method allows to get type of generic.
     *
     * @return
     */
    private Class<?> getTypeArgument() {
        ParameterizedType type = (ParameterizedType) getClass()
                .getGenericSuperclass();
        return (Class<?>) type.getActualTypeArguments()[0];
    }
}
