package by.epam.newsapp.dao.jpa;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dao.CommonDAO;
import by.epam.newsapp.dto.Entity;
import by.epam.newsapp.exception.DAOException;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 5/13/2015.
 */
@Transactional
public class AbstractDAOJpa<T extends Entity> implements CommonDAO<T> {

    private static final Logger LOG = Logger.getLogger(AbstractDAOJpa.class);

    @PersistenceContext
    protected EntityManager em;

    @Override
    public List<T> list() throws DAOException {

        LOG.info("THIS IS JPA!");
        return em.createQuery("from " + getTypeArgument().getName()).getResultList();
    }


    @Override
    public Long add(T object) throws DAOException {
        em.persist(object);
        em.flush();
        return ((Entity) object).getId();
    }

    @Override
    public void delete(Long id) throws DAOException {

        T object = (T) em.find(getTypeArgument(), id);
        em.remove(object);
    }


    @Override
    public void update(T object) throws DAOException {
        em.merge(object);
    }

    @Override
    public T fetchById(Long id) throws DAOException {
        return (T) em.find(getTypeArgument(), id);
    }

    /**
     * Gets the argument type.
     *
     * @return the argument type
     */
    private Class<?> getTypeArgument() {
        ParameterizedType type = (ParameterizedType) getClass()
                .getGenericSuperclass();
        return (Class<?>) type.getActualTypeArguments()[0];
    }

}
