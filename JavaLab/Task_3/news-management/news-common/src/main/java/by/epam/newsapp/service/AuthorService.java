package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.Author;
import by.epam.newsapp.exception.TechnicalException;

/**
 * The service provides methods for working with authors DAO
 */
public interface AuthorService extends Service<Author> {


	/**
	 *
	 * The method allows fetch all not expire authors
	 *
	 * @return the list of authors
	 */
	List<Author> getAllNotExpire() throws TechnicalException;

	/**
	 * The method allows expire the author
	 *
 	 */
	void expire(Long authorId) throws TechnicalException;

	/**
	 *
	 * The method allows fetch the list of expire authors and
	 * news author, even if he is expire.
	 *
	 * @param newsId - news id.
	 *
	 * @return the list of authors
	 */
	List<Author> fetchExpireAndNewsAuthor(Long newsId) throws TechnicalException;

	Author saveOrReturn(String name);
}
