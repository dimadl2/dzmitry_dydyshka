package by.epam.newsapp.service.impl;

import java.util.List;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dao.hibernate.CommentDAPHiberImpl;
import by.epam.newsapp.dao.jpa.CommentDAOJpaImpl;
import by.epam.newsapp.service.CommentService;
import by.epam.newsapp.config.InjectDynamicDAO;
import org.apache.log4j.Logger;

import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

/**
 * The Class CommentServiceImpl.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CommentServiceImpl implements CommentService {

    /**
     * The comment data access object.
     */

    @InjectDynamicDAO(hibernate = CommentDAPHiberImpl.class,
                        jpa = CommentDAOJpaImpl.class)
    private CommentDAO commentDAO;

    /**
     * The log.
     */
    private static Logger log = Logger.getLogger(CommentServiceImpl.class);

    /**
     * @see by.epam.newsapp.service.Service#save(Object)
     */
    @Override
    public Long save(Comment comment) throws TechnicalException {

        try {
            return commentDAO.add(comment);
        } catch (DAOException e) {

            throw new TechnicalException(e);

        }

    }

    /**
     * @see by.epam.newsapp.service.Service#delete(Long)
     */
    @Override
    public boolean delete(Long id) throws TechnicalException {

        boolean flagResult = false;
        try {

            Comment comment = commentDAO.fetchById(id);
            if (comment != null) {

                commentDAO.delete(id);
                flagResult = true;

            }

        } catch (DAOException e) {

            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#update(Object)
     */
    @Override
    public boolean update(Comment comment) throws TechnicalException {

        boolean flagResult = false;
        try {

            Comment updatedComment = commentDAO.fetchById(comment.getId());

            if (updatedComment != null) {

                commentDAO.update(comment);
                flagResult = true;
            }

        } catch (DAOException e) {

            throw new TechnicalException(e);

        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#fetchById(Long)
     */
    @Override
    public Comment fetchById(Long id) throws TechnicalException {

        Comment comment = null;

        try {
            comment = commentDAO.fetchById(id);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return comment;
    }

    /**
     * @see by.epam.newsapp.service.Service#list()
     */
    @Override
    public List<Comment> list() throws TechnicalException {
        List<Comment> list = null;
        try {
            list = commentDAO.list();
        } catch (DAOException e) {

            throw new TechnicalException(e);
        }

        return list;
    }

}
