package by.epam.newsapp.service.impl;

import java.util.List;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.dao.hibernate.AuthorDAOHiberImpl;
import by.epam.newsapp.dao.jpa.AuthorDAOJpaImpl;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.config.InjectDynamicDAO;
import org.apache.log4j.Logger;

import by.epam.newsapp.dto.Author;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

/**
 * The Class AuthorServiceImpl.
 */
@Service("authorService")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AuthorServiceImpl implements AuthorService {

	/** The author dao. */

	@InjectDynamicDAO(hibernate = AuthorDAOHiberImpl.class,
						jpa = AuthorDAOJpaImpl.class)
	private AuthorDAO authorDAO;

	private static Logger log = Logger.getLogger(AuthorServiceImpl.class);


	/**
	 * 
	 * @see by.epam.newsapp.service.Service#save(Object)
	 * 
	 */
	@Override
	public Long save(Author author) throws TechnicalException {

		Long id = null;

		try {

			id = authorDAO.add(author);

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return id;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#delete(Long)
	 * 
	 */
	@Override
	public boolean delete(Long authorId) throws TechnicalException {

		boolean flagResult = false;
		try {

			Author author = authorDAO.fetchById(authorId);
			if (author != null) {

				authorDAO.delete(authorId);
				flagResult = true;
			} 

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return flagResult;
	}

	/**
	 * @see by.epam.newsapp.service.Service#update(Object)
	 */
	@Override
	public boolean update(Author author) throws TechnicalException {

		boolean flagResult = false;

		try {

			Author updatedAuthor = authorDAO.fetchById(author.getId());

			if (updatedAuthor != null) {

				authorDAO.update(author);
				flagResult = true;

			}

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#fetchById(Long)
	 */
	@Override
	public Author fetchById(Long id) throws TechnicalException {

		Author author = null;

		try {

			author = authorDAO.fetchById(id);

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return author;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#list()
	 */
	@Override
	public List<Author> list() throws TechnicalException {

		List<Author> list = null;

		try {

			list = authorDAO.list();

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return list;

	}


	@Override
	public List<Author> getAllNotExpire() throws TechnicalException {

		try {
			return authorDAO.getAllNotExpire();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public void expire(Long authorId) throws TechnicalException {

		try {
			authorDAO.expire(authorId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public List<Author> fetchExpireAndNewsAuthor(Long newsId) throws TechnicalException {
		try {
			return authorDAO.fetchNotExpireAndNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public Author saveOrReturn(String name) {
		return authorDAO.saveOrReturn(name);
	}
}
