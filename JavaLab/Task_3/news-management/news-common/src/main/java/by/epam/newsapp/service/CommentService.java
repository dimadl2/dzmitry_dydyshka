package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.exception.TechnicalException;

/**
 *
 * The service provides methods for working with comment DAO
 *
 */
public interface CommentService extends Service<Comment> {

}
