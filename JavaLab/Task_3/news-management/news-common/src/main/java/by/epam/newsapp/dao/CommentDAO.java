package by.epam.newsapp.dao;

import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/28/2015.
 */
public interface CommentDAO extends CommonDAO<Comment> {

}
