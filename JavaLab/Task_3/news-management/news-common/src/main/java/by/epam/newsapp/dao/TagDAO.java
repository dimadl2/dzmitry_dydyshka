package by.epam.newsapp.dao;

import by.epam.newsapp.dto.Author;
import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/28/2015.
 */
public interface TagDAO extends CommonDAO<Tag> {

    /**
     *
     * The method allows to obtain the list of author by list of tag names.
     * If tag name doesn't contain in database, method will insert it and return for
     * result list.
     * If author name contains in database, method will return it for result list.
     *
     * @param names - the list of tags names.
     * @return - the list of tags.
     */
    List<Tag> saveAndReturn(List<String> names);
}
