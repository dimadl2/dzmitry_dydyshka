package by.epam.newsapp.dto;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class Tag.
 */
@javax.persistence.Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "tag")
public class Tag implements Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
	@SequenceGenerator(name="TAG_SEQ", sequenceName = "TAG_SEQ", allocationSize = 1)
	@Column(name = "tag_id")
	private Long id;

	/** The tag name. */
	@Column(name = "tag_name")
	private String tagName;

	/**
	 * Instantiates a new tag.
	 */
	public Tag() {

	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param id
	 *            the id
	 * @param tagName
	 *            the tag name
	 */
	public Tag(Long id, String tagName) {
		this.id = id;
		this.tagName = tagName;
	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param tagName
	 *            the tag name
	 */

	public Tag(String tagName){
		this.tagName = tagName;
	}

	/**
	 * Gets the tag name.
	 *
	 * @return the tagName
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the tag name.
	 *
	 * @param tagName
	 *            the tagName to set
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

}
