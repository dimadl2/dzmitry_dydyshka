package by.epam.newsapp.service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dao.hibernate.NewsDAOHiberImpl;
import by.epam.newsapp.dao.jpa.NewsDAOJpaImpl;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.config.InjectDynamicDAO;
import by.epam.newsapp.util.Page;
import org.apache.log4j.Logger;

import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

/**
 * The Class NewsServiceImpl.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NewsServiceImpl implements NewsService {


    /**
     * The news dao.
     */

    @InjectDynamicDAO(hibernate = NewsDAOHiberImpl.class,
                        jpa=NewsDAOJpaImpl.class)
    private NewsDAO newsDAO;


    /**
     * The logger.
     */
    private static Logger log = Logger.getLogger(NewsServiceImpl.class);

    /*
     * @see Service#save(Entity)
     *
     */
    @Override
    public Long save(News news) throws TechnicalException {

        Long newsId = null;
        try {

            newsId = newsDAO.add(news);

        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return newsId;
    }

    /**
     * @see by.epam.newsapp.service.Service#delete(Long)
     */
    @Override
    public boolean delete(Long newsId) throws TechnicalException {

        boolean flagResult = false;
        try {

            News news = newsDAO.fetchById(newsId);
            if (news != null) {

                newsDAO.delete(newsId);
                flagResult = true;

            }

        } catch (DAOException e) {

            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#update(Object)
     */
    @Override
    public boolean update(News news) throws TechnicalException {

        boolean flagResult = false;

        try {

            News updatedNews = newsDAO.fetchById(news.getId());

            if (updatedNews != null) {

                newsDAO.update(news);
                flagResult = true;

            }

        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#fetchById(Long)
     */
    @Override
    public News fetchById(Long newsId) throws TechnicalException {

        News news = null;
        try {

            news = newsDAO.fetchById(newsId);

        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return news;

    }

    /**
     * @see by.epam.newsapp.service.Service#list()
     */
    @Override
    public List<News> list() throws TechnicalException {
        List<News> list = null;
        try {
            list = newsDAO.list();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return list;
    }

    /**
     * @see by.epam.newsapp.service.Service#delete(Long)
     */
    @Override
    public void delete(List<Long> newsIds) throws TechnicalException {

        try {
            newsDAO.delete(newsIds);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see by.epam.newsapp.service.NewsService#fetchLimitNews(int, int)
     */
    @Override
    public List<News> fetchLimitNews(int startPosition, int endPosition) throws TechnicalException {

        try {
            return newsDAO.fetchLimitNews(startPosition, endPosition);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see NewsService#getCountRows()
     */
    @Override
    public int getCountRows() throws TechnicalException {

        try {
            return newsDAO.getCountRows();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public List<News> fetchSearchLimit(Filter filter, int startPosition, int endPosition) throws TechnicalException {

        try {
            return newsDAO.fetchSearchLimit(filter, startPosition, endPosition);
        }catch (DAOException e){
            throw new TechnicalException(e);
        }

    }

    @Override
    public int getCountSearchRows(Filter filter) throws TechnicalException {
        try {
            return newsDAO.getCountSearchRows(filter);
        }catch (DAOException e){
            throw new TechnicalException(e);
        }
    }


    /**
     * @see NewsService#getNextAndPreviousNewsId(Long)
     */
    @Override
    public Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws TechnicalException {

        try {
            return newsDAO.getNextAndPreviousNewsId(currentNewsId);
        }catch (DAOException e){
            throw new TechnicalException(e);
        }
    }


}
