package by.epam.newsapp.service.impl;

import by.epam.newsapp.dto.Author;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.dto.Tag;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsManage;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Dzmitry_Dydyshka on 7/17/2015.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NewsManageImpl implements NewsManage {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsService newsService;

    @Override
    public Long save(News news, List<Long> tagsId, Long authorId) throws TechnicalException {

        news = initNews(news, tagsId, authorId);

        Long id = newsService.save(news);

        return id;
    }

    @Override
    public Long save(News news, List<String> tagsName, String authorName) throws TechnicalException {

        final Author author = authorService.saveOrReturn(authorName);
        news.setAuthors(new HashSet<Author>() {
            {
                add(author);
            }
        });

        List<Tag> tags = tagService.saveAndReturn(tagsName);
        news.setTags(tags);

        return  newsService.save(news);
    }

    @Override
    public News fetchById(Long id) throws TechnicalException {
        return newsService.fetchById(id);
    }

    @Override
    public List<News> getAll() throws TechnicalException {
        return newsService.list();
    }

    @Override
    public void update(News news, List<Long> tagsId, Long authorId) throws TechnicalException {

        news = initNews(news, tagsId, authorId);
        newsService.update(news);
    }

    /**
     *
     * @see NewsManage#getPage(int, int)
     *
     * @param numberPage
     * @param numberAtPage - number item at the page
     * @return - The page, that contains the list of news with authors,
     *           tags and number of comments
     * @throws TechnicalException
     */

    @Override
    public Page getPage(int numberPage, int numberAtPage) throws TechnicalException {

        int startPosition = numberPage * numberAtPage - numberAtPage;

        List<News> listNews = newsService.fetchLimitNews(startPosition, numberAtPage);

        int numberAllNews = newsService.getCountRows();

        Page page = new Page(numberAtPage, numberAllNews, listNews);

        return page;

    }

    /**
     *
     * @see NewsManage#getFilteredPage(int, int, Filter)
     *
     * If the list of tags is empty, news search by authors.
     * If the ID authors is empty, news search by tags.
     *
     * @param numberPage - page number
     * @param numberAtPage - number item at the page
     * @param filter - filter for the page {@link Filter}
     * @return - page {@link Page} with the list of news with authors, tags and
     *          number of comments
     * @throws TechnicalException
     */
    @Override
    public Page getFilteredPage(int numberPage, int numberAtPage, Filter filter) throws TechnicalException {

        int startPosition = numberPage * numberAtPage + 1 - numberAtPage;
        int endPosition = numberPage * numberAtPage;

        List<News> listNews = newsService.fetchSearchLimit(filter, startPosition, endPosition);
        int numberAllNews = newsService.getCountSearchRows(filter);


        Page page = new Page(numberAtPage, numberAllNews, listNews);

        return page;

    }

    @Override
    public boolean verifyVersion(Long newsId, Timestamp oldVersion) throws TechnicalException {
        News news = newsService.fetchById(newsId);
        if (news.getModificationDate().equals(oldVersion)){
            return true;
        }else{
            return false;
        }
    }

    private News initNews(News news, List<Long> tagsId, Long authorId){
        Author authorTO = new Author();
        authorTO.setId(authorId);
        Set<Author> authors = new HashSet<>();
        authors.add(authorTO);

        List<Tag> tagsTO = new LinkedList<>();

        if (tagsId != null) {
            for (Long idTag : tagsId) {
                Tag tag = new Tag();
                tag.setId(idTag);
                tagsTO.add(tag);
            }
        }

        news.setAuthors(authors);
        news.setTags(tagsTO);

        return news;
    }
}
