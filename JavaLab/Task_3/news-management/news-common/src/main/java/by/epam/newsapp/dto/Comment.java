package by.epam.newsapp.dto;

import javax.persistence.*;
import java.util.Date;


/**
 * The Class Comment.
 */
@javax.persistence.Entity
@Table(name = "comments")
public class Comment implements Entity {

	/**
	 * 
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = COMMENTS_SEQ)
	@SequenceGenerator(name=COMMENTS_SEQ, sequenceName = COMMENTS_SEQ, allocationSize = 1)
	@Column(name="comments_id")
	private Long id;

	/** The comment text. */
	@Column(name = "comment_text")
	private String commentText;

	/** The creation date. */
	@Column(name = "creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	/** The news id. */

	@ManyToOne
	@JoinColumn(name = "news_id")
	private News news;

	private static final long serialVersionUID = 1L;
	private static final String COMMENTS_SEQ = "COMMENTS_SEQ";



	/**
	 * Instantiates a new comment.
	 */
	public Comment(){

	}



	/**
	 * Instantiates a new comment.
	 *
	 * @param commentText
	 *            the comment text
	 * @param creationDate
	 *            the creation date
	 * @param news
	 *            the news id
	 */
	public Comment(String commentText, Date creationDate, News news) {
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.news = news;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the commentText
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param id
	 *            the id
	 * @param commentText
	 *            the comment text
	 * @param creationDate
	 *            the creation date
	 * @param news
	 *            the news id
	 */
	public Comment(Long id, String commentText, Date creationDate, News news) {
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.news = news;
	}


	/**
	 * Sets the comment text.
	 *
	 * @param commentText
	 *            the commentText to set
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Sets the news id.
	 *
	 * @param news
	 *            the news to set
	 */
	public void setNews(News news) {
		this.news = news;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

}
