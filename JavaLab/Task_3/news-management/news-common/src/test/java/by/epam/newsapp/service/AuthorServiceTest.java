package by.epam.newsapp.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.dto.Author;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.impl.AuthorServiceImpl;

@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {

	@Mock
	private AuthorDAO authorDAO;

	@InjectMocks
	private AuthorServiceImpl service;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAdd() throws Exception {

		Long id = new Long(1);

		when(authorDAO.add(any(Author.class))).thenReturn(id);

		Author author = new Author(id, "Dima");

		Long authorId = service.save(author);
		Assert.assertEquals(id, authorId);
		verify(authorDAO).add(author);
		ArgumentCaptor<Author> authorCaptor = ArgumentCaptor
				.forClass(Author.class);
		verify(authorDAO).add(authorCaptor.capture());
		Author newAuthor = authorCaptor.getValue();
		Assert.assertEquals(author.getName(), newAuthor.getName());

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testAddThrowException() throws Exception {

		when(authorDAO.add(any(Author.class))).thenThrow(DAOException.class);

		Author author = new Author(new Long(1), "Dima");
		service.save(author);

		verify(authorDAO).add(author);

	}

	@Test
	public void testUpdate() throws Exception {

		Long id = new Long(1);

		Author author = new Author(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		Author updatedAuthor = new Author(id, "Sasha");
		boolean update = service.update(updatedAuthor);
		Assert.assertTrue(update);
		verify(authorDAO).fetchById(id);
		ArgumentCaptor<Author> authorCaptor = ArgumentCaptor
				.forClass(Author.class);
		verify(authorDAO).update(authorCaptor.capture());
		author = authorCaptor.getValue();
		Assert.assertEquals("Sasha", author.getName());

	}

	@Test
	public void testUpdateIfAuthorNotFound() throws Exception {

		Long id = new Long(1);
		when(authorDAO.fetchById(id)).thenReturn(null);
		Author author = new Author(id, "Dima");
		boolean update = service.update(author);
		Assert.assertFalse(update);
		verify(authorDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testUpdateThrowException() throws Exception {

		Long id = new Long(1);
		Author author = new Author(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		doThrow(DAOException.class).when(authorDAO).update(author);
		service.update(author);
		verify(authorDAO).fetchById(id);
		verify(authorDAO).update(author);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(1);
		Author author = new Author(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		boolean delete = service.delete(id);
		Assert.assertTrue(delete);
		verify(authorDAO).fetchById(id);
		verify(authorDAO).delete(id);
	}

	@Test
	public void testDeleteIfAuthorNotFound() throws Exception {

		Long id = new Long(1);
		when(authorDAO.fetchById(id)).thenReturn(null);
		boolean delete = service.delete(id);
		Assert.assertFalse(delete);
		verify(authorDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testDeleteThrowsException() throws Exception {

		Long id = new Long(1);
		Author author = new Author(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		doThrow(DAOException.class).when(authorDAO).delete(id);
		service.delete(id);
		verify(authorDAO).fetchById(id);
		verify(authorDAO).delete(id);

	}

	@Test
	public void testList() throws Exception {

		List<Author> authors = new LinkedList<Author>();
		authors.add(new Author(new Long(1), "Dima"));
		authors.add(new Author(new Long(2), "Max"));
		when(authorDAO.list()).thenReturn(authors);

		List<Author> resultAuthors = service.list();

		Assert.assertNotNull(authors);
		Assert.assertEquals(authors.size(), resultAuthors.size());
		Assert.assertArrayEquals(authors.toArray(), resultAuthors.toArray());

		verify(authorDAO).list();

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testListThrowsException() throws Exception {

		when(authorDAO.list()).thenThrow(DAOException.class);
		service.list();

	}

}
