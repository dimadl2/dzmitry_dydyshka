package by.epam.newsapp.dao.hibenate;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.dto.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.text.SimpleDateFormat;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})

public class CommentDAOHiberImplTest {

    private static final String PATH_TO_COMMENTS = "/comment/comment.xml";

    @Autowired
    @Qualifier("commentDAOHiber")
    private CommentDAO commentDAO;

    private Comment expected;

    @Before
    public void setUp() throws Exception{

        expected = new Comment();
        expected.setCommentText("test");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        expected.setCreationDate(dateFormat.parse("01/01/2015 11:10:00"));
        News news = new News();
        news.setId(1L);
        expected.setNews(news);

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testGetAll() throws Exception {

        List<Comment> list = commentDAO.list();

        Assert.assertNotNull(list);

        for (Comment comment : list) {
            Assert.assertNotNull(comment);
            if (comment.getId() == 1L) {

                Comment commentExp = new Comment();
                commentExp.setCommentText("test");
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy HH:mm:ss");
                commentExp.setCreationDate(dateFormat.parse("01/01/2015 11:10:00"));
                News news = new News();
                news.setId(1L);
                commentExp.setNews(news);
                commentExp.setId(1L);
                assertComments(commentExp, comment);

            }

        }

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testSave() throws Exception {

        Long id = commentDAO.add(expected);

        Comment actual = commentDAO.fetchById(id);

        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expected.getCommentText(), actual.getCommentText());



    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testDelete() throws Exception {

        Long id = 2L;
        commentDAO.delete(id);
        Comment actual = commentDAO.fetchById(id);

        Assert.assertNull(actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testFetchById() throws Exception {

        Long id = 1L;

        Comment actual = commentDAO.fetchById(id);
        Assert.assertNotNull(actual);

        Comment comment = new Comment();
        comment.setCommentText("test");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        comment.setCreationDate(dateFormat.parse("01/01/2015 11:10:00"));
        News news = new News();
        news.setId(1L);
        comment.setNews(news);
        comment.setId(1L);

        assertComments(comment, actual);

    }

    private void assertComments(Comment expected, Comment actual) {

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getCommentText(), actual.getCommentText());
        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());


    }

}
