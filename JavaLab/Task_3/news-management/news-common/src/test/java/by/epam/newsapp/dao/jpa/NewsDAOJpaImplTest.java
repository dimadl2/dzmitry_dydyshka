package by.epam.newsapp.dao.jpa;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dto.Comment;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.Filter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.newsapp.dto.News;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class })
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "/news/news.xml")
@Transactional(propagation = Propagation.REQUIRED)
public class NewsDAOJpaImplTest {

	private static final String PATH_TO_NEWS = "/news/news.xml";

	@Autowired
    @Qualifier("newsDAOJpa")
	private NewsDAO newsDAO;

    private News expectedNews;

    @Before
    public void setUp() throws Exception{

        expectedNews = new News();
        expectedNews.setShortText("test short");
        expectedNews.setFullText("test full");
        expectedNews.setTitle("test title");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        Date date = dateFormat.parse("01/01/2015 11:10:00");
        expectedNews.setCreationDate(date);
        expectedNews.setModificationDate(new Timestamp(date.getTime()));

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testList() throws Exception {

        List<News> list = newsDAO.list();

        Assert.assertNotNull(list);

        for (News news : list) {

            Assert.assertNotNull(news);

            if (news.getId() == 1L){

                News newsExp = new News();
                newsExp.setShortText("test short");
                newsExp.setFullText("test full");
                newsExp.setTitle("test title");
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy HH:mm:ss");
                Date date = dateFormat.parse("01/01/2015 11:10:00");
                newsExp.setCreationDate(date);
                newsExp.setModificationDate(new Timestamp(date.getTime()));
                newsExp.setId(1L);

               assertNews(newsExp, news);

            }

        }

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testSave() throws Exception {


        Long id = newsDAO.add(expectedNews);

        News actual = newsDAO.fetchById(id);

        Assert.assertEquals(expectedNews.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expectedNews.getFullText(), actual.getFullText());
        Assert.assertEquals(expectedNews.getShortText(), actual.getShortText());
        Assert.assertEquals(expectedNews.getModificationDate(), actual.getModificationDate());
        Assert.assertEquals(expectedNews.getTitle(), actual.getTitle());

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testDelete() throws Exception {

		newsDAO.delete(3L);
        News actual = newsDAO.fetchById(3L);
        Assert.assertNull(actual);

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testUpdate() throws Exception {

		News news = new News();
		news.setId(3l);
		news.setShortText("new");
		news.setFullText("new");
		news.setTitle("new");
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd/MM/yyyy HH:mm:ss");
		Date date = dateFormat.parse("01/01/2015 11:10:00");
		news.setCreationDate(date);
		news.setModificationDate(new Timestamp(date.getTime()));

		newsDAO.update(news);

        News actual = newsDAO.fetchById(3L);

        assertNews(news, actual);

    }

	@Test
    @DatabaseSetup(PATH_TO_NEWS)
	public void testFetchById() throws Exception {

		News actual = newsDAO.fetchById(1L);
        Assert.assertNotNull(actual);
        News newsExp = new News();
        newsExp.setShortText("test short");
        newsExp.setFullText("test full");
        newsExp.setTitle("test title");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        Date date = dateFormat.parse("01/01/2015 11:10:00");
        newsExp.setCreationDate(date);
        newsExp.setModificationDate(new Timestamp(date.getTime()));
        newsExp.setId(1L);
        assertNews(newsExp, actual);

        List<Comment> comments = actual.getComments();

        System.out.println(comments);

    }


    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void testGetCountRows() throws DAOException {

        int expected  = 3;
        int actual = newsDAO.getCountRows();

        Assert.assertEquals(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void testDeleteList() throws DAOException {

        List<Long> newsId = Arrays.asList(1L, 2L);
        newsDAO.delete(newsId);

        int expected = 1;
        int actual = newsDAO.getCountRows();

        Assert.assertEquals(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void testFetchLimit() throws DAOException {

        List<News> news = newsDAO.fetchLimitNews(1, 3);
        int expected = 3;
        int actual = news.size();

        Assert.assertEquals(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void fetchSearchOnlyAuthorLimit() throws DAOException {

        Filter filter = new Filter(2L);
        List<News> news = newsDAO.fetchSearchLimit(filter, 1, 10);
        Assert.assertEquals(2, news.size());


    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void fetchSearchOnlyTagsLimit() throws DAOException {

        Filter filter = new Filter(Arrays.asList(1L, 2L));

        List<News> news = newsDAO.fetchSearchLimit(filter, 1, 10);
        Assert.assertEquals(1, news.size());

    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void fetchSearchByTagsAndAuthor() throws DAOException {

        Filter filter = new Filter(2L, Arrays.asList(1L, 2L));

        List<News> news = newsDAO.fetchSearchLimit(filter, 1, 10);

        Assert.assertEquals(1, news.size());

    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void testGetCountSearchRows() throws DAOException {

        Filter filter = new Filter(2L);
        int count = newsDAO.getCountSearchRows(filter);
        Assert.assertEquals(2, count);

    }

    @Test
    @DatabaseSetup(PATH_TO_NEWS)
    public void testGetNextAndPrevious() throws DAOException {

        Map<String, Long> ids = newsDAO.getNextAndPreviousNewsId(2L);
        Assert.assertEquals((long)ids.get("previous"), 3L);
        Assert.assertEquals((long)ids.get("next"), 1L);

    }



    private void assertNews(News expected, News actual){

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expected.getFullText(), actual.getFullText());
        Assert.assertEquals(expected.getShortText(), actual.getShortText());
        Assert.assertEquals(expected.getModificationDate(), actual.getModificationDate());
        Assert.assertEquals(expected.getTitle(), actual.getTitle());

    }



}
