package by.epam.newsapp.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dto.Tag;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.impl.TagServiceImpl;

@ContextConfiguration(locations = { "classpath:/spring-test.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {

	@Mock
	private TagDAO tagDAO;

	@InjectMocks
	private TagServiceImpl service;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAdd() throws Exception {

		Long id = new Long(1);

		when(tagDAO.add(any(Tag.class))).thenReturn(id);

		Tag tag = new Tag();
		tag.setTagName("Tag1");

		Long tagId = service.save(tag);
		Assert.assertEquals(id, tagId);
		verify(tagDAO).add(tag);
		ArgumentCaptor<Tag> tagCaptor = ArgumentCaptor.forClass(Tag.class);
		verify(tagDAO).add(tagCaptor.capture());
		Tag newTag = tagCaptor.getValue();
		Assert.assertEquals("Tag1", newTag.getTagName());

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testAddThrowException() throws Exception {

		when(tagDAO.add(any(Tag.class))).thenThrow(DAOException.class);

		Tag tag = new Tag(new Long(1), "Tag1");
		service.save(tag);

		verify(tagDAO).add(tag);

	}

	@Test
	public void testUpdate() throws Exception {

		Long id = new Long(1);

		Tag tag = new Tag(id, "Tag1");
		when(tagDAO.fetchById(id)).thenReturn(tag);

		Tag updatedTag = new Tag(id, "Tag2");

		boolean update = service.update(updatedTag);

		Assert.assertTrue(update);
		verify(tagDAO).fetchById(id);

		ArgumentCaptor<Tag> tagCaptor = ArgumentCaptor.forClass(Tag.class);
		verify(tagDAO).update(tagCaptor.capture());
		tag = tagCaptor.getValue();
		Assert.assertEquals("Tag2", tag.getTagName());

	}

	@Test
	public void testUpdateIfTagNotFound() throws Exception {

		Long id = new Long(1);
		when(tagDAO.fetchById(id)).thenReturn(null);
		Tag tag = new Tag(id, "Tag1");
		boolean update = service.update(tag);
		Assert.assertFalse(update);
		verify(tagDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testUpdateThrowException() throws Exception {

		Long id = new Long(1);
		Tag tag = new Tag(id, "Tag1");
		when(tagDAO.fetchById(id)).thenReturn(tag);
		doThrow(DAOException.class).when(tagDAO).update(tag);
		service.update(tag);
		verify(tagDAO).fetchById(id);
		verify(tagDAO).update(tag);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(1);
		Tag tag = new Tag(id, "Tag1");
		when(tagDAO.fetchById(id)).thenReturn(tag);
		boolean delete = service.delete(id);
		Assert.assertTrue(delete);
		verify(tagDAO).fetchById(id);
		verify(tagDAO).delete(id);
	}

	@Test
	public void testDeleteIfTagNotFound() throws Exception {

		Long id = new Long(1);
		when(tagDAO.fetchById(id)).thenReturn(null);
		boolean delete = service.delete(id);
		Assert.assertFalse(delete);
		verify(tagDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testDeleteThrowsException() throws Exception {

		Long id = new Long(1);
		Tag tag = new Tag(id, "Tag1");
		when(tagDAO.fetchById(id)).thenReturn(tag);
		doThrow(DAOException.class).when(tagDAO).delete(id);
		service.delete(id);
		verify(tagDAO).fetchById(id);
		verify(tagDAO).delete(id);

	}

	@Test
	public void testList() throws Exception {

		List<Tag> tags = new LinkedList<Tag>();
		Tag tag1 = new Tag(new Long(1), "Tag1");
		Tag tag2 = new Tag(new Long(2), "Tag2");
		tags.add(tag1);
		tags.add(tag2);
		when(tagDAO.list()).thenReturn(tags);

		List<Tag> resultTags = service.list();

		Assert.assertNotNull(tags);
		Assert.assertEquals(2, resultTags.size());

		verify(tagDAO).list();

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testListThrowsException() throws Exception {

		when(tagDAO.list()).thenThrow(DAOException.class);
		service.list();

	}

}
