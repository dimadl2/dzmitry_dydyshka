package by.epam.newsapp.dao.jpa;

import java.util.List;

import by.epam.newsapp.dao.AuthorDAO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.newsapp.dto.Author;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class AuthorDAOJpaImplTest {

    private static final String PATH_TO_AUTHORS = "/author/author.xml";


    @Autowired
    @Qualifier("authorDAOJpa")
    private AuthorDAO authorDAO;


    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testList() throws Exception {

        List<Author> list = authorDAO.list();

        Assert.assertNotNull(list);

        Author expected = new Author(new Long(1), "Oleg");
        for (Author author : list) {

            Assert.assertNotNull(author);
            if (author.getId() == 1L) {

                assertAuthor(expected, author);

            }

        }

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testSave() throws Exception {

        Author expected = new Author("Vasia");
        Long id = authorDAO.add(expected);
        Author actual = authorDAO.fetchById(id);

        Assert.assertEquals(expected.getName(), actual.getName());

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testDelete() throws Exception {

        Long id = 3L;

        authorDAO.delete(id);
        Author actual = authorDAO.fetchById(id);

        Assert.assertNull(actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testUpdate() throws Exception {

        Long id = 3L;
        Author expected = new Author(id, "Max");

        authorDAO.update(expected);

        Author actual = authorDAO.fetchById(id);
        assertAuthor(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testFetchById() throws Exception {

        Long id = 3L;
        Author actual = authorDAO.fetchById(id);
        Author expected = new Author(id, "Dima");
        Assert.assertNotNull(actual);

        assertAuthor(expected, actual);

    }


    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testGetAllNotExpire() throws Exception {

        List<Author> authors = authorDAO.getAllNotExpire();

        Assert.assertEquals(2, authors.size());

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testExpire() throws Exception {

        authorDAO.expire(2L);

        List<Author> authors = authorDAO.getAllNotExpire();

        Assert.assertEquals(1, authors.size());

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testFetchNotExpireAndNewsAuthor() throws Exception {

        List<Author> authors = authorDAO.fetchNotExpireAndNewsAuthor(1L);

        Assert.assertEquals(2, authors.size());

    }


    private void assertAuthor(Author expected, Author actual) {

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getName(), actual.getName());

    }

}
