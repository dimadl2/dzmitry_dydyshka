package by.epam.newsapp.dao.hibenate;

import by.epam.newsapp.dao.TagDAO;
import by.epam.newsapp.dto.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TagDAOHiberImplTest {

    public static final String PATH_TO_TAGS = "/tag/tag.xml";

    @Autowired
    @Qualifier("tagDAOJpa")
    private TagDAO tagDAO;

    private Tag expected;

    @Before
    public void setUp() {

        expected = new Tag(1L, "Tag1");

    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testList() throws Exception {

        List<Tag> tags = tagDAO.list();
        Assert.assertNotNull(tags);

        for (Tag tag : tags) {

            Assert.assertNotNull(tag);

            if (tag.getId() == 1L) {
                assertTags(expected, tag);
            }

        }

    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testAdd() throws Exception {

        Tag tag = new Tag();
        tag.setTagName("Tag4");

        Long id = tagDAO.add(tag);
        Tag actual = tagDAO.fetchById(id);
        Assert.assertEquals(tag.getTagName(), actual.getTagName());


    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testDelete() throws Exception {

        tagDAO.delete(3L);
        Tag actual = tagDAO.fetchById(3L);
        Assert.assertNull(actual);


    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testUpdate() throws Exception {

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setTagName("TagUpdate");

        tagDAO.update(tag);

        Tag actual = tagDAO.fetchById(1L);
        assertTags(tag, actual);
    }

    @Test
    @DatabaseSetup(PATH_TO_TAGS)
    public void testFetchById() throws Exception {

        Tag actual = tagDAO.fetchById(1L);

        Assert.assertNotNull(actual);
        assertTags(expected, actual);

    }


    private void assertTags(Tag expected, Tag actual) {

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getTagName(), actual.getTagName());

    }

}
