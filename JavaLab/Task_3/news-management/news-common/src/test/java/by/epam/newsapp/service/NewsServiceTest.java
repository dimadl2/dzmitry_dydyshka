package by.epam.newsapp.service;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.dto.News;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.impl.NewsServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:/spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

    @Mock
    private NewsDAO newsDAO;

    @InjectMocks
    private NewsServiceImpl service;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() throws Exception {

    }


    @Test
    public void testUpdate() throws Exception {

        Long id = new Long(1);

        News news = initNews();

        News newsNews = new News();
        newsNews.setId(id);
        newsNews.setTitle("test1_title");
        newsNews.setFullText("test1_full");
        newsNews.setShortText("test1_short");
        newsNews.setCreationDate(new Date());
        newsNews.setModificationDate(new Timestamp(new Date().getTime()));

        when(newsDAO.fetchById(id)).thenReturn(news);
        boolean update = service.update(newsNews);
        Assert.assertTrue(update);
        verify(newsDAO).fetchById(id);
        ArgumentCaptor<News> newsCaptor = ArgumentCaptor.forClass(News.class);
        verify(newsDAO).update(newsCaptor.capture());
        News updatedNews = newsCaptor.getValue();
        Assert.assertEquals("test1_title", updatedNews.getTitle());
        Assert.assertEquals("test1_full", updatedNews.getFullText());
        Assert.assertEquals("test1_short", updatedNews.getShortText());

    }

    @Test
    public void testUpdateIfNewsNotFound() throws Exception {

        Long id = 1L;
        when(newsDAO.fetchById(id)).thenReturn(null);

        News newsNews = new News();
        newsNews.setId(id);
        newsNews.setTitle("test1_title");
        newsNews.setFullText("test1_full");
        newsNews.setShortText("test1_short");
        newsNews.setCreationDate(new Date());
        newsNews.setModificationDate(new Timestamp(new Date().getTime()));

        boolean update = service.update(newsNews);
        Assert.assertFalse(update);
        verify(newsDAO).fetchById(id);

    }

    @Test(expected = TechnicalException.class)
    public void testUpdateThrowException() throws Exception {

        Long id = 1L;

        News news = initNews();

        when(newsDAO.fetchById(id)).thenReturn(news);
        doThrow(DAOException.class).when(newsDAO).update(news);
        service.update(news);
        verify(newsDAO).fetchById(id);
        verify(newsDAO).update(news);

    }

    @Test
    public void testDelete() throws Exception {

        Long id = 1L;
        News news = initNews();
        news.setModificationDate(new Timestamp(new Date().getTime()));
        when(newsDAO.fetchById(id)).thenReturn(news);
        boolean delete = service.delete(id);
        Assert.assertTrue(delete);
        verify(newsDAO).fetchById(id);
        verify(newsDAO).delete(id);
    }

    @Test
    public void testDeleteIfNewsNotFound() throws Exception {

        Long id = 1L;
        when(newsDAO.fetchById(id)).thenReturn(null);
        boolean delete = service.delete(id);
        Assert.assertFalse(delete);
        verify(newsDAO).fetchById(id);

    }

    @Test(expected = TechnicalException.class)
    public void testDeleteThrowsException() throws Exception {

        Long id = 1L;
        News news = initNews();
        when(newsDAO.fetchById(id)).thenReturn(news);
        doThrow(DAOException.class).when(newsDAO).delete(id);
        service.delete(id);
        verify(newsDAO).fetchById(id);
        verify(newsDAO).delete(id);

    }

    @Test
    public void testList() throws Exception {

        List<News> newsList = new LinkedList<News>();

        News news = initNews();

        newsList.add(news);

        when(newsDAO.list()).thenReturn(newsList);

        List<News> resultNews = service.list();

        Assert.assertNotNull(resultNews);
        Assert.assertEquals(1, resultNews.size());

        verify(newsDAO).list();

    }

    @SuppressWarnings("unchecked")
    @Test(expected = TechnicalException.class)
    public void testListThrowsException() throws Exception {

        when(newsDAO.list()).thenThrow(DAOException.class);
        service.list();

    }

    @Test
    public void testFetchLimitNews() throws Exception {

        News news1 = initNews();
        News news2 = initNews();

        List<News> news = Arrays.asList(news1, news2);

        when(newsDAO.fetchLimitNews(1, 2)).thenReturn(news);

        List<News> newsActual = service.fetchLimitNews(1, 2);
        Assert.assertEquals(news.size(), newsActual.size());
        verify(newsDAO).fetchLimitNews(1, 2);

    }

    @Test
    public void testGetCountRows() throws Exception {

        when(newsDAO.getCountRows()).thenReturn(3);

        int actual = service.getCountRows();
        Assert.assertEquals(3, actual);
        verify(newsDAO).getCountRows();

    }

    private News initNews() {

        News news = new News();
        news.setId(1L);
        news.setTitle("test_title");
        news.setFullText("test_full");
        news.setShortText("test_short");
        news.setCreationDate(new Date());
        news.setModificationDate(new Timestamp(new Date().getTime()));

        return news;
    }

}
