 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="body">
	<div style="margin-left: 63px; margin-top: 50;">
		<table class="table-body" style="border-spacing: 30px;">
			<c:forEach var="tag" items="${tags}">

					<tr>
						<td align="right" valign="top"> <b>Tag:</b></td>
						<td valign="top">
							<form name="${tag.tagName}" action="/news-admin/tags/update"  method="POST">
								<input type="hidden" name="tag-id" value="${tag.id}"/>
								<input type="text" class="input-l" id="${tag.tagName}" name="name" value="${tag.tagName}" disabled />
							</form>
						</td>
						<td valign="top"> <span name="${tag.tagName}" class="edit">edit</span><span id="${tag.tagName}" class="delete" style="display: none; margin-left: 15px;" onClick="document.forms.delete${tag.tagName}.submit()" >delete</span></td>
					</tr>

				<form name="delete${tag.tagName}" action="/news-admin/tags/delete" method="POST">
					<input type="hidden" name="tag-id" value="${tag.id}"/>
				</form>
			</c:forEach>

				<tr>
					<td align="right" valign="top">
						 <b>Add Tag:</b>
					</td>
					<td valign="top">
						<form action="/news-admin/tags/add" method="POST" name="addtag" id="addtag">
							<input type="text" class="input-l" name="new-tag" id="new-tag"/>
						</form>
					</td>
					<td valign="top">
						<span class="save" onClick="document.forms.addtag.submit()">save</span>
					</td>
				</tr>

		</table>
	</div>
</div>