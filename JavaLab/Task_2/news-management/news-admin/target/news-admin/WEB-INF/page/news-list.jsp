<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">

					<form action="/news-admin/news/delete" method="POST">
							<table width="660px" cellspacing=0 class="table-body">
								<tr>
									<td>
										<div class="filter">

										<form action="/news-admin/news/filter" method="POST">
                                            <div class="multiselect" class="row">
                                                <div class="selectBox">
                                                    <select>
                                                        <option>Select an option</option>
                                                    </select>
                                                    <div class="overSelect"></div>
                                                </div>
                                                <div id="checkboxes" align="left">
                                     			   <c:forEach var="tag" items="${tags}">
                                        					<label for="${tag.tagName}"><input type="checkbox" name="${tag.id}"

                                        						<c:if test="${tagsId.contains(tag.id)}">
                                        							checked
                                        						</c:if>

                                        					/>${tag.tagName}</label>
                                        			</c:forEach>

                                                </div>
                                            </div>
											<div class="row">
                                       			<select name="author">

													<c:forEach var="author" items="${authors}">
														<option value="${author.id}"

															<c:if test="${author.id eq authorId}">
																selected
															</c:if>

														>${author.name}</option>
													</c:forEach>

												</select>

												<input type="submit" value="Filter">
											</div>
											</form>

										</div>
									</td>
								</tr>
								<c:forEach var="item" items="${news}">
								<tr>
									<td>
										<div class="item-content">
											<table width="620px" cellspacing=0>
												<tr>
													<td  width="410px">
														<a href="/news-admin/news/${item.newsTO.id}"><b>${item.newsTO.title}</b></a> (by

															<c:forEach var="author" items="${item.authors}">
																${author.name}
															</c:forEach>

														)
													</td>
													<td align="right">
														<u><fmt:formatDate type="date"
                                                                       value="${item.newsTO.creationDate}" /></u>
													</td>
												</tr>
												<tr>
													<td colspan=2>
														<div style="margin-top: 20px; width: 420px">
															${item.newsTO.shortText}
														</div>
													</td>
												</tr>
												<tr>
														<td colspan=2 align="right">
															<div>
																<div style="color: gray; width: 394px;" class="row" align="right">
																	<c:forEach var="tag" items="${item.tags}">
																		${tag.tagName},
																	</c:forEach>
																</div>
																<div class="row" style="color: red;width: 100px; "><spring:message code="label.comments"/>(${item.countComments})</div>
																<div class="row" align="right"><a href="/news-admin/news/edit/${item.newsTO.id}"><spring:message code="label.edit"/></a></div>
																<div class="row"><input type="checkbox" name="newsIds" value="${item.newsTO.id}"></div>

															</div>
														</td>

												</tr>

											</table>
										</div>
									</td>
								</tr>
								</c:forEach>
							</table>
							<div style="padding-left: 590px">
								<input type="submit" value="Delete">
							</div>

					</form>

							<div style="bottom:0; position: absolute; background-color: rgb(242, 242, 242); width: 652px; margin: 5px">
								<div style="text-align: center; margin: 20px">

									<c:forEach var="i" begin="1" end="${numberOfPage}">
									<div class="row">
                                       <form action="/news-admin/news" method="GET">
                                       		<input type="hidden" name="page" value="${i}"/>
                                       		<input type="submit" value="${i}"/>
                                       </form>
                                    </div>
                                    </c:forEach>
								</div>
							</div>
						</div>