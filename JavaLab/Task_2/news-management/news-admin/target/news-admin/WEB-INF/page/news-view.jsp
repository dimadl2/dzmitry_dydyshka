<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="body">
							<div style="margin-left: 10px; margin-top: 5px"><a href="/news-admin/news?page=${pageNum}">BACK</a></div>
							<div style="padding-left: 40px; padding-top: 10px">
								<table class="table-body" style="width: 100%">
									<tr>
										<td width="350px">${news.newsTO.title} </td>
										<td valign="top" width="80px">(by
										    	<c:forEach var="author" items="${news.authors}">
                                            			${author.name}
                                            	</c:forEach>
										)</td>
										<td valign="top">${news.newsTO.creationDate}</td>
									</tr>
									<tr >
										<td colspan=2>
											<div style="padding-top: 20px">
										        ${news.newsTO.fullText}
											</div>
										</td>
									</tr>
									<c:forEach var="comment" items="${news.comments}">
									<tr>
										<td>
											<div class="comment" style="margin-top: 20px">
												<div style="margin-left: 8px">${comment.creationDate}</div>
												<div style="background-color: rgb(210, 210, 201); width: 400px; margin-left: 10px; position: relative;">
												    <div style="position:absolute; width: 20px; height: 20px; top:0px; right:0px; padding:0px">
												      <form action="/news-admin/news/delcomment" method="POST">
                                                            <input type="hidden" name="newsId" value="${news.newsTO.id}">
                                                            <input type="hidden" name="commentId" value="${comment.id}">
                                                            <input type="submit" value="X">
												      </form>
												    </div>
												    ${comment.commentText}

												</div>
											</div>
										</td>
									</tr>
									</c:forEach>

									<tr>
										<td>
											<form action="/news-client/news/${news.newsTO.id}" method="POST">
												<input type="hidden" name="action" value="addComment"/>
 												<textarea name="commentText" cols="54" rows="4" style="margin-left: 10px; margin-top: 15px"> </textarea><br>
												<div style="width: 400px; text-align: right; margin-left: 10px; margin-top: 10px" ><input type="submit" value="Post comment"></div>
											</form>
										</td>
									</tr>
								</table>
							</div>
								<table class="table-body" width="640px" style="margin-left: 10px; margin-right: 10px; margin-top: 20px">
								<tr>
									<td>
										<c:if test="${previousId != 0}" >
											<a href="/news-admin/news/${previousId}">PREVIOUS</a>
										</c:if>
									</td>
									<td align="right">
										<c:if test="${nextId != 0}" >
											<a href="/news-admin/news/${nextId}">NEXT</a>
										</c:if>
									</td>
								</tr>
							</table>
						</div>