 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="body">
	<div style="margin-left: 40px; margin-top: 50;">
	    	<table class="table-body" style="border-spacing: 30px;">
	    		<c:forEach var="author" items="${authors}">
					<tr>
						<td align="right" valign="top"> <b>Author:</b></td>
						<td>
							<form name="update${author.id}" action="/news-admin/authors/update"  method="POST">
								<input type="hidden" name="author-id" value="${author.id}"/>
								<input type="text" id="update${author.id}"  class="input-l" name="name" value="${author.name}" disabled/>
							  </form>
						</td>
						<td valign="top"> <span name="update${author.id}" class="edit">edit</span><span id="update${author.id}" style="display: none; margin-left: 15px;" onClick="document.forms.delete${author.id}.submit()" class="delete" >delete</span></td>
					</tr>
	    		 </c:forEach>
	    		 <tr>
	    		 	<td align="right" valign="top">
	    		 		 <b>Add Author:</b>
	    		 	</td>
	    		 	<td>
	    		 		<form action="/news-admin/authors/add" method="POST" name="addauthor">
	    		 			<input class="input-l" type="text" name="new-author"/>
	    		 		</form>
	    		 	</td>
	    		 	<td valign="top">
	    		 		<div class="save" onClick="document.forms.addauthor.submit()">save</div>
	    		 	</td>
	    		 </tr>
	    	</table>

        <form name="delete${author.id}" action="/news-admin/authors/delete" method="POST">
        	<input type="hidden" name="author-id" value="${author.id}"/>
        </form>
</div>