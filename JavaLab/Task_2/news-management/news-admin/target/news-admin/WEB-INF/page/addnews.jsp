<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">
	${news.newsTO.creationDate.toString()}

<div style="width: 516px; margin-left: 75px; margin-top: 50px;">

<form action="<c:choose>
 				<c:when test="${empty news}">
        			/news-admin/addnews
    			</c:when>
    			<c:otherwise>
       				/news-admin/news/edit/${news.newsTO.id}
    			</c:otherwise>
			</c:choose>" 
	  method="POST">
	<input type="hidden" name="create-date" value="${news.newsTO.creationDate.toString()}">
				<table class="table-body">
					<tr>
						<td>
							<b><spring:message code="label.news.title"/>:</b>
						</td>
						<td>
							<input type="text" name="title" id="title" value="${news.newsTO.title}">
						</td>
					</tr>
					<tr>
						<td>
							<b><spring:message code="label.news.date"/>:</b>
						</td>
						<td>
							<input type="text" name="date" value="${date}" value="${news.newsTO.creationDate}">
						</td>
					</tr>
					<tr>
						<td>
							<b><spring:message code="label.news.brief"/>:</b>
						</td>
						<td>
							<textarea name="short-text" rows="4" cols="61">
									${news.newsTO.shortText}
							</textarea>
						</td>
					</tr>
					<tr>
						<td>
							<b><spring:message code="label.news.content"/>:</b>
						</td>
						<td>
							<textarea name="full-text" rows="10" cols="61">
								${news.newsTO.fullText}
							</textarea>
						</td>
					</tr>
					<tr>
						<td colspan=2>
							 <div class="multiselect" class="row">
                                                <div class="selectBox">
                                                    <select>
                                                        <option><spring:message code="label.select.tags"/></option>
                                                    </select>
                                                    <div class="overSelect"></div>
                                                </div>
                                                <div id="checkboxes" align="left">
                                     			   <c:forEach var="tag" items="${tags}">
                                        					<label for="${tag.tagName}"><input type="checkbox" name="tags" value="${tag.id}"

                                        						<c:forEach var="tagVariant" items="${news.tags}">
	                                        						<c:if test="${tagVariant.id eq tag.id}">
	                                        							checked
	                                        						</c:if>
                                        						</c:forEach>

                                        					/>${tag.tagName}</label>
                                        	</c:forEach>

                                         </div>
                                </div>
                                <div class="row">
                                    <select name="author">

                                    	<option disabled selected><b><spring:message code="label.select.author"/></b></option>

										<c:forEach var="author" items="${authors}">
											<option value="${author.id}"

															<c:forEach var="authorVariant" items="${news.authors}">
																<c:if test="${authorVariant.id eq author.id}">
																	selected
																</c:if>
															</c:forEach>
														>${author.name}</option>
										</c:forEach>

									</select>
								</div>
						
						</td>
					</tr>
					<tr>
						<td colspan=2 align="right"><input type="submit" value="Save"></td>
					</tr>
				</table>
			</form>
		</div>
</div>
