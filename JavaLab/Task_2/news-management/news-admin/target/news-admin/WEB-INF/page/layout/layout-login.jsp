<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>News List</title>
		 <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>">
		<script src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
		  <script src="<c:url value='/js/common.js'/>"></script>
		 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
		<div id="container" align="center">

			<table width="900px" cellspacing=0 class="table-main">

				<tr >
					<td>
                        <tiles:insertAttribute name="header" />
					</td>
				</tr>
				<tr>
					<td>
                        <tiles:insertAttribute name="body" />
					</td>
				</tr>
				<tr>

					<td style="border-bottom: 2px solid black">
                        <tiles:insertAttribute name="footer" />
					</td>

				</tr>

			</table>

		</div>


	</body>
</html>