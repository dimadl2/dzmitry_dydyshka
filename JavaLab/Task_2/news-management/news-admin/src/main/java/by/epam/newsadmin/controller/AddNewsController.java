package by.epam.newsadmin.controller;

import by.epam.newsadmin.util.DateUtils;
import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.dto.NewsVO;
import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsManage;
import by.epam.newsapp.service.TagService;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.enterprise.inject.Model;
import javax.json.Json;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/6/2015.
 */
@Controller
public class AddNewsController {

    private static final String REDIRECT = "redirect:";
    private static final String URL_ADD_NEWS = "/addnews";
    private static final String URL_NEWS = "/news";
    private static final String ATTR_TAGS = "tags";
    private static final String ATTR_AUTHORS = "authors";
    private static final String VIEW_NEWS_ADD = "news-add";
    public static final String ATTR_PLACEHOLDER = "placeholder";


    private static final Logger LOG = Logger.getLogger(AddNewsController.class);

    @Autowired
    private NewsManage newsManage;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;


    @RequestMapping(value = URL_ADD_NEWS, method = RequestMethod.GET)
    public ModelAndView loadPage(HttpServletRequest request) throws Exception {

        List<TagTO> tags = tagService.list();
        List<AuthorTO> authors = authorService.getAllNotExpire();

        ModelAndView modelAndView = new ModelAndView();

        String locale = String.valueOf(request.getAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME));

        modelAndView.addObject(ATTR_PLACEHOLDER, DateUtils.getStringFormat(locale));
        modelAndView.addObject(ATTR_TAGS, tags);
        modelAndView.addObject(ATTR_AUTHORS, authors);
        modelAndView.setViewName(VIEW_NEWS_ADD);

        return modelAndView;

    }

    @RequestMapping(value = URL_ADD_NEWS, method = RequestMethod.POST)
    public ModelAndView addNews(@RequestParam String title,
                                @RequestParam String date,
                                @RequestParam("short-text") String shortText,
                                @RequestParam("full-text") String fullText,
                                @RequestParam Long author,
                                @RequestParam(required = false) List<Long> tags,
                                HttpServletRequest request) throws Exception {

        NewsTO newsTO = new NewsTO();
        newsTO.setTitle(title);
        newsTO.setShortText(shortText);
        newsTO.setFullText(fullText);

        String locale = String.valueOf(request.getAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME));

        SimpleDateFormat format = DateUtils.getFormat(locale);

        Date dateCreate = format.parse(date);
        newsTO.setCreationDate(dateCreate);
        newsTO.setModificationDate(dateCreate);

        NewsVO newsVO = new NewsVO();
        newsVO.setNewsTO(newsTO);

        AuthorTO authorTO = new AuthorTO();
        authorTO.setId(author);
        List<AuthorTO> authors = Arrays.asList(authorTO);

        List<TagTO> tagsTO = new LinkedList<>();

        if (tags != null) {
            for (Long idTag : tags) {
                TagTO tag = new TagTO();
                tag.setId(idTag);
                tagsTO.add(tag);
            }
        }

        newsVO.setAuthors(authors);
        newsVO.setTags(tagsTO);

        Long id = newsManage.save(newsVO);


        return new ModelAndView(REDIRECT + URL_NEWS + "/" + id);

    }

}
