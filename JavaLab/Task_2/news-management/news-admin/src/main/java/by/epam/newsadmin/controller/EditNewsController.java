package by.epam.newsadmin.controller;

import by.epam.newsadmin.util.DateUtils;
import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.dto.NewsVO;
import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsManage;
import by.epam.newsapp.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/7/2015.
 */
@Controller
public class EditNewsController {

    private static final String URL_NEWS_EDIT = "/news/edit/{newsId}";
    private static final String URL_NEWS = "/news";
    private static final String ATTR_TAGS = "tags";
    private static final String ATTR_AUTHORS = "authors";
    private static final String ATTR_NEWS = "news";
    private static final String ATTR_DATE = "date";
    private static final String VIEW_NEWS_ADD = "news-add";
    private static final String REDIRECT = "redirect:";
    public static final String ATTR_PLACEHOLDER = "placeholder";


    @Autowired
    private NewsManage newsManage;


    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = URL_NEWS_EDIT, method = RequestMethod.GET)
    public ModelAndView loadPage(@PathVariable Long newsId, HttpServletRequest request) throws Exception {


        String locale = String.valueOf(request.getAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME));

        SimpleDateFormat format = DateUtils.getFormat(locale);

        String creationDate = format.format(new Date());

        ModelAndView model = new ModelAndView();

        NewsVO newsVO = newsManage.fetchById(newsId);
        List<TagTO> tags = tagService.list();
        List<AuthorTO> authors = authorService.fetchExpireAndNewsAuthor(newsId);

        model.addObject(ATTR_TAGS, tags);
        model.addObject(ATTR_AUTHORS, authors);
        model.addObject(ATTR_NEWS, newsVO);
        model.addObject(ATTR_PLACEHOLDER, DateUtils.getStringFormat(locale));
        model.addObject(ATTR_DATE, creationDate);
        model.setViewName(VIEW_NEWS_ADD);

        return model;
    }


    @RequestMapping(value = URL_NEWS_EDIT, method = RequestMethod.POST)
    public ModelAndView addNews(@PathVariable Long newsId,
                                @RequestParam String title,
                                @RequestParam String date,
                                @RequestParam("short-text") String shortText,
                                @RequestParam("full-text") String fullText,
                                @RequestParam Long author,
                                @RequestParam(required = false) List<Long> tags,
                                HttpServletRequest request) throws Exception {

        NewsTO newsTO = new NewsTO();
        newsTO.setId(newsId);
        newsTO.setTitle(title);
        newsTO.setShortText(shortText);
        newsTO.setFullText(fullText);


        String locale = String.valueOf(request.getAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME));

        SimpleDateFormat format = DateUtils.getFormat(locale);

        Date dateCreate = format.parse(date);
        newsTO.setCreationDate(dateCreate);
        newsTO.setModificationDate(dateCreate);

        NewsVO newsVO = new NewsVO();
        newsVO.setNewsTO(newsTO);

        AuthorTO authorTO = new AuthorTO();
        authorTO.setId(author);
        List<AuthorTO> authors = Arrays.asList(authorTO);

        List<TagTO> tagsTO = new LinkedList<>();

        if (tags != null) {
            for (Long idTag : tags) {
                TagTO tag = new TagTO();
                tag.setId(idTag);
                tagsTO.add(tag);
            }
        }

        newsVO.setAuthors(authors);
        newsVO.setTags(tagsTO);

        newsManage.update(newsVO);


        return new ModelAndView(REDIRECT + URL_NEWS + "/" + newsId);

    }
}
