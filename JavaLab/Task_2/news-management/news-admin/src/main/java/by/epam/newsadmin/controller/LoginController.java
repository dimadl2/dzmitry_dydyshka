package by.epam.newsadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Dzmitry_Dydyshka on 4/15/2015.
 */

@Controller
public class LoginController {

    public static final String URL_LOGIN = "/login";
    public static final String ATTR_ERROR = "error";
    public static final String VIEW_LOGIN = "login";

    @RequestMapping(value = URL_LOGIN, method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject(ATTR_ERROR, "Invalid username and password!");
        }

        model.setViewName(VIEW_LOGIN);

        return model;

    }

}


