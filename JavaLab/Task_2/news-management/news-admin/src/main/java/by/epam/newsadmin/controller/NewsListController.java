package by.epam.newsadmin.controller;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.dto.NewsVO;
import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.AuthorService;
import by.epam.newsapp.service.NewsManage;
import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.service.TagService;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.util.Page;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import sun.security.krb5.internal.crypto.dk.AesDkCrypto;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/3/2015.
 */
@Controller
@SessionAttributes(value = {"pageNum", "tagsId", "authorId", "action"})
public class NewsListController {

    private static final String ATTR_ERROR_FILTER = "errorFilter";
    private static final String ATTR_TAGS_ID = "tagsId";
    private static final String ATTR_AUTHOR_ID = "authorId";
    private static final String ATTR_PAGE_NUMBER = "pageNum";
    private static final String ATTR_NEWS = "news";
    private static final String ATTR_TAGS = "tags";
    private static final String ATTR_AUTHORS = "authors";
    private static final String ATTR_NUMBER_OF_PAGE = "numberOfPage";
    private static final String VIEW_NEWS_LIST = "news-list";
    private static final String URL_NEWS = "/news";
    private static final String ACTION_FILTER = "filter";
    private static final String URL_NEWS_DELETE = "/news/delete";
    private static final String REDIRECT = "redirect:";
    public static final int NUMBER_AT_PAGE = 3;
    private static final String ATTR_ACTION = "action" ;
    private static final String URL_NEWS_FILTER = "/news/filter";

    @Autowired
    private NewsManage newsManage;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    private final static Logger LOG = Logger.getLogger(NewsListController.class);

    @RequestMapping(value = URL_NEWS, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView newsList(@RequestParam(defaultValue = "1") int page) throws TechnicalException {


        ModelAndView modelAndView = new ModelAndView();

        Page pageData = newsManage.getPage(page, NUMBER_AT_PAGE);

        List<NewsVO> news = pageData.getItemsForPage();
        int numberOfPage = pageData.getNumberOfPage();

        List<TagTO> tags = tagService.list();
        List<AuthorTO> authors = authorService.list();

        modelAndView.addObject(ATTR_PAGE_NUMBER, page);
        modelAndView.addObject(ATTR_NEWS, news);
        modelAndView.addObject(ATTR_TAGS, tags);
        modelAndView.addObject(ATTR_AUTHORS, authors);
        modelAndView.addObject(ATTR_TAGS_ID, Collections.emptyList());
        modelAndView.addObject(ATTR_AUTHOR_ID, 0L);
        modelAndView.addObject(ATTR_ACTION, "");
        modelAndView.addObject(ATTR_NUMBER_OF_PAGE, numberOfPage);

        modelAndView.setViewName(VIEW_NEWS_LIST);

        return modelAndView;
    }


    @RequestMapping(value = URL_NEWS_FILTER, method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView filter(@RequestParam(defaultValue = "1") int page,
                               @RequestParam(required = false, defaultValue = "") List<Long> tagsId,
                               @RequestParam(required = false, defaultValue = "0") Long author,
                               HttpSession session) throws TechnicalException {

        ModelAndView modelAndView = new ModelAndView();

        if (tagsId.isEmpty() && author == 0L){

            tagsId = (List) session.getAttribute(ATTR_TAGS_ID);
            author = (Long) session.getAttribute(ATTR_AUTHOR_ID);


        }

        Filter filter = new Filter(author, tagsId);
        Page pageData = newsManage.getFilteredPage(page, NUMBER_AT_PAGE, filter);

        List<NewsVO> news = pageData.getItemsForPage();
        int numberOfPage = pageData.getNumberOfPage();

        List<TagTO> tags = tagService.list();
        List<AuthorTO> authors = authorService.list();

        modelAndView.addObject(ATTR_PAGE_NUMBER, page);
        modelAndView.addObject(ATTR_NEWS, news);
        modelAndView.addObject(ATTR_TAGS, tags);
        modelAndView.addObject(ATTR_AUTHORS, authors);
        modelAndView.addObject(ATTR_TAGS_ID, tagsId);
        modelAndView.addObject(ATTR_AUTHOR_ID, author);
        modelAndView.addObject(ATTR_NUMBER_OF_PAGE, numberOfPage);
        modelAndView.addObject(ATTR_ACTION, ACTION_FILTER);

        modelAndView.setViewName(VIEW_NEWS_LIST);

        return modelAndView;

    }


    @RequestMapping(URL_NEWS_DELETE)
    public ModelAndView deleteNews(@RequestParam List<Long> newsIds) throws Exception {

        newsService.delete(newsIds);

        return new ModelAndView(REDIRECT + URL_NEWS);

    }

}
