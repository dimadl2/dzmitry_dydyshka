<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<table width="100%" class="table-body">
	<tr>
		<td align="left">
			<div class="header"><spring:message code="label.title"/></div>
		</td>
		<td align="right">
			<sec:authorize access="isAuthenticated()">
				<form action="<c:url value='/logout'/>" method="post" id="logoutForm">
						<b> <spring:message code="label.hello"/>, <sec:authentication property="principal.username" /> </b><input type="submit" value="<spring:message code="label.logout"/>">
                </form>
            </sec:authorize>

		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div class="location">
            	<a href="?page=${pageNum}&lang=en&action=${action}"><spring:message code="label.lang.en"/></a>
            	<a href="?page=${pageNum}&lang=ru&action=${action}"><spring:message code="label.lang.ru"/></a>
            </div>
		</td>
	</tr>
</table>


