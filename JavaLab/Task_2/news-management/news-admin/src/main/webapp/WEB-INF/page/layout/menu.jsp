<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="menu">

    <div class="menu-content">

        <ul>
            <li><a href="/news-admin/news"><spring:message code="label.menu.news.list"/></a></li>
            <li><a href="/news-admin/addnews"><spring:message code="label.menu.news.add"/></a></li>
             <li><a href="/news-admin/authors"><spring:message code="label.menu.authors.add"/></a></li>
             <li><a href="/news-admin/tags"><spring:message code="label.menu.tags.add"/></a></li>
        </ul>

    </div>

</div>
