 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">
	<div style="margin-left: 63px; margin-top: 50;">
		<table class="table-body" style="border-spacing: 10px;">
			<c:forEach var="tag" items="${tags}">

					<tr>
						<td align="right" valign="top"> <b><spring:message code="label.tag"/>:</b></td>
						<td valign="top" colspan=2 align="right">
							<form action="/news-admin/tags/update"  method="POST" id="form${tag.id}" onsubmit="return verifyUpdateForm(${tag.id});">
								<input type="hidden" name="tag-id" value="${tag.id}"/>
								<input type="text" class="input-l" id="field${tag.id}" name="name" value="${tag.tagName}" disabled />
								<input type="submit" id="update${tag.id}" class="submit" value="<spring:message code='label.update'/>" style="display: none;"/>
								<input type="button" onclick="changeState('${tag.id}', this)" class="submit" value="<spring:message code='label.edit'/>"/>
							</form>
						</td>
						<td valign="top">
							<form action="/news-admin/tags/delete" method="POST">
								<input type="hidden" name="tag-id" value="${tag.id}"/>
								<input type="submit" class="submit" id="delete${tag.id}" value="<spring:message code='label.delete'/>" style="display: none;">
							</form>
						</td>
					</tr>

				
			</c:forEach>

				<tr>
					<td>
					</td>
					<td id="tagError" class="error" align="center">
						<spring:message code="message.tag.empty"/>
					</td>

				</tr>

				<tr>
					<td align="right" valign="top">
						 <b><spring:message code="label.tag.add"/>:</b>
					</td>
					<td valign="top" align="right" colspan="2">
						<form action="/news-admin/tags/add" method="POST" name="addtag" id="addtag">
							<input type="text" class="input-l" name="new-tag" id="new-tag"/>
							<input type="submit" class="submit" value="<spring:message code="label.save"/>"/>
						</form>
					</td>
				</tr>

		</table>
	</div>
</div>