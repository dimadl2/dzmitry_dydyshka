 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">
	<div style="margin-left: 63px; margin-top: 50;">
		<table class="table-body" style="border-spacing: 10px;">
			<c:forEach var="author" items="${authors}">

					<tr>
						<td align="right" valign="top"> <b><spring:message code="label.author"/>:</b></td>
						<td valign="top" colspan=2 align="right">
							<form action="/news-admin/authors/update"  method="POST" onsubmit="return verifyUpdateForm(${author.id});">
								<input type="hidden" name="author-id" value="${author.id}"/>
								<input type="text" class="input-l" id="field${author.id}" name="name" value="${author.name}" disabled />
								<input type="submit" id="update${author.id}" class="submit" value="<spring:message code='label.update'/>" style="display: none;"/>
								<input type="button" onclick="changeState('${author.id}', this)" class="submit" value="<spring:message code='label.edit'/>"/>
							</form>
						</td>
						<td valign="top">
							<form class="expire" action="/news-admin/authors/delete" method="POST">
								<input type="hidden" name="author-id" value="${author.id}"/>
								<input type="submit" class="submit" id="delete${author.id}" value="<spring:message code='label.expire'/>" style="display: none;">
							</form>
						</td>
					</tr>

				
			</c:forEach>

				<tr>
					<td>
					</td>
					<td id="authorError" class="error" align="center">
						<spring:message code="message.author.empty"/>
					</td>

				</tr>

				<tr>
					<td align="right" valign="top">
						 <b><spring:message code="label.author.add"/>:</b>
					<td valign="top" align="right" colspan="2">
						<form action="/news-admin/authors/add" method="POST"  id="addauthor">
							<input type="text" class="input-l" name="new-author" id="new-author"/>
							<input type="submit" class="submit" value="<spring:message code="label.save"/>"/>
						</form>
					</td>
				</tr>

		</table>
	</div>
</div>