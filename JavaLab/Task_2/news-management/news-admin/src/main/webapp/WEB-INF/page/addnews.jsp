<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="body">
	<div style="width: 516px; margin-left: 20px; margin-top: 50px;">
		<form id="add-news" action="
					<c:choose>
		 				<c:when test="${empty news}">
		        			/news-admin/addnews
		    			</c:when>
		    			<c:otherwise>
		       				/news-admin/news/edit/${news.newsTO.id}
		    			</c:otherwise>
					</c:choose>" 
	  			method="POST">
			<table class="table-body">
				<tr>
					<td></td>
					<td id="titleError" class="error">
						<spring:message code="message.title.empty"/>
					</td>
					</tr>
				<tr>
					<td align="right">
						<b><spring:message code="label.news.title"/>:</b>
					</td>
					<td>
						<input type="text" name="title" id="title" value="${news.newsTO.title}">
					</td>
				</tr>
				<tr>
					<td align="right">
						<b><spring:message code="label.news.date"/>:</b>
					</td>
					<td>
						<input type="text" name="date" id="date" value="${date}" placeholder="${placeholder}">
					</td>
				</tr>
				<tr>
					<td></td>
                    <td id="briefError" class="error">
                        <spring:message code="message.brief.empty"/>
                    </td>
                </tr>
				<tr align="right">
					<td align="right">
						<b><spring:message code="label.news.brief"/>:</b>
					</td>
					<td align="left">
						<textarea name="short-text" id="brief" rows="4" cols="61">${news.newsTO.shortText}</textarea>
					</td>
				</tr>
				<tr>
				    <td></td>
                    <td id="contentError" class="error">
                        <spring:message code="message.content.empty"/>
                    </td>
                </tr>
				<tr>
					<td align="right">
						<b><spring:message code="label.news.content"/>:</b>
					</td>
					<td align="left">
						<textarea name="full-text" id="content" rows="10" cols="61">${news.newsTO.fullText}</textarea>
					</td>
				</tr>
				<tr>
                    <td></td>
                    <td id="authorError" class="error">
                        <spring:message code="message.author.choose"/>
                    </td>
                </tr>

				<tr>
					<td>
					</td>
					<td>
						<div class="filter-tool">
							<div class="multiselect" class="row">
                                <div class="selectBox">
                                    <select>
                                        <option id="sel-title"><spring:message code="label.select.tags"/></option>
                                    </select>
                                    <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes" align="left">
                                <c:forEach var="tag" items="${tags}">
                                    <label for="${tag.tagName}">
                                    	<input class="check" type="checkbox" name="tags" value="${tag.id}"
                                        			<c:forEach var="tagVariant" items="${news.tags}">
	                                        				<c:if test="${tagVariant.id eq tag.id}">
	                                        					checked
	                                        				</c:if>
                                        			</c:forEach>

                                        />
                                        ${tag.tagName}
                                    </label>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="row">
                            <select id="author" name="author">
                                <option disabled selected><b><spring:message code="label.select.author"/></b></option>
									<c:forEach var="author" items="${authors}">
										<option value="${author.id}"
											<c:forEach var="authorVariant" items="${news.authors}">
												<c:if test="${authorVariant.id eq author.id}">
														selected
												</c:if>
											</c:forEach>
										>${author.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan=2 align="right"><input type="submit" value="<spring:message code="label.save"/>"></td>
				</tr>
			</table>
		</form>
	</div>
</div>
