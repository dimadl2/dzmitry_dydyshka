<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">

	<table width="660px" cellspacing=0 class="table-body">
		<c:if test="${not empty errorFilter}">
            <tr>
                <td colspan=2 align="center">
                    <p style="color:red; padding-top: 20px;">Please, choose the author or tags!
                </td>
            </tr>
        </c:if>
        <tr>
            <td colspan=2 align="center" id="filterError" style="display: none">
                <p style="color:red; padding-top: 20px;"><spring:message code="meesage.filter.empty"/>
            </td>
        </tr>
		<tr>
			<td>
				<div class="filter">
                     <form action="/news-admin/news/filter" method="POST" id="filter">
                        <input type="hidden" name="action" value="filter">
                 		<div class="multiselect" class="row">
                     		<div class="selectBox">
                           		<select>
                                	<option id="sel-title"><spring:message code="label.select.tags"/></option>
                            	</select>
                             	<div class="overSelect"></div>
                       		</div>
							<div id="checkboxes" align="left"> 
                            	<c:forEach var="tag" items="${tags}">
									<label for="${tag.tagName}">
                                		<input class="check" type="checkbox" name="tagsId" value="${tag.id}"
            							
                                   				<c:if test="${tagsId.contains(tag.id)}">
            
                                     				checked
            
            		                         	</c:if>

            
 	                                    	/>${tag.tagName}    
        	                       	</label>
                            	</c:forEach>
                         	</div> 
                     	</div>
            
            			<div class="row">
                       		<select id="author" name="author">
            					<option id="selector" disabled selected><b><spring:message code="label.select.author"/></b></option>
            						<c:forEach var="author" items="${authors}">
            							<option value="${author.id}"
            								<c:if test="${author.id eq authorId}">
            
            									selected
            
            								</c:if>
            							>${author.name}</option>
            						</c:forEach>
            				</select>
                           </div>
                        <div class="row">
                            <input type="submit" value="<spring:message code="label.filter"/>">
                            <input type="button" id="reset" value="<spring:message code="label.reset"/>" style="margin-left: 10px">
                        </div>
                    </form>
                </div>
			</td>
		</tr>
	</table>
	<form id="delete-news" action="/news-admin/news/delete" method="POST">
		<table width="660px" cellspacing=0 class="table-body">
			<c:forEach var="item" items="${news}">
				<tr>
					<td>
						<div class="item-content">
							<table width="620px" cellspacing=0>
								<tr>
									<td  width="410px">
										<a href="/news-admin/news/${item.newsTO.id}"><b>${item.newsTO.title}</b></a> (by

											<c:forEach var="author" items="${item.authors}">
												${author.name}
											</c:forEach>

										)
									</td>
									<td align="right">
										<u>
											<spring:message var="datePattern" code="pattern.date" scope="page"/>
											<fmt:formatDate type="date" pattern="${datePattern}"
                                                       value="${item.newsTO.creationDate}" /></u>
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<div style="margin-top: 20px; width: 420px">
											${item.newsTO.shortText}
										</div>
									</td>
								</tr>
								<tr>
									<td colspan=2 align="right">
										<div>
											<div class="row-tool"><input type="checkbox" name="newsIds" value="${item.newsTO.id}"></div>
											<div class="row-tool" align="right"><a href="/news-admin/news/edit/${item.newsTO.id}"><spring:message code="label.edit"/></a></div>
											<div class="row-tool" style="color: red;width: 100px; "><spring:message code="label.comments"/>(${item.countComments})</div>
											<div style="color: gray;" class="row-tool" align="right">
												<c:forEach var="tag" items="${item.tags}">
														${tag.tagName},
												</c:forEach>
											</div>														
										</div>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</c:forEach>								
		</table>
		<div style="padding-left: 590px">
			<input type="submit" value="Delete">
		</div>
	</form>
	<div style="bottom:0; position: absolute; background-color: rgb(242, 242, 242); width: 652px; margin: 5px">
		<div style="text-align: center; margin: 20px">

			<c:forEach var="i" begin="1" end="${numberOfPage}">
				<div class="row">
                    <form

                    	<c:if test="${not empty action}">
                             action="/news-admin/news/filter" method="GET">
                        </c:if>

                        <c:if test="${empty action}">
                             action="/news-admin/news" method="GET">
                        </c:if>

                        <input type="hidden" name="page" value="${i}"/>
                        <input type="submit" value="${i}"/>
                    </form>
                </div>
            </c:forEach>
		</div>
	</div>
</div>