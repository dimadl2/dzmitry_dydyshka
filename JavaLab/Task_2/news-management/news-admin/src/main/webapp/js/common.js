$(document).ready(function(){


 var expanded = false;

 $('html').click(function() {

     $("#checkboxes").css({"display" : "none"});
      expanded = false;
 });


  $(".selectBox").click(function(event){

        event.stopPropagation();
         if (!expanded) {
                    $("#checkboxes").css({"display" : "block"});
                    expanded = true;
                } else {
                    $("#checkboxes").css({"display" : "none"});
                    expanded = false;
                }
    });

     $("#checkboxes").click(function(event){

         event.stopPropagation();

     });

     $("#addtag").submit(function(){

            var tag = $("#new-tag").val();

            tag = $.trim(tag);

            if (tag === "") {
                $("#tagError").css({"display":"block"});
                return false;
            };

            return true;

     });

     $("#addauthor").submit(function(){

            var tag = $("#new-author").val();

            tag = $.trim(tag);

            if (tag === "") {
                $("#authorError").css({"display":"block"});
                return false;
            };

            return true;

     });


     $("#filter").submit(function(){

                 var tags = $("input:checkbox:checked").size();
                 var author = $("#author").val();

                 if(tags === 0 && author === null){

                         $("#filterError").css({"display":"block"});
                         return false;
                 }


                 return true;

         });

          $("#reset").click(function(){



                    $("input:checkbox").removeAttr('checked');
                    $("option").removeAttr('selected');
                    $("option#selector").attr('selected', "selected");
                    window.location.href = "http://localhost:8085/news-admin/news";


             });

          $("#add-news").submit(function(){

                var title = $("#title").val();
                var brief = $("#brief").val();
                var content = $("#content").val();
                 var author = $("#author").val();

                var flag = 0;

                title = $.trim(title);
                brief = $.trim(brief);
                content = $.trim(content);

                if (title === ""){

                    $("#titleError").css({"display":"block"})
                    flag = 1;

                }else{
                    $("#titleError").css({"display":"none"})
                }

                if(brief === ""){
                    $("#briefError").css({"display":"block"})
                                        flag = 1;
                }else{
                                     $("#briefError").css({"display":"none"})
                                 }


                if(content === ""){
                    $("#contentError").css({"display":"block"})
                    flag = 1;
                }else{
                                     $("#contentError").css({"display":"none"})
                                 }

                if (author === null){
                         $("#authorError").css({"display":"block"})
                                            flag = 1;
                }else{
                                     $("#authorError").css({"display":"none"})
                                 }


                if (flag === 1){

                    return false;

                }

                return true;

          });

            var count = $("input:checkbox:checked").size();
            var message = $("#sel-title").val();
             if (count !== 0){

                                   $("#sel-title").html("-- Selected (" + count + ") --");
                        }

          $(".check").change(function() {

                if (count === 0 ){

                    message =  $("#sel-title").val();

                }

                if($(this).is(":checked")) {
                        count+=1;
                }else{
                    count-=1;
                }

                if (count === 0 ){

                   $("#sel-title").html(message);

                 }else {
                                       $("#sel-title").html("-- Selected (" + count + ") --");

                                   }

          });

          $("#delete-news").submit(function(){

               var checks = $("input:checkbox:checked").size();

               if (checks !== 0){

                   return confirm("Are you sure?");

               }else {
                    alert("Choose news!");
                    return false;
               }

          });

          $(".expire").submit(function(){

                return confirm("Are you shure?");

          })


});

      function changeState(elemId, elem){

            $("input#field" + elemId).removeAttr("disabled");
            $("input#update" + elemId).css({"display": "inline"});
            $("input#delete" + elemId).css({"display": "inline"});
            elem.style.display = "none";
            

     }

     function verifyUpdateForm(elemId){

           var elem =  $("#field" + elemId).val();
           elem = $.trim(elem);

           if (elem === ""){
                alert("Empty field!");
                return false;
           }

           return true;

     }

   






