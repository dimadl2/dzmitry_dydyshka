package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.TechnicalException;

/**
 * The service provides methods for working with news DAO
 */
public interface NewsService extends Service<NewsTO>, NewsSearchable, NewsNavigatable {

	/**
	 * The method allows attach author to news.
	 *
	 * @param newsId - ID news
	 * @param authorId - ID author
	 * @return
	 * @throws TechnicalException
	 */
	Long attachAuthorToNews(Long authorId, Long newsId) throws TechnicalException;

	/**
	 * The method allows attach tag to news.
	 *
	 *
	 * @param tagId - ID tag
	 * @param newsId - ID author
	 * @return
	 * @throws TechnicalException
	 */
	Long attachTagToNews(Long tagId, Long newsId) throws TechnicalException;

	/**
	 *
	 * The method allows disconnect all tags on news.
	 *
	 * @param newsId - ID news
	 * @throws TechnicalException
	 */
	void disconnectAllTagsOnNews(Long newsId) throws TechnicalException;

	/**
	 * The method allows disconnect all authors on news
	 *
	 * @param newsId - ID news
	 * @throws TechnicalException
	 */
	void disconnectAllAuthorsOnNews(Long newsId) throws TechnicalException;

	/**
	 * The method allows remove list of news.
	 *
	 * @param newsIds - The list of news ID to be removed
	 * @throws TechnicalException
	 */
	void delete(List<Long> newsIds) throws TechnicalException;


}
