package by.epam.newsapp.dto;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.dto.TagTO;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 3/25/2015.
 */
public class NewsVO {

    private NewsTO newsTO;
    /** The news comments. */
    private List<CommentTO> comments;

    /** The news authors. */
    private List<AuthorTO> authors;

    /** The news tags. */
    private List<TagTO> tags;

    private int countComments;

    public NewsTO getNewsTO() {
        return newsTO;
    }

    public void setNewsTO(NewsTO newsTO) {
        this.newsTO = newsTO;
    }

    public List<CommentTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentTO> comments) {
        this.comments = comments;
    }

    public List<AuthorTO> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorTO> authors) {
        this.authors = authors;
    }

    public List<TagTO> getTags() {
        return tags;
    }

    public void setTags(List<TagTO> tags) {
        this.tags = tags;
    }

    public int getCountComments() {
        return countComments;
    }

    public void setCountComments(int countComments) {
        this.countComments = countComments;
    }
}
