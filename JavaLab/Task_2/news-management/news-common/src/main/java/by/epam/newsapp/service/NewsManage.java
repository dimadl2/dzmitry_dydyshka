package by.epam.newsapp.service;

import by.epam.newsapp.dto.NewsVO;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.util.Page;

import java.util.List;

/**
 * Service provides method for working with News Value Object
 */
public interface NewsManage {

    /**
     * The method allows insert News Value Object into database.
     *
     * @param newsVO - News VO to be inserted
     * @return - ID new News in database
     * @throws TechnicalException
     */
    Long save (NewsVO newsVO) throws TechnicalException;

    /**
     *
     * The method allows fetch News Value Object from database
     *
     * @param id - ID news
     * @return - News Value Object
     *
     * @throws TechnicalException
     */
    NewsVO fetchById(Long id) throws TechnicalException;

    /**
     *
     * The method allow get al news in database.
     *
     * @return - the list of news
     * @throws TechnicalException
     */
    List<NewsVO> getAll() throws TechnicalException;

    /**
     *
     * The method allows update news.
     *
     * @param news - News to be updated
     * @throws TechnicalException
     */
    void update(NewsVO news) throws TechnicalException;


    /**
     *
     * The method allows get the specific page.
     *
     * @param page - number of the page
     * @param numberAtPage - number item at the page
     * @return - page {@link Page}
     * @throws TechnicalException
     *
     * @see Page
     */
    Page getPage(int page, int numberAtPage) throws TechnicalException;

    /**
     *
     * The method allows get the specific page.
     *
     * @param page - page number
     * @param numberAtPage - number item at the page
     * @param filter - filter for the page {@link Filter}
     * @return - page {@link Page}
     * @throws TechnicalException
     *
     * @see Page
     * @sse Filter
     */
    Page getFilteredPage(int page, int numberAtPage, Filter filter) throws TechnicalException;

}
