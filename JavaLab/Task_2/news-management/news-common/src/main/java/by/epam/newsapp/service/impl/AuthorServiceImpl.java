package by.epam.newsapp.service.impl;

import java.util.List;

import by.epam.newsapp.dao.impl.AuthorDAOImpl;
import by.epam.newsapp.dto.Entity;
import by.epam.newsapp.service.AuthorService;
import org.apache.log4j.Logger;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Class AuthorServiceImpl.
 */
@Service("authorService")
public class AuthorServiceImpl implements AuthorService {

	/** The author dao. */
	@Autowired
	private AuthorDAOImpl authorDAO;

	private static Logger log = Logger.getLogger(AuthorServiceImpl.class);


	/**
	 * 
	 * @see by.epam.newsapp.service.Service#save(Entity)
	 * 
	 */
	@Override
	public Long save(AuthorTO author) throws TechnicalException {

		Long id = null;

		try {

			id = authorDAO.add(author);

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return id;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#delete(Long)
	 * 
	 */
	@Override
	public boolean delete(Long authorId) throws TechnicalException {

		boolean flagResult = false;
		try {

			AuthorTO author = authorDAO.fetchById(authorId);
			if (author != null) {

				authorDAO.delete(authorId);
				flagResult = true;
			} 

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return flagResult;
	}

	/**
	 * @see by.epam.newsapp.service.Service#update(Entity)
	 */
	@Override
	public boolean update(AuthorTO author) throws TechnicalException {

		boolean flagResult = false;

		try {

			AuthorTO updatedAuthor = authorDAO.fetchById(author.getId());

			if (updatedAuthor != null) {

				authorDAO.update(author);
				flagResult = true;

			}

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return flagResult;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#fetchById(Long)
	 */
	@Override
	public AuthorTO fetchById(Long id) throws TechnicalException {

		AuthorTO author = null;

		try {

			author = authorDAO.fetchById(id);

		} catch (DAOException e) {

			throw new TechnicalException(e);
		}

		return author;

	}

	/**
	 * 
	 * @see by.epam.newsapp.service.Service#list()
	 */
	@Override
	public List<AuthorTO> list() throws TechnicalException {

		List<AuthorTO> list = null;

		try {

			list = authorDAO.list();

		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return list;

	}

	/**
	 *
	 * @see AuthorService#fetchAuthorsByNews(Long)
	 */
    @Override
    public List<AuthorTO> fetchAuthorsByNews(Long newsId) throws TechnicalException {

        List<AuthorTO> authors;
        try {
            authors = authorDAO.fetchAuthorsByNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return authors;

    }

	@Override
	public List<AuthorTO> getAllNotExpire() throws TechnicalException {

		try {
			return authorDAO.getAllNotExpire();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public void expire(Long authorId) throws TechnicalException {

		try {
			authorDAO.expire(authorId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public List<AuthorTO> fetchExpireAndNewsAuthor(Long newsId) throws TechnicalException {
		try {
			return authorDAO.fetchExpireAndNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}
}
