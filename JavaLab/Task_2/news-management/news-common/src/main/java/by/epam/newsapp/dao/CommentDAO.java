package by.epam.newsapp.dao;

import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * Interface provides method for working with table Comments in database
 */
public interface CommentDAO extends CommonDAO<CommentTO> {

    /**
     * Delete comments from database.
     *
     * @param listId
     *            - the list comments id, that will removed from database
     * @throws DAOException
     *             the DAO exception
     */
    void delete(List<Long> listId) throws DAOException;

    /**
     * Fetch comments by news.
     *
     * @param newsId
     *            the news id
     * @return the list of comments
     * @throws DAOException
     *             the DAO exception
     */
    List<CommentTO> fetchCommentsByNews(Long newsId) throws DAOException;

    /**
     * Returns number of commnets on news
     *
     * @param newsId - the news Id
     * @return - the number of comments on news
     * @throws DAOException
     */
    int getCountCommentsOnNews(Long newsId) throws DAOException;
}
