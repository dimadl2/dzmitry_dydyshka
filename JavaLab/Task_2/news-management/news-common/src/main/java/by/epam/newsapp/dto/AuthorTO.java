package by.epam.newsapp.dto;

import by.epam.newsapp.annotation.Column;
import by.epam.newsapp.annotation.Table;

/**
 * The Class of dto Author.
 */
@Table(name = "author")
public class AuthorTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The name author. */
	@Column(name = "name")
	private String name;

	/**
	 * Default constructor.
	 */
	public AuthorTO() {

	}

	/**
	 * Instantiates a new author by name.
	 *
	 * @param name
	 *            the name
	 */
	public AuthorTO(String name) {

		this.name = name;

	}

	/**
	 * Instantiates a new author by name and ID.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 */
	public AuthorTO(Long id, String name) {

		super(id);
		this.name = name;

	}

	/**
	 * Gets the name author.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name author.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * 
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
