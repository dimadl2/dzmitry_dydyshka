package by.epam.newsapp.dao;

import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * Interface provides method for working with table Comments in database
 */
public interface TagDAO extends CommonDAO<TagTO> {

    /**
     * Fetch tag by news.
     *
     * @param newsId the news id
     * @return the list tags
     * @throws DAOException the DAO exception
     */
    List<TagTO> fetchTagByNews(Long newsId) throws DAOException;
}
