package by.epam.newsapp.dao;


import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * The common interface provides C.R.U.D. methods for working with tables in database
 */
public interface CommonDAO <T> {

    /**
     * Returns the list containing all of the objects from table.
     *
     * @return the list containing all of the objects from table.
     * @throws DAOException
     *
     */
    List<T> list() throws DAOException;

    /**
     * Insert object into database.
     *
     * @param object
     *            -object to be inserted into database
     *
     * @return the ID inserted object
     * @throws DAOException
     *
     */
    Long add(T object) throws DAOException;

    /**
     * Delete object form database.
     *
     * @param id
     *            - the id deleted object
     * @throws DAOException
     *
     */
    void delete(Long id) throws DAOException;

    /**
     * Update existing object.
     *
     * @param object
     *            - the object to be update
     * @throws DAOException
     *
     */
    void update(T object) throws DAOException;

    /**
     * Fetch object form database by id.
     *
     * @param id
     *            the id search
     * @return the
     * @throws DAOException
     *             the DAO exception
     */
    T fetchById(Long id) throws DAOException;

}
