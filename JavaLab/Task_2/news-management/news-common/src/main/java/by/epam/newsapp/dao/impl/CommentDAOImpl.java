package by.epam.newsapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dao.CommentDAO;
import by.epam.newsapp.util.DAOUtils;
import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.exception.DAOException;
import org.springframework.stereotype.Repository;

/**
 * The Class provides access to Comment table in database.
 */

@Repository("commentDAO")
public class CommentDAOImpl extends AbstractDAO<CommentTO> implements CommentDAO {

    /** The SQL-query for fetch comments by news id */
	private static final String SQL_FETCH_BY_NEWS = "SELECT comments_id, comment_text, creation_date, news_id " +
                                                    "FROM comments " +
                                                    "WHERE news_id = ? ORDER BY creation_date DESC";
    private static final String COLUMN_COMMENTS_ID = "comments_id";
    private static final String COLUMN_COMMENT_TEXT = "comment_text";
    private static final String COLUMN_CREATION_DATE = "creation_date";
    private static final String COLUMN_NEWS_ID = "news_id";
    public static final String SQL_SELECT_COUNT_COMMENTS_ON_NEWS = "SELECT count(comments_id) as count " +
																	"FROM comments " +
																	"WHERE news_id = ?";
    public static final String COUNT = "count";
	private static final String SQL_DELETE_LIST_TAGS_PREFIX = "DELETE FROM tags WHERE tags_id IN (";

	/**
	 * Delete comments from database.
	 *
	 * @param listId
	 *            - the list comments id, that will removed from database
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void delete(List<Long> listId) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = dataSource.getConnection();

			StringBuilder builder = new StringBuilder(SQL_DELETE_LIST_TAGS_PREFIX);

			for (int i = 0; i < listId.size(); i++){

				builder.append("?");

				if (i == listId.size()-1){

					builder.append(",");

				}
			}

			builder.append(")");
			statement = connection.prepareStatement(builder.toString());

			for (int i = 0; i < listId.size(); i++){

				statement.setLong(i+1, listId.get(i));

			}

			statement.execute();

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.closeResources(connection, statement, null);
		}

	}

	/**
	 * Fetch comments by news.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list of comments
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<CommentTO> fetchCommentsByNews(Long newsId) throws DAOException {

		List<CommentTO> comments = new LinkedList<CommentTO>();

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_BY_NEWS);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				CommentTO comment = new CommentTO();

				comment.setId(resultSet.getLong(COLUMN_COMMENTS_ID));
				comment.setCommentText(resultSet.getString(COLUMN_COMMENT_TEXT));
				comment.setCreationDate(new Date(resultSet.getTimestamp(COLUMN_CREATION_DATE).getTime()));
                comment.setNewsId(resultSet.getLong(COLUMN_NEWS_ID));

				comments.add(comment);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return comments;

	}

	/**
	 * Returns number of commnets on news
	 *
	 * @param newsId - the news Id
	 * @return - the number of comments on news
	 * @throws DAOException
	 */
    public int getCountCommentsOnNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        int count = 0;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_COUNT_COMMENTS_ON_NEWS);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            while (resultSet.next()){

                count = resultSet.getInt(COUNT);

            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return  count;

    }

}
