package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.exception.TechnicalException;

/**
 *
 * The service provides methods for working with comment DAO
 *
 */
public interface CommentService extends Service<CommentTO> {

	/**
	 *
	 * The method allows fetch all comments by specific news
	 *
	 * @param newsId - the news id
	 * @return - The list of comments
	 */
	List<CommentTO> fetchCommentsByNews(Long newsId) throws TechnicalException;


	/**
	 *
	 * The mehtod allows get the total number of comments on news
	 *
	 * @param newsId - the news ID
	 * @return - The total number of comments
	 * @throws TechnicalException
	 */
	int getCountCommentsOnNews(Long newsId) throws TechnicalException;
}
