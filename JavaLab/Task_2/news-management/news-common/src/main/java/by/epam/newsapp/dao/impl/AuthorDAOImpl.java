package by.epam.newsapp.dao.impl;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import by.epam.newsapp.dao.AuthorDAO;
import by.epam.newsapp.util.DAOUtils;
import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import org.springframework.stereotype.Repository;

/**
 * The Class AuthorDAO.
 * 
 * 
 * 
 */
@Repository("authorDAO")
public class AuthorDAOImpl extends AbstractDAO<AuthorTO> implements AuthorDAO {

	/** SQL-query for fetch authors by news id. */
	private static final String SQL_FETCH_BY_NEWS = "SELECT author.* FROM news_author JOIN author ON news_author.author_id = author.author_id WHERE news_id = ?";
    private static final String COLUMN_AUTHOR_ID = "author_id";
    private static final String COLUMN_NAME = "name";
	private static final String SQL_SELECT_NO_EXPIRE = "SELECT author.author_id, author.name FROM author WHERE exp IS NULL";
	private static final String SQL_UPDATE_AUTHOR = "UPDATE author SET exp=CURRENT_TIMESTAMP  WHERE author_id = ?";
	private static final String SQL_FETCH_EXPIRE_AND_NEWS_AUTHOR = "SELECT DISTINCT author.author_id, author.name " +
			"FROM news_author " +
			"RIGHT JOIN author " +
			"ON news_author.AUTHOR_ID = author.AUTHOR_ID " +
			"WHERE exp IS NULL " +
			"UNION " +
			"SELECT author.author_id, author.name " +
			"FROM NEWS_AUTHOR JOIN AUTHOR ON NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID " +
			"WHERE NEWS_AUTHOR.NEWS_ID = ?";


	/**
	 * @see AuthorDAO#fetchAuthorsByNews(Long)
	 * Fetch authors by news id.
	 *
	 * @param newsId
	 *            the news id search
	 * @return the list of authors
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<AuthorTO> fetchAuthorsByNews(Long newsId) throws DAOException {

		List<AuthorTO> authors = new LinkedList<AuthorTO>();

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_BY_NEWS);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				AuthorTO author = new AuthorTO();

				author.setId(resultSet.getLong(COLUMN_AUTHOR_ID));
				author.setName(resultSet.getString(COLUMN_NAME));

				authors.add(author);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return authors;

	}

	public List<AuthorTO> getAllNotExpire() throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<AuthorTO> authors = new LinkedList<>();

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_SELECT_NO_EXPIRE);
			resultSet = statement.executeQuery();

			while (resultSet.next()){

				AuthorTO authorTO = new AuthorTO(resultSet.getLong(1), resultSet.getString(2));
				authors.add(authorTO);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return authors;

	}

	public void expire(Long authorID) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			statement.setLong(1, authorID);
			statement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.closeResources(connection, statement, null);
		}


	}

	public List<AuthorTO> fetchExpireAndNewsAuthor(Long newsId) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<AuthorTO> authors = new LinkedList<>();

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_EXPIRE_AND_NEWS_AUTHOR);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();

			while (resultSet.next()){

				Long id = resultSet.getLong(1);
				String name = resultSet.getString(2);
				AuthorTO author = new AuthorTO(id, name);

				authors.add(author);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.closeResources(connection, statement, resultSet);
		}

		return authors;
	}

}
