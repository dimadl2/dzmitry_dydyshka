package by.epam.newsapp.dao;

import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.Filter;

import java.util.List;
import java.util.Map;

/**
 * Created by Dzmitry_Dydyshka on 4/28/2015.
 */
public interface NewsDAO extends CommonDAO<NewsTO> {

    /**
     *
     * Method allow fetch limit list of news
     *
     * @param startPosition - start position for fetching list of news
     * @param endPosition - end position for fetching list o news
     * @return List of news
     * @throws DAOException
     */
    List<NewsTO> fetchLimitNews(int startPosition, int endPosition) throws DAOException;

    /**
     *
     * Method allow remove list of news
     *
     * @param newsIds - ID  of news, that will be removed
     * @throws DAOException
     */
    void delete(List<Long> newsIds) throws DAOException;

    /**
     * Attach author to news.
     *
     * @param authorId the author id
     * @param newsId   the news id
     * @return the long
     * @throws DAOException the DAO exception
     */
    Long attachAuthorToNews(Long authorId, Long newsId)throws DAOException;

    /**
     * Attach tag to news.
     *
     * @param tagId  the tag id
     * @param newsId the news id
     * @return the long
     * @throws DAOException the DAO exception
     */
    Long attachTagToNews(Long tagId, Long newsId) throws DAOException;

    /**
     * Disconnect all tags on news.
     *
     * @param newsId - the news id
     * @throws DAOException the DAO exception
     */
    void disconnectAllTagsOnNews(Long newsId) throws DAOException;

    /**
     * Disconnect all authors on news.
     *
     * @param newsId - the news id
     * @throws DAOException
     */
    void disconnectAllAuthorsOnNews(Long newsId) throws DAOException;

    /**
     *
     * The method allows get the total number of news in the database
     *
     * @return the total number of news
     * @throws DAOException
     */
    int getCountRows() throws DAOException;

    /**
     *
     * The method allows get next and previous news ID.
     *
     * @param currentNewsId - the current news ID
     * @return the map, that keep previous value under 'previous' key and next value under 'next' key.
     * @throws DAOException
     */
    Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws DAOException;

    /**
     *
     * The method allows find news according to filter.
     *
     * @param filter - the filter
     * @param startPosition - the start position for limit
     * @param endPosition - the end position for limit
     * @return - the limit list of founding news
     * @throws DAOException
     */
    List<NewsTO> fetchSearchLimit(Filter filter, int startPosition, int endPosition) throws DAOException;

    /**
     *
     * The method allows get count of searching news
     *
     * @param filter - the filter
     * @return - size of searching
     * @throws DAOException
     */
    int getCountSearchRows(Filter filter) throws DAOException;

}
