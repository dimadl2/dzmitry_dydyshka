package by.epam.newsapp.service.impl;

import java.util.List;
import java.util.Map;

import by.epam.newsapp.service.NewsService;
import by.epam.newsapp.util.Filter;
import org.apache.log4j.Logger;

import by.epam.newsapp.dao.impl.AuthorDAOImpl;
import by.epam.newsapp.dao.impl.CommentDAOImpl;
import by.epam.newsapp.dao.impl.NewsDAOImpl;
import by.epam.newsapp.dao.impl.TagDAOImpl;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Class NewsServiceImpl.
 */
@Service
public class NewsServiceImpl implements NewsService {


    /**
     * The news dao.
     */
    @Autowired
    private NewsDAOImpl newsDAO;

    /**
     * The comment dao.
     */
    @Autowired
    private CommentDAOImpl commentDAO;

    /**
     * The tag dao.
     */
    @Autowired
    private TagDAOImpl tagDAO;

    /**
     * The author dao.
     */
    @Autowired
    private AuthorDAOImpl authorDAO;

    /**
     * The logger.
     */
    private static Logger log = Logger.getLogger(NewsServiceImpl.class);

    /*
     * @see Service#save(Entity)
     *
     */
    @Override
    public Long save(NewsTO news) throws TechnicalException {

        Long newsId = null;
        try {

            newsId = newsDAO.add(news);

        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return newsId;
    }

    /**
     * @see by.epam.newsapp.service.Service#delete(Long)
     */
    @Override
    public boolean delete(Long newsId) throws TechnicalException {

        boolean flagResult = false;
        try {

            NewsTO news = newsDAO.fetchById(newsId);
            if (news != null) {

                newsDAO.delete(newsId);
                flagResult = true;

            }

        } catch (DAOException e) {

            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#update(by.epam.newsapp.dto.Entity)
     */
    @Override
    public boolean update(NewsTO news) throws TechnicalException {

        boolean flagResult = false;

        try {

            NewsTO updatedNews = newsDAO.fetchById(news.getId());

            if (updatedNews != null) {

                newsDAO.update(news);
                flagResult = true;

            }

        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return flagResult;

    }

    /**
     * @see by.epam.newsapp.service.Service#fetchById(Long)
     */
    @Override
    public NewsTO fetchById(Long newsId) throws TechnicalException {

        NewsTO news = null;
        try {

            news = newsDAO.fetchById(newsId);

        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return news;

    }

    /**
     * @see by.epam.newsapp.service.Service#list()
     */
    @Override
    public List<NewsTO> list() throws TechnicalException {
        List<NewsTO> list = null;
        try {
            list = newsDAO.list();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

        return list;
    }

    /**
     * @see NewsService#attachAuthorToNews(Long, Long)
     */
    @Override
    public Long attachAuthorToNews(Long authorId, Long newsId) throws TechnicalException {

        try {
            return newsDAO.attachAuthorToNews(authorId, newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see NewsService#attachTagToNews(Long, Long)
     */
    @Override
    public Long attachTagToNews(Long tagId, Long newsId) throws TechnicalException {

        try {
            return newsDAO.attachTagToNews(tagId, newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see NewsService#disconnectAllTagsOnNews(Long)
     */
    @Override
    public void disconnectAllTagsOnNews(Long newsId) throws TechnicalException {

        try {
            newsDAO.disconnectAllTagsOnNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see NewsService#disconnectAllAuthorsOnNews(Long)
     */
    @Override
    public void disconnectAllAuthorsOnNews(Long newsId) throws TechnicalException {

        try {
            newsDAO.disconnectAllAuthorsOnNews(newsId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see by.epam.newsapp.service.Service#delete(Long)
     */
    @Override
    public void delete(List<Long> newsIds) throws TechnicalException {

        try {
            newsDAO.delete(newsIds);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see by.epam.newsapp.service.NewsService#fetchLimitNews(int, int)
     */
    @Override
    public List<NewsTO> fetchLimitNews(int startPosition, int endPosition) throws TechnicalException {

        try {
            return newsDAO.fetchLimitNews(startPosition, endPosition);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * @see NewsService#getCountRows()
     */
    @Override
    public int getCountRows() throws TechnicalException {

        try {
            return newsDAO.getCountRows();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public List<NewsTO> fetchSearchLimit(Filter filter, int startPosition, int endPosition) throws TechnicalException {
        try {
            return newsDAO.fetchSearchLimit(filter, startPosition, endPosition);
        }catch (DAOException e){
            throw new TechnicalException(e);
        }
    }

    @Override
    public int getCountSearchRows(Filter filter) throws TechnicalException {
        try {
            return newsDAO.getCountSearchRows(filter);
        }catch (DAOException e){
            throw new TechnicalException(e);
        }
    }


    @Override
    public Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws TechnicalException {
        try {
            return newsDAO.getNextAndPreviousNewsId(currentNewsId);
        }catch (DAOException e){
            throw new TechnicalException(e);
        }
    }
}
