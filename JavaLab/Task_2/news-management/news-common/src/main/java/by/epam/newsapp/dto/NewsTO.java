package by.epam.newsapp.dto;

import java.util.Date;

import by.epam.newsapp.annotation.Column;
import by.epam.newsapp.annotation.Table;

/**
 * The Class News.
 */
@Table(name = "news")
public class NewsTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The short text. */
	@Column(name = "short_text")
	private String shortText;

	/** The full text. */
	@Column(name = "full_text")
	private String fullText;

	/** The title. */
	@Column(name = "title")
	private String title;

	/** The creation date. */
	@Column(name = "creation_date")
	private Date creationDate;

	/** The modification date. */
	@Column(name = "modification_date")
	private Date modificationDate;


	/**
	 * Gets the short text.
	 *
	 * @return the shortText
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text.
	 *
	 * @param shortText
	 *            the shortText to set
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the fullText
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText
	 *            the fullText to set
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        NewsTO news = (NewsTO) o;

        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null)
            return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }
}
