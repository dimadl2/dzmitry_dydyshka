package by.epam.newsapp.dao;

import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * Interface provides method for working with table Authors in database
 */
public interface AuthorDAO extends CommonDAO<AuthorTO> {

    /**
     * Fetch authors by news id.
     *
     * @param newsId
     *            the news id search
     * @return the list of authors
     * @throws DAOException
     *             the DAO exception
     */
    List<AuthorTO> fetchAuthorsByNews(Long newsId) throws DAOException;

    List<AuthorTO> getAllNotExpire() throws DAOException;
    void expire(Long authorID) throws DAOException;
    List<AuthorTO> fetchExpireAndNewsAuthor(Long newsId) throws DAOException;

}
