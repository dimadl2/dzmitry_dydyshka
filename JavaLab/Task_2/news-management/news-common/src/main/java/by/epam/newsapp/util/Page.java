package by.epam.newsapp.util;

import by.epam.newsapp.dto.Entity;
import by.epam.newsapp.dto.NewsVO;

import java.util.List;

/**
 * Created by Dzmitry_Dydyshka on 4/10/2015.
 */
public class Page {

    private int numberAtPage;
    private int numberItems;
    private List<NewsVO> itemsForPage;

    public Page(int numberAtPage, int numberItems, List<NewsVO> itemsForPage) {
        this.numberAtPage = numberAtPage;
        this.numberItems = numberItems;
        this.itemsForPage = itemsForPage;
    }

    public int getNumberOfPage(){

        int numberOfPage = (int) Math.ceil(numberItems/ (double) numberAtPage);

        return numberOfPage;

    }

    public void setNumberAtPage(int numberAtPage) {
        this.numberAtPage = numberAtPage;
    }

    public void setNumberItems(int numberItems) {
        this.numberItems = numberItems;
    }

    public void setItemsForPage(List<NewsVO> itemsForPage) {
        this.itemsForPage = itemsForPage;
    }

    public List<NewsVO> getItemsForPage() {
        return itemsForPage;
    }
}
