package by.epam.newsapp.service;

import java.util.List;

import by.epam.newsapp.dto.TagTO;
import by.epam.newsapp.exception.TechnicalException;


/**
 *
 * The service provides methods for working with tag DAO
 *
 */
public interface TagService extends Service<TagTO> {

	/**
	 *
	 * The method allows fetch all tags by specific news.
	 *
	 * @param newsId - the news ID
	 * @return - The list of authors
	 * @throws TechnicalException
	 */
	List<TagTO> fetchTagByNews(Long newsId) throws TechnicalException;

}
