package by.epam.newsapp.service;

import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.util.Filter;

import java.util.List;


public interface NewsSearchable {

    /**
     * The method allows fetch the limit list of news.
     *
     * @param startPosition - the start position of limit
     * @param endPosition   - the end position of limit
     * @return - the limit list of news
     * @throws TechnicalException
     */
    List<NewsTO> fetchLimitNews(int startPosition, int endPosition) throws TechnicalException;

    /**
     * The method allows get number of news.
     *
     * @return - number of news
     * @throws TechnicalException
     */
    int getCountRows() throws TechnicalException;

    List<NewsTO> fetchSearchLimit(Filter filter, int startPosition, int endPosition ) throws TechnicalException;


    int getCountSearchRows(Filter filter) throws TechnicalException;
}
