package by.epam.newsapp.dao.impl;

import java.sql.*;
import java.util.*;

import by.epam.newsapp.dao.NewsDAO;
import by.epam.newsapp.util.DAOUtils;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.util.Filter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

/**
 * The Class provides access to News table in database.
 */
@Repository("newsDAO")
public class NewsDAOImpl extends AbstractDAO<NewsTO> implements NewsDAO {


    private static final String PREVIOUS = "previous";
    private static final String NEXT = "next";

    // Column names
    private static final String COLUMN_MODIFICATION_DATE = "modification_date";
    private static final String COLUMN_CREATION_DATE = "creation_date";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_FULL_TEXT = "full_text";
    private static final String COLUMN_SHORT_TEXT = "short_text";
    private static final String COLUMN_NEWS_ID = "news_id";

    // Column id name
    private static final String NEWS_AUTHOR_ID = "news_author_id";
    private static final String NEWS_TAG_ID = "news_tag_id";

    private static final String SQL_DELETE_ALL_TAGS_ON_NEWS = "DELETE FROM news_tag WHERE news_id = ?";
    private static final String SQL_DELETE_ALL_AUTHORS_ON_NEWS = "DELETE FROM news_author WHERE news_id = ?";
    private static final String SQL_ADD_TAG_TO_NEWS = "INSERT INTO news_tag VALUES(Null, ?, ?)";
    private static final String SQL_ADD_AUTHOR_TO_NEWS = "INSERT INTO news_author VALUES (NULL, ?, ?)";

    private static final String SQL_FETCH_LIMIT_LIST = "SELECT " +
            "news_id, short_text, full_text, title, creation_date," +
            " modification_date" +
            " FROM ( " +
            "SELECT rownum rnum,news_id, short_text, full_text, title," +
            " creation_date, modification_date  " +
            "FROM (" +
            "SELECT news.news_id, news.title, news.short_text, " +
            "news.full_text, news.creation_date, " +
            "news.modification_date, count(comments.comments_id) as count " +
            "FROM news " +
            "LEFT JOIN comments " +
            "ON news.news_id = comments.news_id " +
            "GROUP BY news.news_id, news.title, news.short_text, " +
            "news.full_text, news.creation_date, " +
            "news.modification_date " +
            "ORDER BY count DESC, news.modification_date DESC" +
            ") " +
            "WHERE rownum <= ?" +
            ") " +
            "WHERE rnum >= ?";

    private static final String SQL_GET_COUNT_ROWS = "SELECT count(*) FROM news";

    private static final String SQL_SEARCH_PREFIX = "WITH news_list AS (SELECT news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date, count(comments.comments_id) as count \n" +
            "            FROM news \n" +
            "            LEFT JOIN comments \n" +
            "            ON news.news_id = comments.news_id \n" +
            "            GROUP BY news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date \n" +
            "            ORDER BY count DESC, news.MODIFICATION_DATE DESC)\n" +
            "SELECT DISTINCT \n" +
            "            news_id, short_text, full_text, title, creation_date,\n" +
            "            modification_date\n" +
            "            FROM (\n" +
            "            SELECT rownum rnum,news_id, short_text, full_text, title,\n" +
            "            creation_date, modification_date \n" +
            "            FROM (\n" +
            "            SELECT news_list.*\n" +
            "            FROM  news_list";

    private static final String SQL_SEARCH_BY_TAGS = " JOIN News_tag\n" +
            "                        ON  news_list.NEWS_ID = news_tag.NEWS_ID\n" +
            "                        AND NEWS_TAG.TAG_ID IN(";
    private static final String SQL_SEARCH_BY_AUTHOR = " JOIN NEWS_AUTHOR ON news_list.news_id = NEWS_AUTHOR.NEWS_ID\n" +
            "                        AND news_author.author_id = ?";

    private static final String SQL_SEARCH_POSTFIX = " ) " +
            "            WHERE rownum <= ?" +
            "            ) " +
            "            WHERE rnum >= ?";
    private static final String SQL_GET_NEXT_PREVIOUS_ID = "WITH slc AS (\n" +
            "            SELECT news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date, count(comments.comments_id) as count \n" +
            "            FROM news \n" +
            "            LEFT JOIN comments \n" +
            "            ON news.news_id = comments.news_id \n" +
            "            GROUP BY news.news_id, news.title, news.short_text, \n" +
            "            news.full_text, news.creation_date, \n" +
            "            news.modification_date)\n" +
            "select previous, next from (select  slc.news_id as id,\n" +
            "lag(slc.news_id) over (order by slc.count desc, slc.modification_date DESC ) as previous  ,      \n" +
            "lead(slc.news_id) over (order by slc.count desc, slc.modification_date DESC ) as next  from \n" +
            "slc) where id = ?";

    public static final Logger LOG = Logger.getLogger(NewsDAOImpl.class);


    /**
     *
     * Method allow remove list of news
     *
     * @param newsIds - ID  of news, that will be removed
     * @throws DAOException
     */
    @Override
    public void delete(List<Long> newsIds) throws DAOException {


        Connection connection = null;
        PreparedStatement statement = null;


        StringBuilder query = new StringBuilder("DELETE FROM news WHERE news.news_id IN (");

       for (int i = 0; i < newsIds.size(); i++){

           query.append("?");

           if(i != newsIds.size()-1){

               query.append(",");

           }

       }

        query.append(")");

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(query.toString());

            for (int i = 0; i < newsIds.size(); i++){

                statement.setLong(i+1, newsIds.get(i));

            }

            statement.execute();


        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(connection, statement, null);
        }

    }

    /**
     *
     * Method allow fetch limit list of news
     *
     * @param startPosition - start position for fetching list of news
     * @param endPosition - end position for fetching list o news
     * @return List of news
     * @throws DAOException
     */
    @Override
    public List<NewsTO> fetchLimitNews(int startPosition, int endPosition) throws DAOException {

        List<NewsTO> news = new LinkedList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FETCH_LIMIT_LIST);
            statement.setInt(1, endPosition);
            statement.setInt(2, startPosition);

            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                NewsTO newsTO = createFromResultSet(resultSet);
                news.add(newsTO);

            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(connection, statement, resultSet);
        }

        return news;
    }

    /**
     * Attach author to news.
     *
     * @param authorId the author id
     * @param newsId   the news id
     * @return the long
     * @throws DAOException the DAO exception
     */
    @Override
    public Long attachAuthorToNews(Long authorId, Long newsId)
            throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        Long id = null;

        try {

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_ADD_AUTHOR_TO_NEWS,
                    new String[]{NEWS_AUTHOR_ID});

            statement.setLong(1, newsId);
            statement.setLong(2, authorId);

            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            resultSet.next();
            id = resultSet.getLong(1);

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {

            DAOUtils.closeResources(connection, statement, resultSet);
        }

        return id;
    }

    /**
     * Attach tag to news.
     *
     * @param tagId  the tag id
     * @param newsId the news id
     * @return the long
     * @throws DAOException the DAO exception
     */
    @Override
    public Long attachTagToNews(Long tagId, Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        Long id = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_ADD_TAG_TO_NEWS,
                    new String[]{NEWS_TAG_ID});

            statement.setLong(1, newsId);
            statement.setLong(2, tagId);

            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            resultSet.next();
            id = resultSet.getLong(1);

        } catch (SQLException e) {
            throw new DAOException();
        } finally {

            DAOUtils.closeResources(connection, statement, resultSet);
        }

        return id;

    }

    /**
     * Disconnect all tags on news.
     *
     * @param newsId - the news id
     * @throws DAOException the DAO exception
     */
    @Override
    public void disconnectAllTagsOnNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dataSource.getConnection();
            statement = connection
                    .prepareStatement(SQL_DELETE_ALL_TAGS_ON_NEWS);
            statement.setLong(1, newsId);
            statement.executeQuery();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {

            DAOUtils.closeResources(connection, statement, null);

        }

    }

    /**
     * Disconnect all authors on news.
     *
     * @param newsId - the news id
     * @throws DAOException
     */
    @Override
    public void disconnectAllAuthorsOnNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = dataSource.getConnection();
            statement = connection
                    .prepareStatement(SQL_DELETE_ALL_AUTHORS_ON_NEWS);
            statement.setLong(1, newsId);
            statement.executeQuery();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {

            DAOUtils.closeResources(connection, statement, null);

        }

    }

    /**
     *
     * The method allows get the total number of news in the database
     *
     * @return the total number of news
     * @throws DAOException
     */
    @Override
    public int getCountRows() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        int count = 0;

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_GET_COUNT_ROWS);

            resultSet.next();

            count = resultSet.getInt(1);


        } catch (SQLException e) {
            throw new DAOException(e);

        } finally {
            DAOUtils.closeResources(connection, statement, resultSet);
        }

        return count;

    }

    /**
     *
     * The method allows get next and previous news ID.
     *
     * @param currentNewsId - the current news ID
     * @return the map, that keep previous value under 'previous' key and next value under 'next' key.
     * @throws DAOException
     */
    @Override
    public Map<String, Long> getNextAndPreviousNewsId(Long currentNewsId) throws DAOException {

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        Map<String, Long> data = new HashMap<>();

        try {
            conn = dataSource.getConnection();
            st = conn.prepareCall(SQL_GET_NEXT_PREVIOUS_ID);
            st.setLong(1, currentNewsId);

            rs = st.executeQuery();
            rs.next();

            data.put(PREVIOUS, rs.getLong(PREVIOUS));
            data.put(NEXT, rs.getLong(NEXT));

        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            DAOUtils.closeResources(conn, st, rs);
        }

        return data;
    }

    /**
     *
     * The method allows find news according to filter.
     *
     * @param filter - the filter
     * @param startPosition - the start position for limit
     * @param endPosition - the end position for limit
     * @return - the limit list of founding news
     * @throws DAOException
     */
    @Override
    public List<NewsTO> fetchSearchLimit(Filter filter, int startPosition, int endPosition) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        List<NewsTO> news = new LinkedList<>();

        try {
            connection = dataSource.getConnection();
            String query = generateSearchQuery(filter);

            LOG.info(query);
            statement = connection.prepareStatement(query);

            List<Long> tagsId = filter.getTagsId();

            int tagSize = tagsId.size();

            for (int i = 0; i < tagSize; i++) {

                statement.setLong(i + 1, tagsId.get(i));

            }

            if (filter.getAuthorId() != null && filter.getAuthorId() != 0L) {
                statement.setLong(tagSize += 1, filter.getAuthorId());
            }

            statement.setInt(tagSize += 1, endPosition);
            statement.setInt(tagSize += 1, startPosition);

            rs = statement.executeQuery();

            while (rs.next()) {

                NewsTO newsTO = createFromResultSet(rs);
                news.add(newsTO);

            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(connection, statement, rs);
        }
        return news;
    }

    /**
     *
     * The method allows get count of searching news
     *
     * @param filter - the filter
     * @return - size of searching
     * @throws DAOException
     */
    @Override
    public int getCountSearchRows(Filter filter) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        int count = 0;

        try {
            connection = dataSource.getConnection();

            String query = generateSearchQuery(filter);

            statement = connection.prepareStatement(query);

            List<Long> tagsId = filter.getTagsId();

            int tagSize = tagsId.size();

            for (int i = 0; i < tagSize; i++) {

                statement.setLong(i + 1, tagsId.get(i));

            }

            if (filter.getAuthorId() != null && filter.getAuthorId() != 0L) {
                statement.setLong(tagSize += 1, filter.getAuthorId());
            }

            statement.setInt(tagSize += 1, Integer.MAX_VALUE);
            statement.setInt(tagSize += 1, 1);

            rs = statement.executeQuery();



            while (rs.next()) {
                ++count;
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            DAOUtils.closeResources(connection, statement, rs);
        }

        return count;
    }

    /**
     *
     * The method allows generate SQL-query according to filter.
     *
     * @param filter - the filter
     * @return
     */
    private String generateSearchQuery(Filter filter) {

        Long authorId = filter.getAuthorId();
        List<Long> tags = filter.getTagsId();

        StringBuilder query = new StringBuilder(SQL_SEARCH_PREFIX);

        if (!CollectionUtils.isEmpty(tags)) {

            query.append(SQL_SEARCH_BY_TAGS);
            for (int i = 0; i < tags.size(); i++) {

                query.append("?");
                if (i != tags.size() - 1) {

                    query.append(",");

                }

            }
            query.append(")");

        }
        if (authorId != null && authorId != 0L) {

            query.append(SQL_SEARCH_BY_AUTHOR);
        }

        query.append(SQL_SEARCH_POSTFIX);

        return query.toString();

    }

    /**
     * Creates news from result set.
     *
     * @param resultSet the result set
     * @return the news
     * @throws java.sql.SQLException the SQL exception
     */
    private NewsTO createFromResultSet(ResultSet resultSet) throws SQLException {

        NewsTO news = new NewsTO();

        news.setId(resultSet.getLong(COLUMN_NEWS_ID));
        news.setShortText(resultSet.getString(COLUMN_SHORT_TEXT));
        news.setFullText(resultSet.getString(COLUMN_FULL_TEXT));
        news.setTitle(resultSet.getString(COLUMN_TITLE));
        news.setCreationDate(resultSet.getTimestamp(COLUMN_CREATION_DATE));
        news.setModificationDate(resultSet
                .getTimestamp(COLUMN_MODIFICATION_DATE));

        return news;

    }
}
