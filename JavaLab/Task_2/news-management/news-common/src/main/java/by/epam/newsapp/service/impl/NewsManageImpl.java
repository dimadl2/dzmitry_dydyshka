package by.epam.newsapp.service.impl;

import by.epam.newsapp.dto.*;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.CommentService;
import by.epam.newsapp.service.NewsManage;
import by.epam.newsapp.util.Filter;
import by.epam.newsapp.util.Page;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * The class, which implements interface {@link NewsManage}
 *
 */
@Service
public class NewsManageImpl implements NewsManage {

    /**
     * News service
     */
    @Autowired
    private NewsServiceImpl newsService;

    /**
     * Author Service
     */
    @Autowired
    private AuthorServiceImpl authorService;

    /**
     * Comment service
     */
    @Autowired
    private CommentService commentService;

    /**
     * Tags service
     */
    @Autowired
    private TagServiceImpl tagService;

    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(NewsManageImpl.class);

    /**
     *
     * @see NewsManage#save(NewsVO)
     *
     * When news inserts into database, related authors and tags insert
     * into specific tables
     *
     * @param newsVO - News VO to be inserted
     * @return - ID new news
     * @throws TechnicalException
     */
    @Override
    public Long save(NewsVO newsVO) throws TechnicalException {

        Long newsId = null;

        NewsTO newsTO = newsVO.getNewsTO();
        newsId = newsService.save(newsTO);

        List<AuthorTO> authors = newsVO.getAuthors();
        List<TagTO> tags = newsVO.getTags();

        for (AuthorTO author : authors) {

            newsService.attachAuthorToNews(author.getId(), newsId);

        }

        for (TagTO tag : tags) {

            newsService.attachTagToNews(tag.getId(), newsId);
        }


        return newsId;
    }

    /**
     *
     * @see NewsManage#fetchById(Long)
     *
     * @param newsId - ID news
     * @return News Value Object with authors, tags and comments
     * @throws TechnicalException
     */

    @Override
    public NewsVO fetchById(Long newsId) throws TechnicalException {

        NewsTO newsTO = newsService.fetchById(newsId);

        List<CommentTO> comments = commentService.fetchCommentsByNews(newsId);
        List<TagTO> tags = tagService.fetchTagByNews(newsId);
        List<AuthorTO> authors = authorService.fetchAuthorsByNews(newsId);

        NewsVO newsVO = new NewsVO();
        newsVO.setNewsTO(newsTO);
        newsVO.setAuthors(authors);
        newsVO.setComments(comments);
        newsVO.setTags(tags);

        return newsVO;
    }

    /**
     *
     * @see NewsManage#getAll()
     *
     * @return - The list of news with authors, tags and number of comments
     * @throws TechnicalException
     */

    @Override
    public List<NewsVO> getAll() throws TechnicalException {

        List<NewsVO> listNewsVO = new LinkedList<>();
        List<NewsTO> listNewsTO = newsService.list();
        for (NewsTO newsTO : listNewsTO) {

            Long newsId = newsTO.getId();
            List<TagTO> tags = tagService.fetchTagByNews(newsId);
            List<AuthorTO> authors = authorService.fetchAuthorsByNews(newsId);
            int count = commentService.getCountCommentsOnNews(newsId);

            NewsVO newsVO = new NewsVO();
            newsVO.setNewsTO(newsTO);
            newsVO.setTags(tags);
            newsVO.setAuthors(authors);
            newsVO.setCountComments(count);

            listNewsVO.add(newsVO);
        }

        return listNewsVO;
    }

    /**
     *
     * @see NewsManage#update(NewsVO)
     *
     * When news updates, related tags and authors updates at the specific
     * tables in database
     *
     * @param news - News to be updated
     * @throws TechnicalException
     */

    @Override
    public void update(NewsVO news) throws TechnicalException {

        newsService.update(news.getNewsTO());

        Long newsId = news.getNewsTO().getId();
        newsService.disconnectAllTagsOnNews(newsId);

        for (TagTO tag : news.getTags()) {

            newsService.attachTagToNews(tag.getId(), newsId);

        }

        newsService.disconnectAllAuthorsOnNews(newsId);

        for (AuthorTO author : news.getAuthors()) {

            newsService.attachAuthorToNews(author.getId(), newsId);
        }

    }


    /**
     *
     * @see NewsManage#getPage(int, int)
     *
     * @param numberPage
     * @param numberAtPage - number item at the page
     * @return - The page, that contains the list of news with authors,
     *           tags and number of comments
     * @throws TechnicalException
     */

    @Override
    public Page getPage(int numberPage, int numberAtPage) throws TechnicalException {

        List<NewsVO> listNewsVO = new LinkedList<>();

        int startPosition = numberPage * numberAtPage + 1 - numberAtPage;
        int endPosition = numberPage * numberAtPage;

        List<NewsTO> listNewsTO = newsService.fetchLimitNews(startPosition, endPosition);

        for (NewsTO newsTO : listNewsTO) {

            Long newsId = newsTO.getId();

            List<TagTO> tags = tagService.fetchTagByNews(newsId);
            List<AuthorTO> authors = authorService.fetchAuthorsByNews(newsId);
            int count = commentService.getCountCommentsOnNews(newsId);

            NewsVO newsVO = new NewsVO();
            newsVO.setNewsTO(newsTO);
            newsVO.setTags(tags);
            newsVO.setAuthors(authors);
            newsVO.setCountComments(count);

            listNewsVO.add(newsVO);
        }

        int numberAllNews = newsService.getCountRows();

        Page page = new Page(numberAtPage, numberAllNews, listNewsVO);

        return page;

    }

    /**
     *
     * @see NewsManage#getPage(int, int)
     *
     * If the list of tags is empty, news search by authors.
     * If the ID authors is empty, news search by tags.
     *
     * @param numberPage - page number
     * @param numberAtPage - number item at the page
     * @param filter - filter for the page {@link Filter}
     * @return - page {@link Page} with the list of news with authors, tags and
     *          number of comments
     * @throws TechnicalException
     */
    @Override
    public Page getFilteredPage(int numberPage, int numberAtPage, Filter filter) throws TechnicalException {

        List<NewsVO> listNewsVO = new LinkedList<>();

        int startPosition = numberPage * numberAtPage + 1 - numberAtPage;
        int endPosition = numberPage * numberAtPage;

        List<NewsTO> listNewsTO = newsService.fetchSearchLimit(filter, startPosition, endPosition);
        int numberAllNews = newsService.getCountSearchRows(filter);


        for (NewsTO newsTO : listNewsTO) {

            Long newsId = newsTO.getId();
            List<TagTO> tags = tagService.fetchTagByNews(newsId);
            List<AuthorTO> authors = authorService.fetchAuthorsByNews(newsId);
            int count = commentService.getCountCommentsOnNews(newsId);

            NewsVO newsVO = new NewsVO();
            newsVO.setNewsTO(newsTO);
            newsVO.setTags(tags);
            newsVO.setAuthors(authors);
            newsVO.setCountComments(count);

            listNewsVO.add(newsVO);
        }

        Page page = new Page(numberAtPage, numberAllNews, listNewsVO);

        return page;

    }


}
