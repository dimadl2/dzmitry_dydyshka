package by.epam.newsapp.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsapp.dao.impl.CommentDAOImpl;
import by.epam.newsapp.dto.CommentTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.impl.CommentServiceImpl;

@ContextConfiguration(locations = { "classpath:/spring.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {

	@Mock
	private CommentDAOImpl commentDAO;

	@InjectMocks
	@Autowired
	private CommentServiceImpl service;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAdd() throws Exception {

		Long id = new Long(1);

		when(commentDAO.add(any(CommentTO.class))).thenReturn(id);

		CommentTO comment = new CommentTO(id, "test", new Date(), id);

		Long commentId = service.save(comment);
		Assert.assertEquals(id, commentId);
		verify(commentDAO).add(comment);
		ArgumentCaptor<CommentTO> commnetCaptor = ArgumentCaptor
				.forClass(CommentTO.class);

		verify(commentDAO).add(commnetCaptor.capture());
		CommentTO newComment = commnetCaptor.getValue();
		Assert.assertEquals(comment.getCommentText(),
				newComment.getCommentText());
		Assert.assertEquals(comment.getCreationDate(),
				newComment.getCreationDate());
		Assert.assertEquals(comment.getNewsId(),
				newComment.getNewsId());
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=TechnicalException.class)
	public void testAddThrowException() throws Exception {
		
		when(commentDAO.add(any(CommentTO.class))).thenThrow(DAOException.class);

		CommentTO comment = new CommentTO(new Long(1), "test", new Date(), new Long(2));

		service.save(comment);
		verify(commentDAO).add(comment);
		
	}

	@Test
	public void testUpdate() throws Exception {

		Long id = new Long(1);

		CommentTO comment = new CommentTO(id, "test", new Date(), id);
		when(commentDAO.fetchById(id)).thenReturn(comment);
		CommentTO newComment = new CommentTO(id, "new test", new Date(), id);
		boolean update = service.update(newComment);
		Assert.assertTrue(update);
		verify(commentDAO).fetchById(id);
		ArgumentCaptor<CommentTO> commentCaptor = ArgumentCaptor
				.forClass(CommentTO.class);
		verify(commentDAO).update(commentCaptor.capture());
		CommentTO updatedComment = commentCaptor.getValue();
		Assert.assertEquals("new test", updatedComment.getCommentText());

	}
	
	@Test
	public void testUpdateIfCommentNotFound() throws Exception {

		Long id = new Long(1);
		when(commentDAO.fetchById(id)).thenReturn(null);
		CommentTO comment = new CommentTO(id, "test", new Date(), id);
		boolean update = service.update(comment);
		Assert.assertFalse(update);
		verify(commentDAO).fetchById(id);

	}
	
	@Test(expected = TechnicalException.class)
	public void testUpdateThrowException() throws Exception {

		Long id = new Long(1);
		CommentTO comment = new CommentTO(id, "test", new Date(), id);
		when(commentDAO.fetchById(id)).thenReturn(comment);
		doThrow(DAOException.class).when(commentDAO).update(comment);
		service.update(comment);
		verify(commentDAO).fetchById(id);
		verify(commentDAO).update(comment);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(1);
		CommentTO comment = new CommentTO(id, "test", new Date(), id);
		when(commentDAO.fetchById(id)).thenReturn(comment);
		boolean delete = service.delete(id);
		Assert.assertTrue(delete);
		verify(commentDAO).fetchById(id);
		verify(commentDAO).delete(id);
	}
	
	@Test
	public void testDeleteIfCommentNotFound() throws Exception {

		Long id = new Long(1);
		when(commentDAO.fetchById(id)).thenReturn(null);
		boolean delete = service.delete(id);
		Assert.assertFalse(delete);
		verify(commentDAO).fetchById(id);

	}

	@Test(expected=TechnicalException.class)
	public void testDeleteThrowsException() throws Exception{
		
		Long id = new Long(1);
		CommentTO comment = new CommentTO(id, "test", new Date(), id);
		when(commentDAO.fetchById(id)).thenReturn(comment);
		doThrow(DAOException.class).when(commentDAO).delete(id);
		service.delete(id);
		verify(commentDAO).fetchById(id);
		verify(commentDAO).delete(id);
		
	}
	
	@Test
	public void testList() throws Exception {

		List<CommentTO> comments = new LinkedList<CommentTO>();
		CommentTO comment1 = new CommentTO(new Long(1), "test", new Date(), new Long(1));
		CommentTO comment2 = new CommentTO(new Long(2), "test", new Date(), new Long(1));
		comments.add(comment1);
		comments.add(comment2);
		when(commentDAO.list()).thenReturn(comments);

		List<CommentTO> resultComments = service.list();

		Assert.assertNotNull(comments);
		Assert.assertEquals(comments.size(), resultComments.size());
		Assert.assertArrayEquals(comments.toArray(), resultComments.toArray());
		
		verify(commentDAO).list();

	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=TechnicalException.class)
	public void testListThrowsException() throws Exception {
		
		when(commentDAO.list()).thenThrow(DAOException.class);
		service.list();
		
	}

}
