package by.epam.newsapp.dao;

import java.text.SimpleDateFormat;
import java.util.List;

import by.epam.newsapp.dao.impl.CommentDAOImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.newsapp.dto.CommentTO;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})

public class CommentDAOTest {

    public static final String PATH_TO_COMMENTS = "/comment/comment.xml";
    @Autowired
    private CommentDAOImpl commentDAO;

    private CommentTO expected;

    @Before
    public void setUp() throws Exception{

        expected = new CommentTO();
        expected.setCommentText("test");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        expected.setCreationDate(dateFormat.parse("01/01/2015 11:10:00"));
        expected.setNewsId(1L);
        expected.setId(1L);

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testGetAll() throws Exception {

        List<CommentTO> list = commentDAO.list();

        Assert.assertNotNull(list);

        for (CommentTO comment : list) {
            Assert.assertNotNull(comment);
            if (comment.getId() == 1L) {

                assertComments(expected, comment);

            }

        }

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testSave() throws Exception {

        Long id = commentDAO.add(expected);

        CommentTO actual = commentDAO.fetchById(id);

        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expected.getCommentText(), actual.getCommentText());
        Assert.assertEquals(expected.getNewsId(), actual.getNewsId());


    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testDelete() throws Exception {

        Long id = 2L;
        commentDAO.delete(id);
        CommentTO actual = commentDAO.fetchById(id);

        Assert.assertNull(actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testUpdate() throws Exception {

        Long id = 1L;

        commentDAO.update(expected);

        CommentTO actual = commentDAO.fetchById(id);
        assertComments(expected, actual);


    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testFetchById() throws Exception {

        Long id = 1L;

        CommentTO actual = commentDAO.fetchById(id);
        Assert.assertNotNull(actual);

        assertComments(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testFetchCommentsByNews() throws Exception {

        List<CommentTO> comments = commentDAO.fetchCommentsByNews(1L);

        Assert.assertNotNull(comments);
        Assert.assertEquals(2, comments.size());

        Long id = 1L;

        for (CommentTO comment : comments) {

            Assert.assertNotNull(comment);

            if (comment.getId() == id){
                assertComments(expected, comment);
            }
        }

    }

    @Test
    @DatabaseSetup(PATH_TO_COMMENTS)
    public void testGetCountCommentsOnNews() throws Exception{

        int actual  = commentDAO.getCountCommentsOnNews(1L);
        int expected = 2;

        Assert.assertEquals(expected, actual);

    }

    private void assertComments(CommentTO expected, CommentTO actual) {

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getCommentText(), actual.getCommentText());
        Assert.assertEquals(expected.getCreationDate(), actual.getCreationDate());
        Assert.assertEquals(expected.getNewsId(), actual.getNewsId());

    }

}
