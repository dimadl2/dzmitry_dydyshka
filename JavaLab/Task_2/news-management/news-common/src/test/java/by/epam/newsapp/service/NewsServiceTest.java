package by.epam.newsapp.service;

import by.epam.newsapp.dao.impl.NewsDAOImpl;
import by.epam.newsapp.dto.NewsTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.impl.NewsServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:/spring.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

    @Mock
    private NewsDAOImpl newsDAO;

    @InjectMocks
    @Autowired
    private NewsServiceImpl service;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() throws Exception {


    }


    @Test
    public void testUpdate() throws Exception {

        Long id = new Long(1);

        NewsTO news = initNews();

        NewsTO newsNews = new NewsTO();
        newsNews.setId(id);
        newsNews.setTitle("test1_title");
        newsNews.setFullText("test1_full");
        newsNews.setShortText("test1_short");
        newsNews.setCreationDate(new Date());
        newsNews.setModificationDate(new Date());

        when(newsDAO.fetchById(id)).thenReturn(news);
        boolean update = service.update(newsNews);
        Assert.assertTrue(update);
        verify(newsDAO).fetchById(id);
        ArgumentCaptor<NewsTO> newsCaptor = ArgumentCaptor.forClass(NewsTO.class);
        verify(newsDAO).update(newsCaptor.capture());
        NewsTO updatedNews = newsCaptor.getValue();
        Assert.assertEquals("test1_title", updatedNews.getTitle());
        Assert.assertEquals("test1_full", updatedNews.getFullText());
        Assert.assertEquals("test1_short", updatedNews.getShortText());

    }

    @Test
    public void testUpdateIfNewsNotFound() throws Exception {

        Long id = 1L;
        when(newsDAO.fetchById(id)).thenReturn(null);

        NewsTO newsNews = new NewsTO();
        newsNews.setId(id);
        newsNews.setTitle("test1_title");
        newsNews.setFullText("test1_full");
        newsNews.setShortText("test1_short");
        newsNews.setCreationDate(new Date());
        newsNews.setModificationDate(new Date());

        boolean update = service.update(newsNews);
        Assert.assertFalse(update);
        verify(newsDAO).fetchById(id);

    }

    @Test(expected = TechnicalException.class)
    public void testUpdateThrowException() throws Exception {

        Long id = 1L;

        NewsTO news = initNews();

        when(newsDAO.fetchById(id)).thenReturn(news);
        doThrow(DAOException.class).when(newsDAO).update(news);
        service.update(news);
        verify(newsDAO).fetchById(id);
        verify(newsDAO).update(news);

    }

    @Test
    public void testDelete() throws Exception {

        Long id = 1L;
        NewsTO news = initNews();
        news.setModificationDate(new Date());
        when(newsDAO.fetchById(id)).thenReturn(news);
        boolean delete = service.delete(id);
        Assert.assertTrue(delete);
        verify(newsDAO).fetchById(id);
        verify(newsDAO).delete(id);
    }

    @Test
    public void testDeleteIfNewsNotFound() throws Exception {

        Long id = 1L;
        when(newsDAO.fetchById(id)).thenReturn(null);
        boolean delete = service.delete(id);
        Assert.assertFalse(delete);
        verify(newsDAO).fetchById(id);

    }

    @Test(expected = TechnicalException.class)
    public void testDeleteThrowsException() throws Exception {

        Long id = 1L;
        NewsTO news = initNews();
        when(newsDAO.fetchById(id)).thenReturn(news);
        doThrow(DAOException.class).when(newsDAO).delete(id);
        service.delete(id);
        verify(newsDAO).fetchById(id);
        verify(newsDAO).delete(id);

    }

    @Test
    public void testList() throws Exception {

        List<NewsTO> newsList = new LinkedList<NewsTO>();

        NewsTO news = initNews();

        newsList.add(news);

        when(newsDAO.list()).thenReturn(newsList);

        List<NewsTO> resultNews = service.list();

        Assert.assertNotNull(resultNews);
        Assert.assertEquals(1, resultNews.size());

        verify(newsDAO).list();

    }

    @SuppressWarnings("unchecked")
    @Test(expected = TechnicalException.class)
    public void testListThrowsException() throws Exception {

        when(newsDAO.list()).thenThrow(DAOException.class);
        service.list();

    }

    @Test
    public void testAttachAuthorToNews() throws Exception {


        when(newsDAO.attachAuthorToNews(anyLong(), anyLong())).thenReturn(1L);

        Long actual = service.attachAuthorToNews(1L, 1L);
        Long expected = 1L;

        Assert.assertEquals(expected, actual);
        verify(newsDAO).attachAuthorToNews(1L, 1L);

    }

    @Test(expected = TechnicalException.class)
    public void testAttachAuthorToNewsThrowsException() throws Exception {

        when(newsDAO.attachAuthorToNews(anyLong(), anyLong())).thenThrow(DAOException.class);
        service.attachAuthorToNews(1L, 1L);
        verify(newsDAO).attachAuthorToNews(1L, 1L);

    }

    @Test
    public void testAttachTagToNews() throws Exception {


        when(newsDAO.attachTagToNews(anyLong(), anyLong())).thenReturn(1L);

        Long actual = service.attachTagToNews(1L, 1L);
        Long expected = 1L;

        verify(newsDAO).attachTagToNews(1L, 1L);
        Assert.assertEquals(expected, actual);

    }

    @Test(expected = TechnicalException.class)
    public void testAttachTagToNewsThrowsException() throws Exception {

        when(newsDAO.attachTagToNews(anyLong(), anyLong())).thenThrow(DAOException.class);
        service.attachTagToNews(1L, 1L);
        verify(newsDAO).attachTagToNews(1L, 1L);

    }

    @Test
    public void testFetchLimitNews() throws Exception {

        NewsTO news1 = initNews();
        NewsTO news2 = initNews();

        List<NewsTO> news = Arrays.asList(news1, news2);

        when(newsDAO.fetchLimitNews(1, 2)).thenReturn(news);

        List<NewsTO> newsActual = service.fetchLimitNews(1, 2);
        Assert.assertEquals(news.size(), newsActual.size());
        verify(newsDAO).fetchLimitNews(1, 2);

    }

    @Test
    public void testGetCountRows() throws Exception {

        when(newsDAO.getCountRows()).thenReturn(3);

        int actual = service.getCountRows();
        Assert.assertEquals(3, actual);
        verify(newsDAO).getCountRows();

    }

    @Test
    public void testGetNextNewsId() throws Exception {

        when(newsDAO.getNextNewsId(anyLong())).thenReturn(2L);
        long actual = service.getNextNewsId(1L);
        Assert.assertEquals(2L, actual);
        verify(newsDAO).getNextNewsId(1L);

    }

    @Test
    public void testGetPreviousNewsId() throws Exception {

        when(newsDAO.getPreviousNewsId(anyLong())).thenReturn(2L);
        long actual = service.getPreviousNewsId(1L);
        Assert.assertEquals(2L, actual);
        verify(newsDAO).getPreviousNewsId(1L);

    }

    @Test
    public void testGetCountSearchRows() throws Exception {

        when(newsDAO.getCountSearchRows(anyLong(), anyList())).thenReturn(3);

        int actual = service.getCountSearchRows(1L, Arrays.asList(1L, 2L));
        Assert.assertEquals(3, actual);
        verify(newsDAO).getCountSearchRows(1L, Arrays.asList(1L, 2L));

    }

    @Test
    public void testGetCountSearchByTagsRows() throws Exception {

        when(newsDAO.getCountSearchByTagsRows(anyList())).thenReturn(3);
        int actual = service.getCountSearchByTagsRows(Arrays.asList(1L, 2L));
        Assert.assertEquals(3, actual);
        verify(newsDAO).getCountSearchByTagsRows(Arrays.asList(1L, 2L));

    }

    @Test
    public void testGetCountSearchByAuthorRows() throws Exception {

        when(newsDAO.getCountSearchByAuthorRows(anyLong())).thenReturn(3);
        int actual = service.getCountSearchByAuthorRows(1L);
        Assert.assertEquals(3, actual);
        verify(newsDAO).getCountSearchByAuthorRows(1L);

    }

    @Test
    public void testFetchLimitSearchNews() throws Exception {

        List<Long> tagsId = Arrays.asList(1L, 2L);
        List<NewsTO> news = Arrays.asList(initNews(), initNews());

        when(newsDAO.searchLimitsByTagsAndAuthors(1L, tagsId, 1, 2));
        List<NewsTO> newsActual = service.fetchLimitSearchNews(1L, tagsId, 1, 2);
        Assert.assertEquals(news.size(), newsActual.size());
        verify(newsDAO).searchLimitsByTagsAndAuthors(1L, tagsId, 1, 2);

    }

    @Test
    public void testFetchLimitSearchByTagsNews() throws Exception {

        List<Long> tagsId = Arrays.asList(1L, 2L);
        List<NewsTO> news = Arrays.asList(initNews(), initNews());

        when(newsDAO.fetchLimitSearchByTags(tagsId, 1, 2));
        List<NewsTO> newsActual = service.fetchLimitSearchByTagsNews(tagsId, 1, 2);
        Assert.assertEquals(news.size(), newsActual.size());
        verify(newsDAO).fetchLimitSearchByTags(tagsId, 1, 2);

    }

    @Test
    public void testFetchLimitSearchByAuthorsNews() throws Exception {

        List<NewsTO> news = Arrays.asList(initNews(), initNews());

        when(newsDAO.fetchLimitSearchByAuthor(1L, 1, 2));
        List<NewsTO> newsActual = service.fetchLimitSearchByAuthorNews(1L, 1, 2);
        Assert.assertEquals(news.size(), newsActual.size());
        verify(newsDAO).fetchLimitSearchByAuthor(1L, 1, 2);

    }

    private NewsTO initNews() {

        NewsTO news = new NewsTO();
        news.setId(1L);
        news.setTitle("test_title");
        news.setFullText("test_full");
        news.setShortText("test_short");
        news.setCreationDate(new Date());
        news.setModificationDate(new Date());

        return news;
    }

}
