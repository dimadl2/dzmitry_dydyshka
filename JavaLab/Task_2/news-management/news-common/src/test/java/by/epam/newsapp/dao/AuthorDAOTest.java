package by.epam.newsapp.dao;

import java.util.List;

import by.epam.newsapp.dao.impl.AuthorDAOImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.newsapp.dto.AuthorTO;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})

public class AuthorDAOTest {

    private static final String PATH_TO_AUTHORS = "/author/author.xml";


    @Autowired
    private AuthorDAOImpl authorDAO;

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testList() throws Exception {

        List<AuthorTO> list = authorDAO.list();

        Assert.assertNotNull(list);

        AuthorTO expected = new AuthorTO(new Long(1), "Oleg");
        for (AuthorTO author : list) {

            Assert.assertNotNull(author);
            if (author.getId() == 1L) {

                assertAuthor(expected, author);

            }

        }

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testSave() throws Exception {

        AuthorTO expected = new AuthorTO("Vasia");
        Long id = authorDAO.add(expected);
        AuthorTO actual = authorDAO.fetchById(id);

        Assert.assertEquals(expected.getName(), actual.getName());

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testDelete() throws Exception {

        Long id = 3L;

        authorDAO.delete(id);
        AuthorTO actual = authorDAO.fetchById(id);

        Assert.assertNull(actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testUpdate() throws Exception {

        Long id = 3L;
        AuthorTO expected = new AuthorTO(id, "Max");

        authorDAO.update(expected);

        AuthorTO actual = authorDAO.fetchById(id);
        assertAuthor(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testFetchById() throws Exception {

        Long id = 3L;
        AuthorTO actual = authorDAO.fetchById(id);
        AuthorTO expected = new AuthorTO(id, "Dima");
        Assert.assertNotNull(actual);

        assertAuthor(expected, actual);

    }

    @Test
    @DatabaseSetup(PATH_TO_AUTHORS)
    public void testFetchAuthorsByNews() throws Exception {

        Long id = 1L;
        List<AuthorTO> authors = authorDAO.fetchAuthorsByNews(id);

        Assert.assertNotNull(authors);
        Assert.assertEquals(1, authors.size());

        for (AuthorTO author : authors) {

            Assert.assertNotNull(author);
        }

        Long idAuthor = 2L;
        AuthorTO expected = new AuthorTO(idAuthor, "Alex");
        AuthorTO actual = authors.get(0);
        assertAuthor(expected, actual);
    }

    private void assertAuthor(AuthorTO expected, AuthorTO actual) {

        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getName(), actual.getName());

    }

}
