package by.epam.newsapp.service;

import by.epam.newsapp.dto.*;
import by.epam.newsapp.service.impl.*;
import by.epam.newsapp.util.Page;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@ContextConfiguration(locations = { "classpath:/spring.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsManageTest {

    @Mock
    private NewsServiceImpl newsService;

    @Mock
    private CommentServiceImpl commentService;

    @Mock
    private AuthorServiceImpl authorService;

    @Mock
    private TagServiceImpl tagService;

    @Autowired
    @InjectMocks
    private NewsManageImpl newsManage;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws Exception{

        NewsVO news = initNews();

        when(newsService.save(any(NewsTO.class))).thenReturn(1L);

        long expectedId = 1L;
        long actual = newsManage.save(news);

        Assert.assertEquals(expectedId, actual);
        verify(newsService).save(any(NewsTO.class));
        verify(newsService, times(2)).attachAuthorToNews(anyLong(), anyLong());
        verify(newsService, times(2)).attachTagToNews(anyLong(), anyLong());

    }

    @Test
    public void testFetchById() throws Exception{

        NewsTO newsTO = initNewsTO();

        CommentTO comment1 = new CommentTO(1L, "comment1", new Date(), 1L);
        CommentTO comment2 = new CommentTO(2L, "comment2", new Date(), 1L);

        List<CommentTO> comments = Arrays.asList(comment1, comment2);

        TagTO tag1 = new TagTO(1L, "tag1");
        TagTO tag2 = new TagTO(2L, "tag2");

        List<TagTO> tags = Arrays.asList(tag1, tag2);

        AuthorTO author1 = new AuthorTO(1L, "author1");
        AuthorTO author2 = new AuthorTO(2L, "author2");

        List<AuthorTO> authors = Arrays.asList(author1, author2);

        when(newsService.fetchById(1L)).thenReturn(newsTO);
        when(commentService.fetchCommentsByNews(1L)).thenReturn(comments);
        when(tagService.fetchTagByNews(1L)).thenReturn(tags);
        when(authorService.fetchAuthorsByNews(1L)).thenReturn(authors);

        NewsVO news = newsManage.fetchById(1L);

        NewsTO newsTOActual = news.getNewsTO();

        Assert.assertEquals(newsTO.getId(), newsTOActual.getId());
        Assert.assertEquals(newsTO.getTitle(), newsTOActual.getTitle());
        Assert.assertEquals(newsTO.getShortText(), newsTOActual.getShortText());
        Assert.assertEquals(newsTO.getFullText(), newsTOActual.getFullText());
        Assert.assertEquals(newsTO.getCreationDate(), newsTOActual.getCreationDate());
        Assert.assertEquals(newsTO.getModificationDate(), newsTOActual.getModificationDate());

        Assert.assertEquals(2, news.getAuthors().size());
        Assert.assertEquals(2, news.getComments().size());
        Assert.assertEquals(2, news.getTags().size());

        verify(newsService).fetchById(1L);
        verify(commentService).fetchCommentsByNews(1L);
        verify(tagService).fetchTagByNews(1L);


    }

    @Test
    public void testGetAll() throws Exception{

        NewsTO newsTO = initNewsTO();

        List<NewsTO> newsTOs = Arrays.asList(newsTO);

        TagTO tag1 = new TagTO(1L, "tag1");
        TagTO tag2 = new TagTO(2L, "tag2");

        List<TagTO> tags = Arrays.asList(tag1, tag2);

        AuthorTO author1 = new AuthorTO(1L, "author1");
        AuthorTO author2 = new AuthorTO(2L, "author2");

        List<AuthorTO> authors = Arrays.asList(author1, author2);

        when(newsService.list()).thenReturn(newsTOs);
        when(tagService.fetchTagByNews(1L)).thenReturn(tags);
        when(authorService.fetchAuthorsByNews(1L)).thenReturn(authors);
        when(commentService.getCountCommentsOnNews(1L)).thenReturn(3);

        List<NewsVO> newsVOs = newsManage.getAll();

        Assert.assertEquals(1, newsVOs.size());
        verify(newsService).list();
        verify(tagService).fetchTagByNews(1L);
        verify(commentService).getCountCommentsOnNews(1L);
        verify(authorService).fetchAuthorsByNews(1L);


    }

    @Test
    public void testUpdate() throws Exception {

        newsManage.update(initNews());

        verify(newsService).update(any(NewsTO.class));
        verify(newsService).disconnectAllTagsOnNews(1L);
        verify(newsService, times(2)).attachTagToNews(anyLong(), anyLong());
        verify(newsService).disconnectAllAuthorsOnNews(1L);
        verify(newsService, times(2)).attachAuthorToNews(anyLong(), anyLong());


    }

    @Test
    public void testGetPage() throws Exception {

        NewsTO newsTO1 = initNewsTO();
        NewsTO newsTO2 = initNewsTO();

        List<NewsTO> newsTOs = Arrays.asList(newsTO1, newsTO2);

        TagTO tag1 = new TagTO(1L, "tag1");
        TagTO tag2 = new TagTO(2L, "tag2");

        List<TagTO> tags = Arrays.asList(tag1, tag2);

        AuthorTO authorTO1 = new AuthorTO(1L, "author1");
        AuthorTO authorTO2 = new AuthorTO(2L, "author2");

        List<AuthorTO> authors = Arrays.asList(authorTO1, authorTO2);


        when(newsService.fetchLimitNews(anyInt(), anyInt())).thenReturn(newsTOs);
        when(tagService.fetchTagByNews(anyLong())).thenReturn(tags);
        when(authorService.fetchAuthorsByNews(anyLong())).thenReturn(authors);
        when(commentService.getCountCommentsOnNews(anyLong())).thenReturn(3);
        when(newsService.getCountRows()).thenReturn(3);

        Page page = newsManage.getPage(1, 5);

        Assert.assertEquals(2, page.getItemsForPage().size());

        ArgumentCaptor<Integer> startValueCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> endValueCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(newsService).fetchLimitNews(startValueCaptor.capture(), endValueCaptor.capture());

        Assert.assertEquals(1, (int) startValueCaptor.getValue());
        Assert.assertEquals(5, (int) endValueCaptor.getValue());

        verify(tagService, times(2)).fetchTagByNews(anyLong());
        verify(authorService, times(2)).fetchAuthorsByNews(anyLong());
        verify(commentService, times(2)).getCountCommentsOnNews(anyLong());
        verify(newsService).getCountRows();

    }

    @Test
    public void testGetFilteredPageByAuthor(){



    }


    private NewsVO initNews(){

        NewsVO newsVO = new NewsVO();

        TagTO tag1 = new TagTO(1L, "tag1");
        TagTO tag2 = new TagTO(2L, "tag2");

        List<TagTO> tags = Arrays.asList(tag1, tag2);

        AuthorTO authorTO1 = new AuthorTO(1L, "author1");
        AuthorTO authorTO2 = new AuthorTO(2L, "author2");

        List<AuthorTO> authors = Arrays.asList(authorTO1, authorTO2);

        newsVO.setNewsTO(initNewsTO());
        newsVO.setAuthors(authors);
        newsVO.setTags(tags);

        return newsVO;

    }

    private NewsTO initNewsTO(){

        NewsTO newsTO = new NewsTO();
        newsTO.setId(1L);
        newsTO.setTitle("test title");
        newsTO.setShortText("test short");
        newsTO.setFullText("test full text");
        newsTO.setModificationDate(new Date());
        newsTO.setCreationDate(new Date());

        return newsTO;

    }

}
