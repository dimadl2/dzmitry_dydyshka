package by.epam.newsapp.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsapp.dao.impl.AuthorDAOImpl;
import by.epam.newsapp.dto.AuthorTO;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.TechnicalException;
import by.epam.newsapp.service.impl.AuthorServiceImpl;

@ContextConfiguration(locations = { "classpath:/spring.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {

	@Mock
	private AuthorDAOImpl authorDAO;

	@InjectMocks
	private AuthorServiceImpl service;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAdd() throws Exception {

		Long id = new Long(1);

		when(authorDAO.add(any(AuthorTO.class))).thenReturn(id);

		AuthorTO author = new AuthorTO(id, "Dima");

		Long authorId = service.save(author);
		Assert.assertEquals(id, authorId);
		verify(authorDAO).add(author);
		ArgumentCaptor<AuthorTO> authorCaptor = ArgumentCaptor
				.forClass(AuthorTO.class);
		verify(authorDAO).add(authorCaptor.capture());
		AuthorTO newAuthor = authorCaptor.getValue();
		Assert.assertEquals(author.getName(), newAuthor.getName());

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testAddThrowException() throws Exception {

		when(authorDAO.add(any(AuthorTO.class))).thenThrow(DAOException.class);

		AuthorTO author = new AuthorTO(new Long(1), "Dima");
		service.save(author);

		verify(authorDAO).add(author);

	}

	@Test
	public void testUpdate() throws Exception {

		Long id = new Long(1);

		AuthorTO author = new AuthorTO(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		AuthorTO updatedAuthor = new AuthorTO(id, "Sasha");
		boolean update = service.update(updatedAuthor);
		Assert.assertTrue(update);
		verify(authorDAO).fetchById(id);
		ArgumentCaptor<AuthorTO> authorCaptor = ArgumentCaptor
				.forClass(AuthorTO.class);
		verify(authorDAO).update(authorCaptor.capture());
		author = authorCaptor.getValue();
		Assert.assertEquals("Sasha", author.getName());

	}

	@Test
	public void testUpdateIfAuthorNotFound() throws Exception {

		Long id = new Long(1);
		when(authorDAO.fetchById(id)).thenReturn(null);
		AuthorTO author = new AuthorTO(id, "Dima");
		boolean update = service.update(author);
		Assert.assertFalse(update);
		verify(authorDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testUpdateThrowException() throws Exception {

		Long id = new Long(1);
		AuthorTO author = new AuthorTO(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		doThrow(DAOException.class).when(authorDAO).update(author);
		service.update(author);
		verify(authorDAO).fetchById(id);
		verify(authorDAO).update(author);

	}

	@Test
	public void testDelete() throws Exception {

		Long id = new Long(1);
		AuthorTO author = new AuthorTO(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		boolean delete = service.delete(id);
		Assert.assertTrue(delete);
		verify(authorDAO).fetchById(id);
		verify(authorDAO).delete(id);
	}

	@Test
	public void testDeleteIfAuthorNotFound() throws Exception {

		Long id = new Long(1);
		when(authorDAO.fetchById(id)).thenReturn(null);
		boolean delete = service.delete(id);
		Assert.assertFalse(delete);
		verify(authorDAO).fetchById(id);

	}

	@Test(expected = TechnicalException.class)
	public void testDeleteThrowsException() throws Exception {

		Long id = new Long(1);
		AuthorTO author = new AuthorTO(id, "Dima");
		when(authorDAO.fetchById(id)).thenReturn(author);
		doThrow(DAOException.class).when(authorDAO).delete(id);
		service.delete(id);
		verify(authorDAO).fetchById(id);
		verify(authorDAO).delete(id);

	}

	@Test
	public void testList() throws Exception {

		List<AuthorTO> authors = new LinkedList<AuthorTO>();
		authors.add(new AuthorTO(new Long(1), "Dima"));
		authors.add(new AuthorTO(new Long(2), "Max"));
		when(authorDAO.list()).thenReturn(authors);

		List<AuthorTO> resultAuthors = service.list();

		Assert.assertNotNull(authors);
		Assert.assertEquals(authors.size(), resultAuthors.size());
		Assert.assertArrayEquals(authors.toArray(), resultAuthors.toArray());

		verify(authorDAO).list();

	}

	@SuppressWarnings("unchecked")
	@Test(expected = TechnicalException.class)
	public void testListThrowsException() throws Exception {

		when(authorDAO.list()).thenThrow(DAOException.class);
		service.list();

	}

}
