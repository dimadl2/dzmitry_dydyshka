<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="header"><spring:message code="label.title"/></div>
<div class="location">
	<a href="?page=${pageNum}&lang=en&action=${action}"><spring:message code="label.lang.en"/></a>
	<a href="?page=${pageNum}&lang=ru&action=${action}"><spring:message code="label.lang.ru"/></a>
</div>