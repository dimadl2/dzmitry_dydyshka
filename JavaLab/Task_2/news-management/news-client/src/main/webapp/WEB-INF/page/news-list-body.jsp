<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">
	<table width="880px" cellspacing=0 class="table-body">
		<c:if test="${not empty errorFilter}">
			<tr>
				<td colspan=2 align="center">
					<p style="color:red; padding-top: 20px;">Please, choose the author or tags!
				</td>
			</tr>
		</c:if>
			<tr>
            	<td colspan=2 align="center" id="filterError" style="display: none">
            		<p style="color:red; padding-top: 20px;">Please, choose the author or tags!
            	</td>
            </tr>
		<tr>
			<td>
				<div class="filter">
					<form action="/news-client/news/filter" method="POST" id="filter">
						<input type="hidden" name="action" value="filter">
                        <div class="multiselect" class="row">
                            <div class="selectBox">
                                <select>
                                    <option id="sel-title"><spring:message code="label.select.tags"/></option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes" align="left">
                                <c:forEach var="tag" items="${tags}">
                                    <label for="${tag.tagName}">
                                   		<input type="checkbox" class="check" name="tagsId" value="${tag.id}"

                                      		<c:if test="${tagsId.contains(tag.id)}">
                                        			checked
                                        	</c:if>

                                        />${tag.tagName}
                                    </label>
                                </c:forEach>
                            </div>
                        </div>
						<div class="row">
                            <select id="author" name="author">
								<option id="selector" disabled selected><b><spring:message code="label.select.author"/></b></option>
								<c:forEach var="author" items="${authors}">
									<option value="${author.id}"

										<c:if test="${author.id eq authorId}">
											selected
										</c:if>

									>${author.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="row">
							<input type="submit" value="<spring:message code="label.filter"/>">
							<input type="button" id="reset" value="<spring:message code="label.reset"/>" style="margin-left: 10px">
						</div>
					</form>
				</div>
			</td>
		</tr>

		<c:if test="${empty news}">
			<tr>
				<td colspan=2 align="center" style="padding-top: 50px;">
					<h3 style="color: gray"><spring:message code="message.filter.empty"/></h3>
				</td>
			</tr>
		</c:if>

		<c:forEach var="item" items="${news}">
			<tr>
				<td>
					<div class="item-content">
						<table width="800px" cellspacing=0>
							<tr>
								<td  width="690px">
									<b>${item.newsTO.title}</b> (by

										<c:forEach var="author" items="${item.authors}">
											${author.name}
										</c:forEach>

										)
								</td>
								<td align="right">
									<u>
										<spring:message var="datePattern" code="pattern.date" scope="page"/>
										<fmt:formatDate type="date" pattern="${datePattern}" value="${item.newsTO.creationDate}" /></u>
								</td>
							</tr>
							<tr>
								<td colspan=2>
									<div style="margin-top: 20px; width: 700px">
										${item.newsTO.shortText}
									</div>
								</td>
							</tr>
							<tr>
								<td colspan=2 align="right">
									<div>
										<div class="row-tool" align="right">
											<a href="/news-client/news/${item.newsTO.id}"><spring:message code="label.view"/></a>
										</div>
										<div class="row-tool" style="color: red;width: 100px; ">
											<spring:message code="label.comments"/>(${item.countComments})
										</div>
										<div style="color: gray; width: 540px;" class="row-tool" align="right">
											<c:forEach var="tag" items="${item.tags}">
												${tag.tagName},

											</c:forEach>
										</div>
										
										
									</div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>

		</c:forEach>
	</table>

	<div class="paginator">
          <div style="text-align: center; margin: 20px">
               <c:forEach var="i" begin="1" end="${numberOfPage}">
                   <div class="row">
                       <form

                 			<c:if test="${not empty action}">
                 				action="/news-client/news/filter" method="GET">
                 			</c:if>

                 			<c:if test="${empty action}">
                                action="/news-client/news" method="GET">
                            </c:if>


                            <input type="hidden" name="page" value="${i}"/>
                            <input type="submit" value="${i}"/>
                       </form>
                   </div>
               </c:forEach>
          </div>
    </div>
</div>