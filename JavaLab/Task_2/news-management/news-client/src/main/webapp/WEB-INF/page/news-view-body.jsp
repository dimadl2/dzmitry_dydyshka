<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="body">
	<div style="margin-left: 10px; margin-top: 5px">

	<c:if test="${not empty action}">
		<a href="/news-client/news/filter?page=${pageNum}">
        	<spring:message code="label.back"/>
        </a>
	</c:if>

	<c:if test="${empty action}">
    		<a href="/news-client/news?page=${pageNum}">
            	<spring:message code="label.back"/>
            </a>
    	</c:if>

	</div>
	<div style="padding-left: 40px; padding-top: 10px">
		<table class="table-body" style="width: 100%">
			<tr>
				<td width="550"><b>${news.newsTO.title} </b></td>
				<td valign="top" width="150px">(by
					<c:forEach var="author" items="${news.authors}">
                                           			${author.name}
                       </c:forEach>
											)</td>
				<td valign="top" style="margin">
					<spring:message var="datePattern" code="pattern.date" scope="page"/>
					<fmt:formatDate type="date" pattern="${datePattern}" value="${news.newsTO.modificationDate}" />
				</td>
			</tr>
			<tr >
				<td colspan=2 align="justify">
					<div style="padding-top: 30px">
							${news.newsTO.fullText}
					</div>
				</td>
			</tr>
			<c:forEach var="comment" items="${news.comments}">
				<tr>
					<td>
						<div class="comment" style="margin-top: 20px">
							<div style="margin-left: 8px">
								<fmt:formatDate type="date" pattern="${datePattern}" value="${comment.creationDate}" />

							</div>
							<div style="background-color: rgb(210, 210, 201); width: 400px; margin-left: 10px">
								${comment.commentText}
							</div>
						</div>
					</td>
				</tr>
			</c:forEach>

			<c:if test="${not empty errorComment}">
				<tr>
					<td align="center">
						<p style="color:red;margin: 0;padding-top: 10px;"><spring:message code="message.comment.empty"/>
					</td>
				</tr>
			</c:if>

			<tr>
				<td align="center" id="errorComment" style="display: none;">
					<p style="color:red;margin: 0;padding-top: 10px;"><spring:message code="message.comment.empty"/>
				</td>
			</tr>
			<tr>
				<td>
					<form action="/news-client/news/${news.newsTO.id}" method="POST" id="comment">
						<input type="hidden" name="action" value="addComment"/>
 						<textarea id="txt-comment" name="commentText" cols="54" rows="4" style="margin-left: 10px; margin-top: 15px; resize: none" wrap="hard"> </textarea><br>
						<div style="width: 400px; text-align: right; margin-left: 10px; margin-top: 10px" >
							<input type="submit" value="<spring:message code="label.comments.post"/>">
						</div>
					</form>
				</td>
			</tr>
		</table>
	</div>
	<table class="table-body" width="860px" style="margin-left: 10px; margin-right: 10px; margin-top: 20px">
		<tr>
			<td>
				<c:if test="${previousId != 0}" >
					<a href="/news-client/news/${previousId}"><spring:message code="label.previous"/></a>
				</c:if>
			</td>
			<td align="right">
				<c:if test="${nextId != 0}" >
					<a href="/news-client/news/${nextId}"><spring:message code="label.next"/></a>
				</c:if>
			</td>
		</tr>
	</table>
</div>